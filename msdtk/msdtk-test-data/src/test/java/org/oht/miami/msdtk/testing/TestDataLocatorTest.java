/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.testing;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Verifies the presence of required test data.
 * 
 * @author Raphael Yu Ning
 */
public class TestDataLocatorTest {

    private TestDataLocator testDataLocator = null;

    @Before
    public void setUp() throws IOException {

        testDataLocator = new TestDataLocator();
    }

    @After
    public void tearDown() {

        testDataLocator = null;
    }

    private void doTestLocateStudy(String studyName, int numResources) {

        List<String> studyResources = testDataLocator.locateStudy(studyName);
        assertNotNull(studyResources);
        assertTrue(studyResources.size() == numResources);

        for (String studyResource : studyResources) {
            if (studyResource.startsWith("classpath:")) {
                studyResource = studyResource.substring("classpath:".length());
            }
            assertNotNull(Thread.currentThread().getContextClassLoader().getResource(studyResource));
        }
    }

    @Test
    public void testLocateMINT10Study() {

        // MINT10 study contains two DICOM instances
        doTestLocateStudy("MINT10", 2);
    }

    @Test
    public void testLocate20phaseStudy() {

        // 20 phase study contains four DICOM instances
        doTestLocateStudy("20phase", 4);
    }

    @Test
    public void testLocateNemamfctStudy() {

        // nemamfct study contains two DICOM instances
        doTestLocateStudy("nemamfct", 2);
    }

    private void doTestLocateDataset(String datasetName) {

        File datasetDir = testDataLocator.locateDataset(datasetName);
        assertNotNull(datasetDir);
    }

    @Test
    public void testLocateSingleFrameDicomDataset() {

        doTestLocateDataset("singleframe");
    }

    @Test
    public void testLocateMultiFrameDicomDataset() {

        doTestLocateDataset("multiframe");
    }

    @Test
    public void testLocateOutput() {

        File outputDir = testDataLocator.locateOutput();
        assertNotNull(outputDir);
    }
}
