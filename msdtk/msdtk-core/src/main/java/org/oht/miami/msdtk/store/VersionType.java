/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

/**
 * The type of the version that is being stored:
 *      New:        This is the zeroth version
 *      Unchanged:  ??
 *      Append:     This version only adds new data elements to the Study.  
 *      Mofidy:     This version modifies the values of existing data element or deletes 
 *                  existing data elements it also might add new data elements to the version.
 *      
 * @author James F Philbin (james.philbin@jhmi.edu)
 * @author Ryan Stonebraker
 */
public enum VersionType {
    New,
    Unchanged,
    Append,
    Modify
}