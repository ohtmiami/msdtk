/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.util.DicomUID;

import java.io.Serializable;
import java.util.List;

/**
 * Class for encapsulating all fields for a study entry in a way that is
 * serializable
 * 
 * @author Ryan Stonebraker (rstonebr@harris.com)
 */
public class TransportableStudyEntry implements Serializable {
    // Fields

    /**
     * 
     */
    private static final long serialVersionUID = -3439909592394672396L;

    /**
     * The DICOM Study Instance UID for the study. It must be supplied when the
     * Entry is created.
     */
    protected final DicomUID studyInstanceUID;

    /**
     * Version history
     */
    private VersionHistory versionHistory;

    /**
     * A StudyInfo object containing a synopsis of study
     */
    private StudyInfo studyInfo;

    /**
     * A flag specifying whether a study has been deleted or not. If this flag
     * is set the study will not be visible except to privileged users.
     */
    private DeleteType deleteType = DeleteType.Live;

    /**
     * Bulk data set history
     */
    private List<BulkDataSet> bulkDataSetHistory;

    /**
     * Constructor
     * 
     * @param studyInstanceUID
     * @param versionHistory
     * @param studyInfo
     * @param deleteType
     * @param bulkDataSetHistory
     */
    public TransportableStudyEntry(DicomUID studyInstanceUID, VersionHistory versionHistory,
            StudyInfo studyInfo, DeleteType deleteType, List<BulkDataSet> bulkDataSetHistory) {
        this.studyInstanceUID = studyInstanceUID;
        this.versionHistory = versionHistory;
        this.studyInfo = studyInfo;
        this.deleteType = deleteType;
        this.bulkDataSetHistory = bulkDataSetHistory;
    }

    /**
     * @return the bulkDataSetHistory
     */
    public List<BulkDataSet> getBulkDataSetHistory() {
        return bulkDataSetHistory;
    }

    /**
     * @return the studyInstanceUID
     */
    public DicomUID getStudyInstanceUID() {
        return studyInstanceUID;
    }

    /**
     * @return the versionHistory
     */
    public VersionHistory getVersionHistory() {
        return versionHistory;
    }

    /**
     * @return the studyInfo
     */
    public StudyInfo getStudyInfo() {
        return studyInfo;
    }

    /**
     * @return the deleteType
     */
    public DeleteType getDeleteType() {
        return deleteType;
    }

    /**
     * Get string representation
     * 
     * @return String output of contained fields
     */
    @Override
    public String toString() {
        return "StudyInstanceUID: " + studyInstanceUID.toString() + "\n VersionHistory:"
                + versionHistory.toString() + "\n StudyInfo:" + studyInfo.toString()
                + "\n DeleteType:" + deleteType + "\n bulkDataSetHistory:"
                + bulkDataSetHistory.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((bulkDataSetHistory == null) ? 0 : bulkDataSetHistory.hashCode());
        result = prime * result + ((deleteType == null) ? 0 : deleteType.hashCode());
        result = prime * result + ((studyInfo == null) ? 0 : studyInfo.hashCode());
        result = prime * result + ((studyInstanceUID == null) ? 0 : studyInstanceUID.hashCode());
        result = prime * result + ((versionHistory == null) ? 0 : versionHistory.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransportableStudyEntry other = (TransportableStudyEntry) obj;
        if (bulkDataSetHistory == null) {
            if (other.bulkDataSetHistory != null) {
                return false;
            }
        } else if (!bulkDataSetHistory.equals(other.bulkDataSetHistory)) {
            return false;
        }
        if (deleteType != other.deleteType) {
            return false;
        }
        if (studyInfo == null) {
            if (other.studyInfo != null) {
                return false;
            }
        } else if (!studyInfo.equals(other.studyInfo)) {
            return false;
        }
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null) {
                return false;
            }
        } else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
            return false;
        }
        if (versionHistory == null) {
            if (other.versionHistory != null) {
                return false;
            }
        } else if (!versionHistory.equals(other.versionHistory)) {
            return false;
        }
        return true;
    }
}
