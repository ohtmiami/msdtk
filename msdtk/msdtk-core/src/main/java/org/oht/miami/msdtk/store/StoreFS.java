/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.util.UUID;

/**
 * This is the File System implementation of the IStore interface. It provides
 * naming methods for folders, versions and BulkData. It also provides the
 * version creation method.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 */
public class StoreFS extends Store {

    public static final String VERSION_FILE_EXTENSION = ".dcm";

    public static final String BULK_DATA_FILE_EXTENSION = ".msdbd";

    private final File rootDirectory;

    /**
     * Creates a File System based Store. The root directory for the Store is
     * provided when the store is created. Each object in the store has a path
     * in this format: <rootDirectory>/<studyInstanceUID>/<UUID>.<ext> where
     * <rootDirecory> The root directory in the file system where the all
     * objects are stored <studyInstanceUID> The name of the folder in which the
     * objects assocated with this study are stored. <UUID> The UUID of the
     * object stored <ext> An extension showing the type of object where: ".dcm"
     * is the Version object and ".msdbd" is the BulkData object
     * 
     * @param rootDirectory
     *            A String that contains the file system path of the root
     *            directory for this Store.
     */
    StoreFS(String rootDirectory) {
        this.rootDirectory = new File(rootDirectory);
        this.rootDirectory.mkdirs();
    }

    public File getRootDirectory() {
        return rootDirectory;
    }

    /**
     * getStudyPath returns a File with the path to the folder or directory that
     * contains the versions, bulkdata, etc. for the Study with
     * studyInstanceUID.
     * 
     * @param studyInstanceUID
     *            The DicomUID of the Study
     * @return A File with the path to the folder or directory containing the
     *         Study.
     */
    File getStudyFolderPath(DicomUID studyInstanceUID) {
        return new File(rootDirectory, studyInstanceUID.toString());
    }

    /**
     * This method returns the full pathname including filename and extension
     * for version with the matching studyInstanceUID and UUID.
     * 
     * @param studyInstanceUID
     * @param uuid
     * @return The full pathname to the file which contains or will contain the
     *         version.
     */
    File getVersionFilePath(DicomUID studyInstanceUID, UUID versionUUID) {
        return new File(getStudyFolderPath(studyInstanceUID), versionUUID.toString()
                + VERSION_FILE_EXTENSION);
    }

    /**
     * This method returns the full pathname including filename and extension
     * for bulk data file with the matching studyInstanceUID and UUID.
     * 
     * @param studyInstanceUID
     * @param uuid
     * @return The full pathname to the file which contains or will contain the
     *         bulk data item.
     */
    File getBulkDataFilePath(DicomUID studyInstanceUID, UUID bulkDataUUID) {
        // TODO Update this part to consider EntryFS
        return new File(getStudyFolderPath(studyInstanceUID), bulkDataUUID.toString()
                + BULK_DATA_FILE_EXTENSION);
    }

    /**
     * This method creates a new version of a study. If no version of the study
     * exists it creates a new version with versionNumber zero. If a version
     * already exists this creates a new version of the study with the
     * versionNumber incremented by one.
     * 
     * @param version
     *            The Study from which the version is created.
     * @return The UUID of the version that is written to the Store
     * @throws IOException
     */
    public UUID writeVersion(Study version) throws IOException {
        if (version == null) {
            throw new InvalidParameterException("Study version cannot be null!");
        }
        StudyEntry entry = directory.lookup(version.getStudyInstanceUID(), true);
        // If there is a deleted entry, throw exception
        if (entry != null && !entry.deleteType.equals(DeleteType.Live)) {
            throw new VersionPreviouslyDeletedException();
        }

        if (version.getVersionType() == VersionType.New) {
            // If there is no entry, add new entry to the directory
            if (entry == null) {
                entry = new StudyEntryFS(this, version);
                directory.insert(entry);
                return entry.writeVersion(version);
            } else {
                // If there is a non deleted entry, throw exception
                throw new VersionAlreadyExistsException();
            }
        } else {
            // If there is no entry found, throw exception
            if (entry == null) {
                throw new VersionDoesNotExistException();
            } else if (version.getVersionType() == VersionType.Unchanged) {
                return null;
            } else {
                return entry.writeVersion(version);
            }
        }
    }

    @Override
    public boolean insertStudyEntry(TransportableStudyEntry transportableStudyEntry) {
        // TODO: check for consistency of entry
        StudyEntry studyEntry = new StudyEntryFS(this, transportableStudyEntry);
        StudyEntry previousEntry = directory.lookup(transportableStudyEntry.getStudyInstanceUID());
        if (previousEntry != null && !studyEntry.contains(previousEntry)) {
            return false;
        }
        directory.insert(studyEntry);
        return true;
    }

    /**
     * File system based input stream to read a version
     */
    @Override
    public InputStream getVersionInputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException {
        File file = getVersionFilePath(studyInstanceUID, uuid);
        return new BufferedInputStream(new FileInputStream(file));
    }

    /**
     * File system based input stream to read a bulk data
     */
    @Override
    public InputStream getBulkDataInputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException {
        File file = getBulkDataFilePath(studyInstanceUID, uuid);
        return new BufferedInputStream(new FileInputStream(file));
    }

    /**
     * File system based output stream to write a version to the store
     */
    @Override
    public OutputStream getVersionOutputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException {
        File file = getVersionFilePath(studyInstanceUID, uuid);
        file.getParentFile().mkdirs();
        return new BufferedOutputStream(new FileOutputStream(file));
    }

    /**
     * File system based output stream to write a bulk data item to the store
     */
    @Override
    public OutputStream getBulkDataOutputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException {
        File file = getBulkDataFilePath(studyInstanceUID, uuid);
        file.getParentFile().mkdirs();
        return new BufferedOutputStream(new FileOutputStream(file));
    }

    @Override
    public UUID createBulkDataItem(DicomUID studyInstanceUID) {
        return UUID.randomUUID();
    }

    @Override
    public long getVersionFileSize(UUID uuid, DicomUID studyInstanceUID) throws IOException {
        File file = getVersionFilePath(studyInstanceUID, uuid);
        return file.length();
    }

    @Override
    public long getBulkDataFileSize(UUID uuid, DicomUID studyInstanceUID) throws IOException {
        File file = getBulkDataFilePath(studyInstanceUID, uuid);
        return file.length();
    }

}
