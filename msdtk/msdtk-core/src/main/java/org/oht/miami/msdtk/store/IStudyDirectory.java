/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.util.DicomUID;

// TODO fill in the link below.
/**
 * The Interface to the MSD Store study directory.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu),
 * @author Josh Stephens (joshua.stephens@harris.com)
 * 
 * @see "http://ohtmiami.atlassian.net/MSD-Store.docx"
 */
// TODO fix docx reference above
public interface IStudyDirectory {

    /**
     * Looks up the StudyEntry that corresponds to the DicomUID, which must be
     * unique, and returns it. Will not return a study marked as deleted.
     * 
     * @param studyInstanceUid
     *            A DICOM DicomUID containing the studyInstanceUID of the Study
     *            that is being looked up.
     * @return A StudyEntry that corresponds to the DicomUID or Null if the
     *         studyInstnaceUID isn't in the table.
     */
    abstract StudyEntry lookup(DicomUID studyInstanceUID);

    /**
     * Looks up the StudyEntry that corresponds to the DicomUID, which must be
     * unique, and returns it. Will allow studies marked deleted to be returned.
     * 
     * @param studyUID
     *            A DICOM DicomUID containing the studyInstanceUID of the Study
     *            that is being looked up.
     * @param includeDeleted
     *            If true will return studies in the directory that have been marked deleted
     * @return A StudyEntry that corresponds to the DicomUID or Null if the
     *         studyInstnaceUID isn't in the table.
     */
    abstract StudyEntry lookup(DicomUID studyUID, boolean includeDeleted);

    /**
     * Inserts a StudyEntry into the StudyDirectory. Throws an
     * IllegalStateException if the entry already exists.
     * 
     * @param entry
     *            The StudyEntry to be entered into the StudyDirectory
     */
    abstract void insert(StudyEntry entry);

    /**
     * Removes an entry from the Study Directory. Throws an
     * IllegalStateException if no entry exists. This is an internal procedure
     * that should only be called by the system or an administrator.
     * 
     * @param entry
     *            The StudyEntry to be deleted from the StudyDirectory.
     */
    abstract void delete(StudyEntry entry);


    /**
     * Removes an entry from the Study Directory. Throws an
     * IllegalStateException if no entry exists.
     * 
     * @param studyInstanceUID
     *            The The DicomUID of the StudyEntry to be deleted from the
     *            StudyDirectory.
     */
    abstract void delete(DicomUID studyInstanceUid);
    
    // TODO add a search capability
    // public List<StudyEntry> search(String searchCriteria);

}
