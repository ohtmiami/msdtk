/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.crypto.StudyCryptoException;
import org.oht.miami.msdtk.io.StudyParser;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.FileUtils;

import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.util.CloseUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.UUID;

/**
 * A StudyEntry contains all the information necessary to store and retrieve
 * Studies and their associated BulkDataSets. It is a thread-safe class; only
 * one thread can manipulate a StudyEntry or its substructure at a time. The
 * StudyEntry object is stored in the StudyDirectory and handles all MSD Store
 * IO-operations.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 */
class StudyEntryFS extends StudyEntry {

    // Constants

    private final StoreFS storeFS;

    // Constructors

    StudyEntryFS(StoreFS storeFS, Study version) {
        super(version);
        this.storeFS = storeFS;
    }

    StudyEntryFS(StoreFS storeFS, TransportableStudyEntry transportableStudyEntry) {
        super(transportableStudyEntry);
        this.storeFS = storeFS;
    }

    // Methods

    /**
     * This method reads the latest version of a study.
     */
    @Override
    synchronized Study readVersion() throws IOException {
        return internalReadVersion(versionHistory.getMostRecentVersionInfo());
    }

    /**
     * This method reads a specific version of a study based on the
     * versionNumber argument.
     */
    @Override
    synchronized Study readVersion(int versionNumber) throws IOException {
        VersionInfo vInfo;
        try {
            vInfo = versionHistory.getVersionInfo(versionNumber);
        } catch (IndexOutOfBoundsException ex) {
            throw new InvalidVersionNumberException();
        }
        return internalReadVersion(vInfo);
    }

    /**
     * InternalReadVersion is a private method that reads a stored version,
     * decodes, i.e. parses, it into a Study, which it returns.
     * 
     * @param versionInfo
     *            The VersionInfo for the stored study
     * @return Study The newly created study that corresponds to the stored
     *         version
     * @throws IOException
     */
    private Study internalReadVersion(VersionInfo versionInfo) throws IOException {
        File versionFile = storeFS.getVersionFilePath(this.studyInstanceUID, versionInfo.getUUID());
        FileChannel versionChannel = FileChannel
                .open(versionFile.toPath(), StandardOpenOption.READ);
        ByteBuf encrypted = this.cipher.allocateBufferForEncryptedData((int) versionFile.length());
        try {
            encrypted.writeBytes(versionChannel, encrypted.writableBytes());
        } finally {
            CloseUtils.safeClose(versionChannel);
        }
        ByteBuf decrypted = null;
        try {
            this.cipher.initDecrypt(this.studyInstanceUID);
            decrypted = this.cipher.doFinal(encrypted);
        } catch (StudyCryptoException e) {
            throw new IOException(e);
        }
        DicomInputStream dicomIn = new DicomInputStream(new ByteBufInputStream(decrypted));
        Study study = new Study(storeFS, this.studyInstanceUID);
        study.setVersionType(VersionType.Unchanged);
        StudyParser parser = new StudyParser(storeFS);
        try {
            parser.parse(dicomIn, study);
        } finally {
            CloseUtils.safeClose(dicomIn);
        }
        updateLastAccess(LastAccessType.Read);
        return study;
    }

    @Override
    protected void writeToStore(ByteBuf versionBuffer, UUID versionUUID) throws IOException {
        File versionFile = storeFS.getVersionFilePath(this.studyInstanceUID, versionUUID);
        FileChannel versionChannel = FileChannel.open(versionFile.toPath(),
                StandardOpenOption.WRITE, StandardOpenOption.CREATE_NEW);
        try {
            versionBuffer.readBytes(versionChannel, versionBuffer.readableBytes());
        } finally {
            CloseUtils.safeClose(versionChannel);
        }
    }

    /**
     * This method does either soft or hard deletes of the entire study. If the
     * delete is "Soft" then the StudyEntry for this study is marked with
     * DeleteType.Soft. If the delete is hard then then all it's versions and
     * BulkDataSet is deleted, but the StudyEntry is not deleted, but rather
     * marked with DeleteType.Hard, which means that is exists but cannot be
     * read in the normal manner.
     * 
     * @throws IOException
     */
    @Override
    synchronized void deleteStudy(DeleteType type) throws IOException {
        deleteType = type;
        if (type == DeleteType.Soft) {
            return;
        }
        if (type == DeleteType.Hard) {
            FileUtils.deleteFile(storeFS.getStudyFolderPath(studyInstanceUID));
        }
    }

    /**
     * Delete study version file
     * 
     * @param uuid
     *            of file stored
     */
    void deleteVersionFile(UUID uuid) throws InvalidVersionNumberException {
        File versionFile = storeFS.getVersionFilePath(studyInstanceUID, uuid);
        versionFile.delete();
    }
}
