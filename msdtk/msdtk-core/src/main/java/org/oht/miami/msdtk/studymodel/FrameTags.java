/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import org.dcm4che2.data.Tag;

import java.util.HashMap;
import java.util.Map;

/**
 * Class holds the list of attributes defined to be frame tags. Reference:
 * PS(3.3), tables
 * 
 * @author Mahmoud Ismail (maismail@cs.jhu.edu)
 */

public abstract class FrameTags {
   
    /* Map of all possible frame tags and the associated description */
    final static Map<Integer, String> map = new HashMap<Integer, String>(64);
    /* Flag to ensure the list of frame tags are inserted in Map */
    static boolean isInitialized = false;

    private static void initialize() {
        isInitialized = true;
        // Add all possible frame Tags
        map.put(Tag.FrameContentSequence, "Frame Content Sequence");
        map.put(Tag.PlanePositionSequence, "Plane Position Sequence");
        map.put(Tag.PlaneOrientationSequence, "Plane Orientation Sequence");
        map.put(Tag.PixelMeasuresSequence, "Pixel Measures Sequence");
    }

    /* Function to check if a certain tag is a frame tag or not */
    public static boolean isFrameTag(int tag) {
        if (!isInitialized) {
            initialize();
        }
        return map.containsKey(tag);
    }
}
