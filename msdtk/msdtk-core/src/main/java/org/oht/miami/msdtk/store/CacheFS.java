/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 * The CacheFS class extends the Store to provide a file system based caching
 * store.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 */
public class CacheFS extends Store {

    private final String rootDirectory;

    public CacheFS(String rootDir) {
        rootDirectory = rootDir;
    }

    public String getRootDirectory() {
        return rootDirectory;
    }

    public UUID writeVersion(Study version) throws IOException {
        // TODO fix this implementation
        if (directory.lookup(version.getStudyInstanceUID()) != null) {
            throw new VersionAlreadyExistsException();
        } else {
            // StudyEntryFS entry = new StudyEntryFS(version);
            // return entry.writeNewVersion(this, version);
        }
        return null;
    }

    @Override
    public boolean insertStudyEntry(TransportableStudyEntry transportableStudyEntry) {
        // TODO Auto-generated method stub
        return false;

    }

    @Override
    public InputStream getVersionInputStream(UUID uuid, DicomUID studyInstanceUID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InputStream getBulkDataInputStream(UUID uuid, DicomUID studyInstanceUID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OutputStream getVersionOutputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OutputStream getBulkDataOutputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UUID createBulkDataItem(DicomUID studyInstanceUID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getVersionFileSize(UUID uuid, DicomUID studyInstanceUID) throws IOException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getBulkDataFileSize(UUID uuid, DicomUID studyInstanceUID) throws IOException {
        // TODO Auto-generated method stub
        return 0;
    }

}
