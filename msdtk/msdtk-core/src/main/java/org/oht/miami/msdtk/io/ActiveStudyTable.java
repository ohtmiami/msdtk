/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.io;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalListeners;
import com.google.common.cache.RemovalNotification;

/**
 * The active study table holds the studies being parsed by {@code StudyParser}.
 * If the contents of a study stay unchanged for some time, the study will be
 * timed out and removed from this table.
 * 
 * @author Raphael Yu Ning
 */
public class ActiveStudyTable extends CacheLoader<DicomUID, Study> implements
        RemovalListener<DicomUID, Study> {

    /**
     * Logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ActiveStudyTable.class);

    /**
     * The collection of the studies being parsed, implemented as a Guava
     * {@code LoadingCache}, using the Study Instance UID as the key and
     * {@code Study} as the value for each cache entry.
     */
    protected final LoadingCache<DicomUID, Study> activeStudies;

    /**
     * The MSD-Store that is searched when a requested study is not yet in
     * {@code activeStudies}.
     */
    protected final Store store;

    /**
     * The custom handler that is called when a study is timed out and about to
     * be removed from {@code activeStudies}.
     */
    protected final StudyTimeoutHandler timeoutHandler;

    /**
     * Constructs an {@code ActiveStudyTable} with all parameters configurable.
     * 
     * @param store
     *            The MSD-Store that is searched when a requested study is not
     *            yet in {@code activeStudies}. Must not be {@code null}.
     * @param timeoutDuration
     *            The study timeout duration.
     * @param timeoutUnit
     *            The unit that {@code timeoutDuration} is expressed in.
     * @param timeoutHandler
     *            The custom handler that is called when a study is timed out
     *            and about to be removed from {@code activeStudies}. If
     *            {@code null}, only the default operations are performed when a
     *            study is timed out.
     * @param timeoutHandlerExecutor
     *            The {@code Executor} with which {@code timeoutHandler} will be
     *            executed. If {@code null}, {@code timeoutHandler} will be
     *            executed synchronously.
     */
    public ActiveStudyTable(Store store, long timeoutDuration, TimeUnit timeoutUnit,
            StudyTimeoutHandler timeoutHandler, Executor timeoutHandlerExecutor) {

        if (store == null) {
            throw new IllegalArgumentException("Store must not be null");
        }
        this.store = store;
        this.timeoutHandler = timeoutHandler;
        RemovalListener<DicomUID, Study> timeoutListener = timeoutHandlerExecutor == null ? this
                : RemovalListeners.asynchronous(this, timeoutHandlerExecutor);
        activeStudies = CacheBuilder.newBuilder().expireAfterAccess(timeoutDuration, timeoutUnit)
                .removalListener(timeoutListener).build(this);
    }

    /**
     * Constructs an {@code ActiveStudyTable} that executes
     * {@code timeoutHandler} synchronously. Should not be used if costly
     * operations need to be performed when a study is timed out.
     * 
     * @param store
     *            The MSD-Store that is searched when a requested study is not
     *            yet in {@code activeStudies}. Must not be {@code null}.
     * @param timeoutDuration
     *            The study timeout duration.
     * @param timeoutUnit
     *            The unit that {@code timeoutDuration} is expressed in.
     * @param timeoutHandler
     *            The custom handler that is called when a study is timed out
     *            and about to be removed from {@code activeStudies}. If
     *            {@code null}, only the default operations are performed when a
     *            study is timed out.
     * @see #ActiveStudyTable(Store, long, TimeUnit, StudyTimeoutHandler,
     *      Executor)
     */
    public ActiveStudyTable(Store store, long timeoutDuration, TimeUnit timeoutUnit,
            StudyTimeoutHandler timeoutHandler) {

        this(store, timeoutDuration, timeoutUnit, timeoutHandler, null);
    }

    /**
     * Getter for {@code store}.
     * 
     * @return {@code store}.
     * @see #store
     */
    public Store getStore() {

        return store;
    }

    /**
     * Searches {@code activeStudies} for a study. If not found, tries to find
     * an existing version of the study in {@code store} and adds it to
     * {@code activeStudies}. If the study is not yet stored, creates a new
     * {@code Study} and adds it to {@code activeStudies}.
     * 
     * @param studyInstanceUID
     *            The Study Instance UID of the study to look up.
     * @return The {@code Study} that is either found in or added to
     *         {@code activeStudies}.
     * @throws IOException
     */
    public Study findOrCreateStudy(DicomUID studyInstanceUID) throws IOException {

        try {
            return activeStudies.get(studyInstanceUID);
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                throw new RuntimeException("Unexpected error while loading study "
                        + studyInstanceUID + " to the active study table", cause);
            }
        }
    }

    /**
     * This method is called implicitly by {@code findOrCreateStudy} when the
     * requested study is not in {@code activeStudies}. It either finds the
     * study in {@code store} or creates the study. The {@code Study} it returns
     * will be automatically added to {@code activeStudies}.
     * 
     * @param studyInstanceUID
     *            The Study Instance UID of the study to be added to
     *            {@code activeStudies}.
     * @return The {@code Study} to be added to {@code activeStudies}.
     * @see #findOrCreateStudy(DicomUID)
     */
    @Override
    public Study load(DicomUID studyInstanceUID) throws Exception {

        return store.findOrCreateStudy(studyInstanceUID);
    }

    /**
     * This method is called when a study in the active study table is timed
     * out. It saves the study's bulk data to {@code store}, then calls
     * {@code timeoutHandler}. Whether this method is executed synchronously or
     * asynchronously depends on how this instance of {@code ActiveStudyTable}
     * is constructed.
     * 
     * @param notification
     *            Gives the study that has been timed out.
     * @see StudyTimeoutHandler#onStudyTimeout(Study)
     */
    @Override
    public void onRemoval(RemovalNotification<DicomUID, Study> notification) {

        Study study = notification.getValue();
        try {
            study.getBulkDataSet().close();
        } catch (IOException e) {
            LOG.warn("Error while saving bulk data for study " + study.getStudyInstanceUID(), e);
        }
        if (timeoutHandler != null) {
            try {
                timeoutHandler.onStudyTimeout(study);
            } catch (Exception e) {
                LOG.error("Error in StudyTimeoutHandler for study " + study.getStudyInstanceUID(),
                        e);
            }
        }
    }

    /**
     * Clears the active study table. This method is useful for timing out all
     * active studies manually.
     */
    public void clear() {

        activeStudies.invalidateAll();
    }

    /**
     * Calls {@code activeStudies.cleanUp()} to check for expired studies and
     * time them out.
     * 
     * @see LoadingCache#cleanUp()
     */
    public void cleanUp() {

        activeStudies.cleanUp();
    }

    /**
     * Defines the interface for custom study timeout handlers.
     * 
     * @author Raphael Yu Ning
     */
    public interface StudyTimeoutHandler {

        /**
         * This method is called when a study in the active study table is timed
         * out, after the default operations are performed.
         * 
         * @param study
         *            The study that has been timed out.
         * @throws Exception
         * @see ActiveStudyTable#onRemoval(RemovalNotification)
         */
        void onStudyTimeout(Study study) throws Exception;
    }
}
