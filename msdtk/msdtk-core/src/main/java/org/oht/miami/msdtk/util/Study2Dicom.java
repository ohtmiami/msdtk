/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.util;

import org.oht.miami.msdtk.conversion.dicom.Instance2Dicom;
import org.oht.miami.msdtk.conversion.dicom.Series2Dicom;
import org.oht.miami.msdtk.conversion.dicom.Study2DicomUtil;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Series;
import org.oht.miami.msdtk.studymodel.Study;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomOutputStream;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Utility for converting from study model to dcm4che DICOM objects. Author
 * Mahmoud Ismail (maismail@cs.jhu.edu)
 */
public class Study2Dicom {

    /**
     * Writes a study model to a secondary storage device in single frame Dicom
     * (SFD) format.
     * 
     * @param study
     *            the Study to convert
     * @param outputDir
     *            the directory to write to
     * @param withMultipleFrames
     *            Boolean flag, true implies creating a dicom object for each
     *            child frame in the input instance, false implies creating one
     *            output dicom object per input instance, child instance frames
     *            are added as child dicom object to the output dicom object.
     * @throws IOException
     */
    public static void study2SingleFrameDicom(Study study, File outputDir,
            boolean withMultipleFrames) throws IOException {
        Iterator<DicomElement> seriesAttrIter;
        Iterator<Series> seriesIter;
        Iterator<Instance> instanceIter;
        DicomObject studyDcmObj = org.oht.miami.msdtk.conversion.dicom.Study2Dicom
                .getStudyLevelAttributesAsDicomObject(study);
        /* Adds all series level attributes on top of base object */
        seriesIter = study.seriesIterator();
        while (seriesIter.hasNext()) {
            Series series = seriesIter.next();
            DicomObject seriesDcmObj = new BasicDicomObject();
            Study2DicomUtil.copyDicomObject(studyDcmObj, seriesDcmObj);
            seriesAttrIter = series.attributeIterator();
            while (seriesAttrIter.hasNext()) {
                Instance2Dicom.convertAttribute(seriesAttrIter.next(), seriesDcmObj);
            }
            instanceIter = series.instanceIterator();
            /*
             * Adds all instance level attribute on top of study and series
             * attributes
             */
            while (instanceIter.hasNext()) {
                Instance instance = instanceIter.next();
                /*
                 * Writes DicomObject out to file Ensures that SOPInstanceUID
                 * and Media storage instance UID values are the same
                 */
                instance.updateMediaStorage();
                ArrayList<DicomObject> instanceDcmObjs = Instance2Dicom.instance2SingleFrameDicom(
                        seriesDcmObj, instance, withMultipleFrames);
                for (DicomObject instanceDcmObj : instanceDcmObjs) {
                    /* Write DicomObject out to file */
                    String sopInstanceUID = instanceDcmObj.getString(Tag.SOPInstanceUID);
                    File dcmFile = new File(outputDir, sopInstanceUID + ".dcm");
                    DicomOutputStream out = new DicomOutputStream(dcmFile);
                    try {
                        // writing a single DICOM file
                        out.writeDicomFile(instanceDcmObj);
                    } finally {
                        out.close();
                    }
                }
            }
        }
    }

    /**
     * Converts each series within a study into a MULTIFRAME Dicom object and
     * writes it to secondary storage media. The function creates a dicom object
     * per series.
     * 
     * @param study
     *            the Study to convert
     * @param outputDir
     *            the directory to write to
     * @throws IOException
     */
    public static void study2MultiFrameDicom(final Study study, final File outputDir)
            throws IOException {

        DicomObject studyDcmObj = org.oht.miami.msdtk.conversion.dicom.Study2Dicom
                .getStudyLevelAttributesAsDicomObject(study);

        /* Add all series level attributes on top of base object */
        Iterator<Series> seriesIter = study.seriesIterator();
        while (seriesIter.hasNext()) {
            Series series = seriesIter.next();
            ArrayList<DicomObject> seriesDcmObjs = Series2Dicom.series2MultiFrameDicom(series,
                    studyDcmObj);
            int index = 1;
            for (DicomObject seriesDcmObj : seriesDcmObjs) {
                /* Write DicomObject out to file */
                String fileNameSuffix = series.getSeriesInstanceUID();
                File dcmFile = new File(outputDir, fileNameSuffix + "_" + index + "_MultiFrame.dcm");
                DicomOutputStream out = new DicomOutputStream(dcmFile);
                try {
                    out.writeDicomFile(seriesDcmObj);
                } finally {
                    out.close();
                }
                index++;
            }
        }
    }

    /**
     * Converts a study model to a multi-series dicom object
     * 
     * @param study
     *            , the study model to be converted.
     * @param outputDir
     *            , the location where the created dicom object will be saved.
     * @throws IOException
     */
    public static void study2MultiSeriesDicom(final Study study, final File outputDir)
            throws IOException {
        File dcmFile = new File(outputDir, study.getStudyInstanceUIDAsString()
                + "StudyMultiFrame.dcm");
        DicomOutputStream out = new DicomOutputStream(dcmFile);
        try {
            out.writeDicomFile(org.oht.miami.msdtk.conversion.dicom.Study2Dicom
                    .study2UnifiedMultiSeriesDicom(study));
        } finally {
            out.close();
        }
    }
}
