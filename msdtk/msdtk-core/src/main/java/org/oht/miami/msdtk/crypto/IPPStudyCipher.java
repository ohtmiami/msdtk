/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.crypto;

import org.oht.miami.ipp4j.cp.IPPAESGCMBehavior;
import org.oht.miami.ipp4j.cp.IPPAESKeyLength;
import org.oht.miami.ipp4j.cp.IPPCrypto;
import org.oht.miami.ipp4j.cp.IPPCryptoException;
import org.oht.miami.ipp4j.dc.IPPZlib;
import org.oht.miami.ipp4j.dc.IPPZlibException;
import org.oht.miami.ipp4j.util.DirectBuffers;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class IPPStudyCipher extends StudyCipher {

    private static final Logger LOG = LoggerFactory.getLogger(IPPStudyCipher.class);

    private static final byte[] GLOBAL_SECRET = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
                    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14,
                    0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F };

    private static final Pointer P_GLOBAL_SECRET = DirectBuffers
            .allocateAligned64(GLOBAL_SECRET.length);

    // 128B is enough for holding any key message
    private static final int KEY_MESSAGE_SIZE = 128;

    // 1MB
    private static final int CHUNK_SIZE = 1 << 20;

    private final IPPAESGCMBehavior behavior;

    private Pointer pContext;

    private Pointer pKeyMessage;

    private Pointer pKey;

    private Pointer pAAD;

    private ByteBuffer aadBuffer;

    private Pointer pIV;

    private byte[] ivBytes;

    private Pointer pMAC;

    private ByteBuffer macBuffer;

    private ByteBuf mac;

    private ByteBuf inputMAC;

    private Pointer pDecryptedChunk;

    private ByteBuffer decryptedChunkBuffer;

    private Random random;

    static {

        P_GLOBAL_SECRET.write(0, GLOBAL_SECRET, 0, GLOBAL_SECRET.length);
    }

    public IPPStudyCipher(IPPAESGCMBehavior behavior) {

        this.behavior = behavior;
        pContext = null;
        pKeyMessage = DirectBuffers.allocateAligned64(KEY_MESSAGE_SIZE);
        // Allocate 32B = 256b for SHA256
        pKey = DirectBuffers.allocateAligned64(32);
        pAAD = DirectBuffers.allocateAligned64(StudyCipher.AAD_SIZE);
        aadBuffer = pAAD.getByteBuffer(0, StudyCipher.AAD_SIZE);
        pIV = pAAD.share(StudyCipher.HEADER_SIZE);
        ivBytes = new byte[StudyCipher.IV_SIZE];
        pMAC = DirectBuffers.allocateAligned64(StudyCipher.MAC_SIZE);
        macBuffer = pMAC.getByteBuffer(0, StudyCipher.MAC_SIZE);
        mac = Unpooled.wrappedBuffer(macBuffer);
        inputMAC = null;
        pDecryptedChunk = DirectBuffers.allocateAligned64(CHUNK_SIZE);
        decryptedChunkBuffer = pDecryptedChunk.getByteBuffer(0, CHUNK_SIZE);
        random = new SecureRandom();
    }

    public IPPStudyCipher() {

        this(IPPAESGCMBehavior.DEFAULT);
    }

    @Override
    protected ByteBuf allocateBuffer(int capacity) {

        Memory memoryBlock = DirectBuffers.allocateAligned64(capacity);
        return Unpooled.wrappedBuffer(memoryBlock.getByteBuffer(0, capacity)).clear();
    }

    private void prepareKey(DicomUID studyInstanceUID) throws StudyCryptoException {

        String keyMessage = Study.IMPLEMENTATION_VERSION_NAME + studyInstanceUID;
        pKeyMessage.setString(0, keyMessage);
        // Assume there are no wide characters in keyMessage
        int keyMessageSize = keyMessage.length();
        try {
            IPPCrypto.hmacSHA256MessageDigest(pKeyMessage, keyMessageSize, P_GLOBAL_SECRET,
                    GLOBAL_SECRET.length, pKey, 32);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
    }

    private void prepareIV() {

        random.nextBytes(ivBytes);
        pIV.write(0, ivBytes, 0, StudyCipher.IV_SIZE);
    }

    private void prepareHeader() throws StudyCryptoException {

        ByteBuffer headerBuffer = pAAD.getByteBuffer(0, StudyCipher.HEADER_SIZE);
        ByteBuf header = Unpooled.wrappedBuffer(headerBuffer);
        header.writerIndex(0);
        try {
            this.encryptionInfo.save(header);
        } catch (IOException e) {
            throw new StudyCryptoException(e);
        }
    }

    @Override
    protected void init(OperationMode mode, DicomUID studyInstanceUID) throws StudyCryptoException {

        super.init(mode, studyInstanceUID);
        long keyPrepStart = System.nanoTime();
        prepareKey(studyInstanceUID);
        long contextInitStart = System.nanoTime();
        try {
            if (pContext == null) {
                IntByReference pSize = new IntByReference();
                IPPCrypto.rijndael128GCMGetSizeManaged(behavior, pSize);
                pContext = DirectBuffers.allocateAligned64(pSize.getValue());
            }
            IPPCrypto.rijndael128GCMInitManaged(behavior, pKey, IPPAESKeyLength._128, pContext);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
        long contextInitEnd = System.nanoTime();
        LOG.trace("{}\t{}\t", contextInitStart - keyPrepStart, contextInitEnd - contextInitStart);
    }

    @Override
    public void initEncrypt(DicomUID studyInstanceUID, boolean compress, UUID uuid, long length)
            throws StudyCryptoException {

        super.initEncrypt(studyInstanceUID, compress, uuid, length);
        long ivPrepStart = System.nanoTime();
        prepareIV();
        long headerPrepStart = System.nanoTime();
        prepareHeader();
        long aeadInitStart = System.nanoTime();
        try {
            IPPCrypto.rijndael128GCMStart(pIV, StudyCipher.IV_SIZE, pAAD, StudyCipher.AAD_SIZE,
                    pContext);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
        long aeadInitEnd = System.nanoTime();
        LOG.trace("{}\t{}\t{}\t", new Object[] { headerPrepStart - ivPrepStart,
                        aeadInitStart - headerPrepStart, aeadInitEnd - aeadInitStart });
    }

    @Override
    public ByteBuf getKey() {

        byte[] keyBytes = new byte[StudyCipher.KEY_SIZE];
        pKey.read(0, keyBytes, 0, StudyCipher.KEY_SIZE);
        return Unpooled.wrappedBuffer(keyBytes);
    }

    private void processAAD(ByteBuf input) throws StudyCryptoException {

        long aadParsingStart = System.nanoTime();
        ByteBuffer inputBuffer = input.nioBuffer();
        ByteBuf aad = input.readSlice(StudyCipher.AAD_SIZE);
        try {
            this.encryptionInfo.load(aad);
        } catch (IOException e) {
            throw new StudyCryptoException(e);
        }
        long aeadInitStart = System.nanoTime();
        Pointer pInputAAD = Native.getDirectBufferPointer(inputBuffer);
        Pointer pInputIV = pInputAAD.share(StudyCipher.HEADER_SIZE);
        try {
            IPPCrypto.rijndael128GCMStart(pInputIV, StudyCipher.IV_SIZE, pInputAAD,
                    StudyCipher.AAD_SIZE, pContext);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
        long aeadInitEnd = System.nanoTime();
        LOG.trace("{}\t{}\t", aeadInitStart - aadParsingStart, aeadInitEnd - aeadInitStart);
    }

    @Override
    protected ByteBuf updateEncrypt(ByteBuf input) throws StudyCryptoException {

        if (!input.isDirect()) {
            throw new StudyCryptoException("Input buffer must be a direct buffer");
        }
        long compressionStart = System.nanoTime();
        ByteBuf unencrypted = input.readSlice(input.readableBytes());
        ByteBuffer unencryptedBuffer = unencrypted.nioBuffer();
        if (this.encryptionInfo.isCompressed()) {
            try {
                unencryptedBuffer = IPPZlib.compress(unencryptedBuffer, this.compressionLevel);
            } catch (IPPZlibException e) {
                throw new StudyCryptoException(e);
            }
        }
        long encryptionStart = System.nanoTime();
        ByteBuffer encryptedBuffer = null;
        try {
            encryptedBuffer = IPPCrypto.rijndael128GCMEncrypt(unencryptedBuffer, pContext);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
        ByteBuf encrypted = Unpooled.wrappedBuffer(aadBuffer, encryptedBuffer);
        long encryptionEnd = System.nanoTime();
        LOG.trace("{}\t{}\t{}\t", new Object[] { encryptionStart - compressionStart,
                        unencryptedBuffer.remaining(), encryptionEnd - encryptionStart });
        return encrypted;
    }

    @Override
    protected ByteBuf updateDecrypt(ByteBuf input) throws StudyCryptoException {

        if (!input.isDirect()) {
            throw new StudyCryptoException("Input buffer must be a direct buffer");
        }
        processAAD(input);
        long decryptionStart = System.nanoTime();
        ByteBuf encrypted = input.readSlice(input.readableBytes() - StudyCipher.MAC_SIZE);
        inputMAC = input.readSlice(StudyCipher.MAC_SIZE);
        ByteBuffer decryptedBuffer = null;
        try {
            decryptedBuffer = IPPCrypto.rijndael128GCMDecrypt(encrypted.nioBuffer(), pContext);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
        long decompressionStart = System.nanoTime();
        ByteBuf decrypted = null;
        if (!this.encryptionInfo.isCompressed()) {
            decrypted = Unpooled.wrappedBuffer(decryptedBuffer);
        } else {
            int uncompressedSize = (int) this.encryptionInfo.getLength();
            Pointer pDecompressed = DirectBuffers.allocateAligned64(uncompressedSize);
            ByteBuffer decompressedBuffer = pDecompressed.getByteBuffer(0, uncompressedSize);
            try {
                IPPZlib.decompress(decryptedBuffer, decompressedBuffer);
            } catch (IPPZlibException e) {
                throw new StudyCryptoException(e);
            }
            decrypted = Unpooled.wrappedBuffer(decompressedBuffer);
        }
        long decompressionEnd = System.nanoTime();
        LOG.trace("{}\t{}\t", decompressionStart - decryptionStart, decompressionEnd
                - decompressionStart);
        return decrypted;
    }

    @Override
    protected ByteBuf updateAuthenticate(ByteBuf input) throws StudyCryptoException {

        if (!input.isDirect()) {
            throw new StudyCryptoException("Input buffer must be a direct buffer");
        }
        ByteBuf authenticated = input.duplicate();
        processAAD(input);
        long authenticationStart = System.nanoTime();
        ByteBuf encrypted = input.readSlice(input.readableBytes() - StudyCipher.MAC_SIZE);
        inputMAC = input.readSlice(StudyCipher.MAC_SIZE);
        while (encrypted.readable()) {
            int bytesToAuthenticate = Math.min(encrypted.readableBytes(), CHUNK_SIZE);
            ByteBuf encryptedChunk = encrypted.readSlice(bytesToAuthenticate);
            try {
                IPPCrypto.rijndael128GCMDecrypt(encryptedChunk.nioBuffer(), decryptedChunkBuffer,
                        pContext);
            } catch (IPPCryptoException e) {
                throw new StudyCryptoException(e);
            }
            decryptedChunkBuffer.clear();
        }
        long authenticationEnd = System.nanoTime();
        LOG.trace("{}\t", authenticationEnd - authenticationStart);
        return authenticated;
    }

    @Override
    protected ByteBuf finish() throws BadMACException, StudyCryptoException {

        long macComputationStart = System.nanoTime();
        try {
            IPPCrypto.rijndael128GCMGetTag(pMAC, StudyCipher.MAC_SIZE, pContext);
        } catch (IPPCryptoException e) {
            throw new StudyCryptoException(e);
        }
        long macValidationStart = System.nanoTime();
        ByteBuf tail = null;
        if (this.mode.isEncrypt()) {
            tail = mac;
        } else {
            if (!mac.equals(inputMAC)) {
                throw new BadMACException();
            }
        }
        long macValidationEnd = System.nanoTime();
        LOG.trace("{}\t{}\t", macValidationStart - macComputationStart, macValidationEnd
                - macValidationStart);
        return tail;
    }
}
