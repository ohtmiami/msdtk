/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.crypto;

import org.oht.miami.msdtk.util.DicomUID;

import java.util.UUID;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public abstract class StudyCipher {

    protected enum OperationMode {

        ENCRYPT_UNCOMPRESSED(true), ENCRYPT_COMPRESSED(true), DECRYPT(false), AUTHENTICATE(false);

        private final boolean encrypt;

        OperationMode(boolean encrypt) {

            this.encrypt = encrypt;
        }

        public boolean isEncrypt() {

            return encrypt;
        }
    }

    public static final int KEY_SIZE = 16;

    public static final int IV_SIZE = 12;

    public static final int AAD_SIZE = 256;

    public static final int HEADER_SIZE = AAD_SIZE - IV_SIZE;

    public static final int MAC_SIZE = 16;

    public static final int ZLIB_HEADER_LEN = 2;

    protected OperationMode mode = null;

    protected StudyCryptoInfo encryptionInfo = null;

    protected int compressionLevel = 6;

    public static StudyCipher getInstance() {

        final String packageName = StudyCipher.class.getPackage().getName();
        final String ippStudyCipherClassName = "IPPStudyCipher";
        try {
            Class<?> ippStudyCipherClass = Class.forName(packageName + "."
                    + ippStudyCipherClassName);
            return (StudyCipher) ippStudyCipherClass.newInstance();
        } catch (Throwable t) {
            return new BCStudyCipher();
        }
    }

    protected abstract ByteBuf allocateBuffer(int capacity);

    public ByteBuf allocateBufferForUnencryptedData(int capacity, boolean compress) {

        if (compress) {
            return allocateBuffer(ZLIB_HEADER_LEN + capacity).writerIndex(ZLIB_HEADER_LEN);
        } else {
            return allocateBuffer(capacity);
        }
    }

    public ByteBuf allocateBufferForEncryptedData(int capacity) {

        return allocateBuffer(capacity);
    }

    protected void init(OperationMode mode, DicomUID studyInstanceUID) throws StudyCryptoException {

        if (studyInstanceUID == null) {
            throw new NullPointerException("studyInstanceUID");
        }
        this.mode = mode;
        encryptionInfo = new StudyCryptoInfo();
    }

    public void initEncrypt(DicomUID studyInstanceUID, boolean compress, UUID uuid, long length)
            throws StudyCryptoException {

        if (compress) {
            init(OperationMode.ENCRYPT_COMPRESSED, studyInstanceUID);
            encryptionInfo.setCompressed(true);
        } else {
            init(OperationMode.ENCRYPT_UNCOMPRESSED, studyInstanceUID);
            encryptionInfo.setCompressed(false);
        }
        encryptionInfo.setUUID(uuid);
        encryptionInfo.setLength(length);
    }

    public void initDecrypt(DicomUID studyInstanceUID) throws StudyCryptoException {

        init(OperationMode.DECRYPT, studyInstanceUID);
    }

    public void initAuthenticate(DicomUID studyInstanceUID) throws StudyCryptoException {

        init(OperationMode.AUTHENTICATE, studyInstanceUID);
    }

    public int getCompressionLevel() {

        return compressionLevel;
    }

    public void setCompressionLevel(int compressionLevel) {

        this.compressionLevel = compressionLevel;
    }

    public abstract ByteBuf getKey();

    public ByteBuf update(ByteBuf input) throws StudyCryptoException {

        if (mode == null) {
            throw new IllegalStateException("Not initialized");
        }
        if (mode.isEncrypt()) {
            return updateEncrypt(input);
        } else if (mode == OperationMode.DECRYPT) {
            return updateDecrypt(input);
        } else {
            return updateAuthenticate(input);
        }
    }

    protected abstract ByteBuf updateEncrypt(ByteBuf input) throws StudyCryptoException;

    protected abstract ByteBuf updateDecrypt(ByteBuf input) throws StudyCryptoException;

    protected abstract ByteBuf updateAuthenticate(ByteBuf input) throws StudyCryptoException;

    public ByteBuf doFinal(ByteBuf input) throws BadMACException, StudyCryptoException {

        ByteBuf output = update(input);
        ByteBuf tail = doFinal();
        if (tail == null) {
            return output;
        } else {
            return Unpooled.wrappedBuffer(output, tail);
        }
    }

    public ByteBuf doFinal() throws BadMACException, StudyCryptoException {

        if (mode == null) {
            throw new IllegalStateException("Not initialized");
        }
        ByteBuf tail = finish();
        mode = null;
        return tail;
    }

    protected abstract ByteBuf finish() throws BadMACException, StudyCryptoException;
}
