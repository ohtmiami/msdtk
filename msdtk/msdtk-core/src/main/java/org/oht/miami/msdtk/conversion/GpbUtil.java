/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.conversion;

import org.oht.miami.msdtk.conversion.StudyProtos.DicomElementGpb;
import org.oht.miami.msdtk.conversion.StudyProtos.ItemGpb;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SequenceDicomElement;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.VR;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.protobuf.ByteString;

public class GpbUtil {
    /**
     * Converts from DicomElement object to GPB object
     * 
     * @param elem
     *            the <code>DicomElement</code> to convert into GPB
     * @return the converted element
     */
    public static DicomElementGpb dicomElementToGPB(final DicomElement elem) {
        DicomElementGpb.Builder builder = DicomElementGpb.newBuilder();
        builder.setBigEndian(elem.bigEndian());
        builder.setVrCode(elem.vr().code());
        builder.setTag(elem.tag());
        if (elem.hasDicomObjects()) {
            // Copies sequence elements
            for (int i = 0; i < elem.countItems(); i++) {
                ItemGpb.Builder itemBuilder = ItemGpb.newBuilder();
                DicomObject obj = elem.getDicomObject(i);
                Iterator<DicomElement> iter = obj.iterator();
                while (iter.hasNext()) {
                    itemBuilder.addSonElements(dicomElementToGPB(iter.next()));
                }
                builder.addNested(itemBuilder.build());
            }
        } else {
            // Copies byte [].
            builder.setValue(ByteString.copyFrom(elem.getBytes()));
        }
        return builder.build();
    }

    /**
     * Converts from GPB back into a <code>DicomElement</code> object
     * 
     * @param gpbData
     *            the GPB element to convert
     * @return the converted element
     */
    public static DicomElement gpbToDicomElement(DicomElementGpb gpbData) {
        /*
         * The VR is constructed using valueOf method This is the same way it is
         * constructed using dcm4che2 read dicom object
         */
        DicomElement elem = null;
        if (gpbData.getNestedCount() > 0) {
            List<Object> dicomObjects = new ArrayList<Object>();
            // read sequence elements
            for (ItemGpb nested : gpbData.getNestedList()) {
                DicomObject obj = new BasicDicomObject();
                for (DicomElementGpb gpbDicomElement : nested.getSonElementsList()) {
                    // add each child element to the object that the sequence
                    // element holds
                    obj.add(gpbToDicomElement(gpbDicomElement));
                }
                dicomObjects.add(obj);
            }
            elem = new SequenceDicomElement(gpbData.getTag(), VR.SQ, gpbData.getBigEndian(),
                    dicomObjects, null);
        } else {
            elem = new SimpleDicomElement(gpbData.getTag(), VR.valueOf(gpbData.getVrCode()),
                    gpbData.getBigEndian(), gpbData.getValue().toByteArray(), null);
        }
        return elem;
    }

}
