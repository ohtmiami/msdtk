/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

/**
 * These are the types of deletion supported by the system for now there are
 * three types: 
 *      Live: The entry and associated versions and BulkData are live,
 *            i.e. not deleted.
 *      Soft: The entry is marked deleted but the versions and BulkData still 
 *            exist.
 *      Hard: The entry still exists by is marked deleted, but the version
 *            and BulkData have been deleted.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 * 
 */

public enum DeleteType {
    Live, Soft, Hard;
}
