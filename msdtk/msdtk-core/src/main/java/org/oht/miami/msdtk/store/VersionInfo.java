/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * A class that encapsulates useful meta information about a particular version
 * of a study.
 * 
 * @author Raphael Yu Ning
 */
public class VersionInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6002056076462808439L;

    /**
     * The UUID of the stored Version
     */
    private final UUID uuid;

    /**
     * The number, starting at zero (0), of the Version being stored.
     */
    private final int versionNumber;

    /**
     * The type of version being stored [New, Append, Modify]
     */
    private final VersionType versionType;

    /**
     * The date and time (including the time zone) this version was created.
     */
    // TODO Should we be using Joda Time or java.util.Date?
    private final Date creationDate;

    /**
     * The compression type of the stored Version (see
     * SupportedCompressionsTypes class)
     */
    private final String compressionType;

    /**
     * Study Encryption Key for both Version and BulkDataSet. If this value is 0
     * then the contents are not encrypted.
     */
    private final long encryptionKey;

    /**
     * This is an integrity seal for the contents of the study Version. If the
     * value is 0 then the contents are not sealed and the seal doesn't need to
     * be checked.
     */
    // TODO what kind of seal to use?
    private final long integritySeal;

    /**
     * This is a flag that says whether the object has been transmitted from
     * cache to LTA.
     */
    private final boolean inLTA = false;

    /**
     * Constructor
     * 
     * @param uuid
     * @param versionNumber
     * @param versionType
     * @param creationDate
     */
    // the Constructor should create the creationDate
    // TODO update to include compression, encryption, and seal
    public VersionInfo(UUID uuid, int versionNumber, VersionType versionType, Date creationDate) {
        if (versionNumber < 0) {
            throw new IllegalArgumentException("Version number must be non-negative");
        }
        this.versionNumber = versionNumber;
        // TODO validate that version type is one of New, Append or Modify.
        this.versionType = versionType;
        this.creationDate = creationDate;
        this.uuid = uuid;
        // TODO: change these to actual values
        compressionType = "";
        encryptionKey = 0;
        integritySeal = 0;

    }

    public int getVersionNumber() {

        return versionNumber;
    }

    public VersionType getType() {

        return versionType;
    }

    public UUID getUUID() {

        return uuid;
    }

    public Date getCreationDate() {

        return creationDate;
    }

    // TODO decide if this should be available outside package
    public String getCompressionType() {

        return compressionType;
    }

    // TODO decide if this should be available outside package
    public long getEncryptionKey() {

        return encryptionKey;
    }

    // TODO decide if this should be available outside package
    public long getIntegritySeal() {

        return integritySeal;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(uuid.toString() + " ");
        sb.append(versionNumber + " ");
        sb.append(creationDate + " ");
        sb.append(compressionType + " ");
        sb.append(versionType.toString() + " \n");
        return sb.toString();

    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((compressionType == null) ? 0 : compressionType.hashCode());
        result = prime * result + ((creationDate == null) ? 0 : creationDate.hashCode());
        result = prime * result + (int) (encryptionKey ^ (encryptionKey >>> 32));
        result = prime * result + (inLTA ? 1231 : 1237);
        result = prime * result + (int) (integritySeal ^ (integritySeal >>> 32));
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        result = prime * result + versionNumber;
        result = prime * result + ((versionType == null) ? 0 : versionType.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        VersionInfo other = (VersionInfo) obj;
        if (compressionType == null) {
            if (other.compressionType != null) {
                return false;
            }
        } else if (!compressionType.equals(other.compressionType)) {
            return false;
        }
        if (creationDate == null) {
            if (other.creationDate != null) {
                return false;
            }
        } else if (!creationDate.equals(other.creationDate)) {
            return false;
        }
        if (encryptionKey != other.encryptionKey) {
            return false;
        }
        if (inLTA != other.inLTA) {
            return false;
        }
        if (integritySeal != other.integritySeal) {
            return false;
        }
        if (uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!uuid.equals(other.uuid)) {
            return false;
        }
        if (versionNumber != other.versionNumber) {
            return false;
        }
        if (versionType != other.versionType) {
            return false;
        }
        return true;
    }

}
