/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.crypto;

import org.oht.miami.msdtk.studymodel.Study;

import org.dcm4che2.util.CloseUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

class StudyCryptoInfo {

    private static final String UUID_PROPERTY = "uuid";

    private static final String COMPRESSED_PROPERTY = "compressed";

    private static final String LENGTH_PROPERTY = "length";

    private static final String TRUE_VALUE = "1";

    private static final String FALSE_VALUE = "0";

    private UUID uuid;

    private boolean compressed;

    private long length;

    public StudyCryptoInfo(UUID uuid, boolean compressed, long length) {

        setUUID(uuid);
        setCompressed(compressed);
        setLength(length);
    }

    public StudyCryptoInfo() {

        this(null, false, -1);
    }

    public UUID getUUID() {

        return uuid;
    }

    public void setUUID(UUID uuid) {

        this.uuid = uuid;
    }

    public boolean isCompressed() {

        return compressed;
    }

    public void setCompressed(boolean compressed) {

        this.compressed = compressed;
    }

    public long getLength() {

        return length;
    }

    public void setLength(long length) {

        this.length = length;
    }

    public Properties toProperties() {

        Properties properties = new Properties();
        if (uuid != null) {
            properties.setProperty(UUID_PROPERTY, uuid.toString());
        }
        properties.setProperty(COMPRESSED_PROPERTY, compressed ? TRUE_VALUE : FALSE_VALUE);
        properties.setProperty(LENGTH_PROPERTY, Long.toString(length));
        return properties;
    }

    public void save(ByteBuf buf) throws IOException {

        if (buf == null) {
            throw new NullPointerException("buf");
        }
        int initialWriterIndex = buf.writerIndex();
        buf.writerIndex(initialWriterIndex + 2);
        ByteBufOutputStream bufOut = new ByteBufOutputStream(buf);
        Properties properties = toProperties();
        try {
            properties.store(bufOut, Study.IMPLEMENTATION_VERSION_NAME);
        } finally {
            CloseUtils.safeClose(bufOut);
        }
        buf.setShort(initialWriterIndex, buf.writerIndex() - initialWriterIndex - 2);
    }

    public void load(ByteBuf buf) throws IOException {

        if (buf == null) {
            throw new NullPointerException("buf");
        }
        int size = buf.readUnsignedShort();
        buf.markWriterIndex();
        buf.writerIndex(buf.readerIndex() + size);
        ByteBufInputStream bufIn = new ByteBufInputStream(buf);
        Properties properties = new Properties();
        try {
            properties.load(bufIn);
        } finally {
            CloseUtils.safeClose(bufIn);
            buf.resetWriterIndex();
        }

        String uuidValue = properties.getProperty(UUID_PROPERTY);
        if (uuidValue == null) {
            uuid = null;
        } else {
            uuid = UUID.fromString(uuidValue);
        }
        String compressedValue = properties.getProperty(COMPRESSED_PROPERTY);
        compressed = TRUE_VALUE.equals(compressedValue) ? true : false;
        String lengthValue = properties.getProperty(LENGTH_PROPERTY);
        if (lengthValue == null) {
            length = -1;
        } else {
            length = Long.parseLong(lengthValue);
        }
    }
}
