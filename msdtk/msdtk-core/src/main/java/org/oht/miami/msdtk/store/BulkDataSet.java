/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import org.oht.miami.msdtk.studymodel.BulkDataReference;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.VR;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * The BulkDataSet class manages all the BulkDataInfos for a version
 * 
 * @author Raphael Yu Ning
 * @author James F Philbin
 */
public class BulkDataSet implements Iterable<BulkDataInfo>, Serializable {

    /**
     * serial version UID
     */
    private static final long serialVersionUID = -7168072858764997748L;

    // 1GB
    public static final int MAX_FILE_SIZE = 1 << 30;

    /**
     * study instance uid
     */
    private final DicomUID studyInstanceUID;

    /**
     * active bulk data info
     */
    private transient BulkDataInfo activeBulkDataInfo;
    /**
     * active bulk data output stream
     */
    private transient OutputStream activeBulkDataOutputStream;
    /**
     * store
     */
    protected final transient Store store;

    // Usually, there are around 4 bulk data info/file per study, hence we
    // initialize the capacity of the ArrayList to 4
    private final List<BulkDataInfo> bulkDataInfos = new ArrayList<BulkDataInfo>(4);

    private transient int activeFileSize = 0;

    /**
     * Constructor.
     * 
     * @param store
     *            The MSD-Store where the bulk data infos/files are stored.
     * @param studyInstanceUID
     *            The Study Instance UID of the study to which this BulkDataSet
     *            belongs.
     */
    public BulkDataSet(DicomUID studyInstanceUID, Store store) {
        this.studyInstanceUID = studyInstanceUID;
        activeBulkDataInfo = null;
        activeBulkDataOutputStream = null;
        this.store = store;
    }

    @Override
    public Iterator<BulkDataInfo> iterator() {

        return bulkDataInfos.iterator();
    }

    /**
     * Get number of items
     * 
     * @return int number of items
     */
    public int getNumberOfItems() {

        return bulkDataInfos.size();
    }

    /**
     * Get bulk data info
     * 
     * @param index
     *            of BulkDataInfo to be returned
     * @return BulkDataInfo
     */
    public BulkDataInfo getInfo(int index) {

        return bulkDataInfos.get(index);
    }

    /**
     * Add info. For internal use only.
     * 
     * @param info
     *            BulkDataInfo to be added
     */
    public synchronized void addInfo(BulkDataInfo info) {

        if (info == null) {
            throw new IllegalArgumentException("BulkDataInfo must not be null");
        }
        bulkDataInfos.add(info);
    }

    /**
     * Appends a bulk data value to the study's bulk data set. The value is
     * appended to the active BulkDataInfo, or a new BulkDataInfo if no
     * BulkDataInfo exists or the active BulkDataInfo does not have enough room
     * for the value.
     * 
     * @param vr
     *            The VR of the attribute with which the value is associated.
     * @param value
     *            The value as an array of bytes.
     * @return A BulkDataReference which locates the value in the study's bulk
     *         data.
     * @throws IOException
     */
    public synchronized BulkDataReference appendValue(VR vr, byte[] value) throws IOException {

        // if new bulk data item needs to be created
        if (activeBulkDataOutputStream == null || !canAppendValue(value)) {
            if (activeBulkDataOutputStream != null) {
                activeBulkDataOutputStream.close();
            }
            UUID activeBulkDataUUID = store.createBulkDataItem(studyInstanceUID);
            activeFileSize = 0;
            activeBulkDataOutputStream = store.getBulkDataOutputStream(activeBulkDataUUID,
                    studyInstanceUID);
            activeBulkDataInfo = new BulkDataInfo(studyInstanceUID, activeBulkDataUUID);
            bulkDataInfos.add(activeBulkDataInfo);
        }
        int index = getNumberOfItems() - 1;
        int offset = activeFileSize;
        activeBulkDataOutputStream.write(value);
        activeFileSize += value.length;
        return new BulkDataReference(vr, this, index, offset, value.length);
    }

    // TODO: Discuss whether bulk data item should be bulkdatainfo
    /**
     * Reads a bulk data value from the study's bulk data set.
     * 
     * @param index
     *            The index of the bulk data item which contains the value.
     * @param offset
     *            The offset of the value in the bulk data item.
     * @param length
     *            The length of the value.
     * @return The value as a byte array.
     * @throws IOException
     */
    public byte[] readValue(int index, int offset, int length) throws IOException {
        // TODO: determine whether this method needs to be removed
        BulkDataInfo info = getInfo(index);
        InputStream inputStream = store.getBulkDataInputStream(info.getUUID(), studyInstanceUID);

        byte[] value = new byte[length];
        try {
            inputStream.skip(offset);
            inputStream.read(value);
        } finally {
            inputStream.close();
        }
        return value;
    }

    /**
     * Reads value from an InputStream using the index, offset and length from
     * the BulkDataSet
     * 
     * @param bdr
     *            that contains the index, offset and length needed read the
     *            stream
     * @return byte[] value returned
     * @throws IOException
     */
    public byte[] readValue(BulkDataReference bdr) throws IOException {

        if (bdr == null) {
            throw new IllegalArgumentException("BulkDataReference must not be null");
        }
        return readValue(bdr.getIndex(), bdr.getOffset(), bdr.getLength());
    }

    private boolean canAppendValue(byte[] value) {

        return activeFileSize <= MAX_FILE_SIZE - value.length;
    }

    /**
     * Closes the active bulk data stream
     * 
     * @throws IOException
     */
    public void close() throws IOException {
        if (activeBulkDataOutputStream != null) {
            activeBulkDataOutputStream.close();
            activeBulkDataOutputStream = null;
            activeBulkDataInfo = null;
            activeFileSize = 0;
            // TODO: discuss inserting bulkdatainfo here
        }
    }

    /**
     * Compress BulkDataSet
     * 
     * @throws IOException
     */
    public void compress() throws IOException {
        // TODO
    }

    /**
     * Decompress BulkDataSet
     * 
     * @throws IOException
     */
    public void decompress() throws IOException {
        // TODO
    }

    /**
     * Encrypt BulkDataSet
     * 
     * @param ekey
     *            encryption key
     * @throws IOException
     */
    public void encrypt(long ekey) throws IOException {
        // TODO
    }

    /**
     * Decrypt BulkDataSet
     * 
     * @param ekey
     *            encryption key
     * @throws IOException
     */
    public void decrypt(long ekey) throws IOException {
        // TODO
    }

    /**
     * Create seal
     */
    public void createSeal() {
        // TODO
    }

    /**
     * Validate seal
     * 
     * @return boolean of validity
     */
    public boolean validateSeal() {
        // TODO
        return false;
    }

    /**
     * Get String representation of bulk data set
     * 
     * @return String representation of bulk data set
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (BulkDataInfo info : bulkDataInfos) {
            sb.append(info.getUUID().toString() + "\n");
        }
        return sb.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((activeBulkDataInfo == null) ? 0 : activeBulkDataInfo.hashCode());
        result = prime * result + ((bulkDataInfos == null) ? 0 : bulkDataInfos.hashCode());
        result = prime * result + ((studyInstanceUID == null) ? 0 : studyInstanceUID.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BulkDataSet other = (BulkDataSet) obj;
        if (bulkDataInfos == null) {
            if (other.bulkDataInfos != null) {
                return false;
            }
        } else if (!bulkDataInfos.equals(other.bulkDataInfos)) {
            return false;
        }
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null) {
                return false;
            }
        } else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
            return false;
        }
        return true;
    }
}
