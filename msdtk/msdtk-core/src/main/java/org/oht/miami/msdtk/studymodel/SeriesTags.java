/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import org.dcm4che2.data.Tag;

import java.util.HashMap;
import java.util.Map;

/**
 * Class holds the list of attributes defined to be series tags. Reference:
 * PS(3.3), tables C.7-5.
 * 
 * @author maismail
 */
public abstract class SeriesTags {

    /* Map of all possible sereis tags and the associated description */
    static final Map<Integer, String> map = new HashMap<Integer, String>(25);
    /* Flag to ensure the list of series tags is initialized */
    static boolean isInitialized = false;

    private static void initialize() {
        isInitialized = true;
        // Add all possible study Tags
        // TODO(maismail) Ensure the list of attributes below matches the table
        // C.7-5
        map.put(Tag.SeriesDate, "Series Date");
        map.put(Tag.SeriesTime, "Series Time");
        map.put(Tag.Modality, "Modality");
        map.put(Tag.SeriesDescription, "Series Description");
        map.put(Tag.SeriesDescription, "Series Description");
        map.put(Tag.SeriesDescriptionCodeSequence, "Series Description Code Sequence");
        map.put(Tag.PerformingPhysicianIdentificationSequence,
                "Performing Physician Identification Sequence");
        map.put(Tag.OperatorsName, "Operator's Name");
        map.put(Tag.OperatorIdentificationSequence, "Operator Identification Sequence");
        map.put(Tag.ReferencedPerformedProcedureStepSequence,
                "Referenced Performed Procedure Step Sequence");
        map.put(Tag.RelatedSeriesSequence, "Related Series Sequence");
        map.put(Tag.BodyPartExamined, "Body Part Examined");
        map.put(Tag.ProtocolName, "Protocol Name");
        map.put(Tag.PatientPosition, "Patient Position");
        map.put(Tag.SeriesInstanceUID, "Series Instance UID");
        map.put(Tag.SeriesNumber, "Series Number");
        map.put(Tag.Laterality, "Laterality");
        map.put(Tag.SmallestPixelValueInSeries, "Smallest Pixel Value in Series");
        map.put(Tag.LargestPixelValueInSeries, "Largest Pixel Value in Series");
        map.put(Tag.PerformedProcedureStepStartDate, "Performed Procedure Step Start Date");
        map.put(Tag.PerformedProcedureStepStartTime, "Performed Procedure Step Start Time");
        map.put(Tag.PerformedProcedureStepID, "Performed Procedure Step ID");
        map.put(Tag.PerformedProcedureStepDescription, "Performed Procedure Step Description");
        map.put(Tag.RequestAttributesSequence, "Request Attribute Sequence");
        map.put(Tag.CommentsOnThePerformedProcedureStep, "Comments on the Performed Procedure Step");
    }

    /* Function to check if a certain tag is a series tag or not */
    public static boolean isSeriesTag(int tag) {
        if (!isInitialized) {
            initialize();
        }
        return map.containsKey(tag);
    }
}
