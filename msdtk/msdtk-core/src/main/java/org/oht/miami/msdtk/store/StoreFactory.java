/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

/**
 * A factory that creates Stores of different types.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 */
// TODO document this more fully
// TODO extend this to provide parameters such as StoreSize, encrypt objects,
// etc.
public class StoreFactory {

    public static final UUID SIMPLE_FS = UUID.fromString("d2c28eb8-f3ea-41b9-ada0-14152b11d0d4");

    public static final UUID CACHE_FS = UUID.fromString("9c7f5e4b-f26a-4e4a-bcdd-5fcd2f0c0ebe");

    private static final String DEFAULT_CONFIG_FILE = "store.properties";

    private static final String TYPE_UUID_CONFIG_KEY = "store.uuid";

    private static final String ROOT_DIRECTORY_CONFIG_KEY = "storeFS.rootDirectory";

    private static final UUID DEFAULT_TYPE_UUID = SIMPLE_FS;

    private static final String DEFAULT_ROOT_DIRECTORY = "store-root";

    public static Store createStore() throws InvalidStoreTypeException {
        return createStore(DEFAULT_CONFIG_FILE);
    }

    public static Store createStore(String configResourceName) throws InvalidStoreTypeException {
        Properties config = loadConfig(configResourceName);
        Store store = null;

        UUID typeUUID = null;
        String typeUUIDValue = config.getProperty(TYPE_UUID_CONFIG_KEY);
        if (typeUUIDValue == null) {
            typeUUID = DEFAULT_TYPE_UUID;
        } else {
            try {
                typeUUID = UUID.fromString(typeUUIDValue);
            } catch (IllegalArgumentException e) {
                throw new InvalidStoreTypeException();
            }
        }

        String rootDirectory = config.getProperty(ROOT_DIRECTORY_CONFIG_KEY);
        if (rootDirectory == null) {
            rootDirectory = DEFAULT_ROOT_DIRECTORY;
        }

        if (typeUUID.equals(SIMPLE_FS)) {
            store = new StoreFS(rootDirectory);
        } else if (typeUUID.equals(CACHE_FS)) {
            store = new CacheFS(rootDirectory);
        } else {
            throw new InvalidStoreTypeException();
        }
        return store;
    }

    private static Properties loadConfig(String configResourceName) {
        Properties config = new Properties();
        InputStream configIn = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(configResourceName);
        if (configIn != null) {
            try {
                config.load(configIn);
            } catch (IOException e) {
                // TODO Log a warning
            } finally {
                try {
                    configIn.close();
                } catch (IOException e) {
                    // TODO Log a warning
                }
            }
        } else {
            // TODO Log a warning
        }
        return config;
    }
}
