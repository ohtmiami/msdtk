/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

/**
 * This exception is thrown when trying to read a version with a versionNumber
 * that does not exist.
 *
 * @author James F Philbin (james.philbin@jhmi.edu)
 * 
 * TODO
 */

import java.io.IOException;

//TODO should this be a public class

public class InvalidVersionNumberException extends IOException {
    static final long serialVersionUID = 1L;

    public InvalidVersionNumberException() {
        super("InvalidVersionNumberException");
    }

    public InvalidVersionNumberException(String s) {
        super(s);
    }
}
