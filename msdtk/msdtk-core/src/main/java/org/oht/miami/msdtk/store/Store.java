/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

/**
 * This package implements an MSD store using the Java file system interface. 
 * 
 *  @author James F Philbin (james.philbin@jhmi.edu)
 *
 */

// TODO Decide whether to use NIO for this implementation;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public abstract class Store {

    /**
     * This is a directory of StudyEntries.
     */
    protected final StudyDirectory directory = new StudyDirectory();

    /**
     * Returns the version number of the most recent version. This method can be
     * used to see if the version has been updated. If the version does not
     * exist it returns -1
     * 
     * @param studyInstanceUID
     * @return The version number of the most recent version or -1 if the
     *         version does not exist.
     */
    public int getMostRecentVersionNumber(DicomUID studyInstanceUID) {
        StudyEntry entry = directory.lookup(studyInstanceUID);
        if (entry == null) {
            return -1;
        } else {
            return entry.getMostRecentVersionNumber();
        }
    }

    /**
     * This method reads the most recent version of the Study with the matching
     * studyInstanceUID. It returns a new Study with the attributes from that
     * version. If no version of the Study exists it returns null
     * 
     * @param studyInstanceUID
     * @return Study
     * @throws IOException
     */
    public Study readVersion(DicomUID studyInstanceUID) throws IOException {
        StudyEntry entry = directory.lookup(studyInstanceUID, true);
        // if the entry has been deleted, throw an exception
        if (entry != null && !entry.deleteType.equals(DeleteType.Live)) {
            throw new VersionPreviouslyDeletedException();
        }
        if (entry == null) {
            return null;
        }
        return entry.readVersion();
    }

    /**
     * This method reads the stored version of the Study, with the matching
     * studyInstanceUID and versionNumber. It returns a new Study containing the
     * attributes from that version. If no version exists with versionNumber
     * then this method returns null.
     * 
     * @param studyInstanceUID
     * @param versionNumber
     * @return Study
     * @throws IOException
     */
    public Study readVersion(DicomUID studyInstanceUID, int versionNumber) throws IOException {
        StudyEntry entry = directory.lookup(studyInstanceUID);
        if (entry == null) {
            return null;
        }
        return entry.readVersion(versionNumber);
    }

    /**
     * This method writes a new version of a study. If no version of the study
     * exists it creates a new version with versionNumber zero. If a version
     * already exists, it creates a new version of the study with the
     * versionNumber incremented by one.
     * 
     * @param version
     *            The Study from which the version is created.
     * @return The UUID of the version that is written to the Store
     * @throws IOException
     */
    public abstract UUID writeVersion(Study version) throws IOException;

    /**
     * Searches this MSD-Store for an existing version of the requested study.
     * If not found, creates a new study.
     * 
     * @param studyInstanceUID
     *            The Study Instance UID of the study to look up.
     * @return Either an existing version of the requested study, or a new study
     *         with the given Study Instance UID.
     * @throws IOException
     */
    public Study findOrCreateStudy(DicomUID studyInstanceUID) throws IOException {

        Study version = readVersion(studyInstanceUID);
        if (version == null) {
            version = new Study(this, studyInstanceUID);
        } else {
            // TODO Move to Study.addDicomObject()
            version.setVersionType(VersionType.Append);
        }
        return version;
    }

    /**
     * This method make the study unavailable, but doesnot remove it from the
     * StudyDirectory
     */
    /**
     * deleteStudy is used to handle two types of Study deletion: Soft and Hard.
     * In both cases the StudyEntry is marked with the deletion type, but it is
     * not deleted. If the DeleteType is Soft then the StudyEntry associated
     * with studyInstanceUID is marked as deleted, but the StudyEntry and all
     * the versions and BulkDataSet associated with the study are left in place.
     * If the DeleteType is Hard then the StudyEntry with studyInstanceUID is
     * mark as deleted and all of the versions and BulkDataSet associated with
     * the study are deleted from the Store. When a study has been deleted,
     * either Soft or Hard, no new versions; can be added to the study.
     * 
     * @param studyInstanceUID
     *            The Study whose data is to be marked as deleted
     * @param dType
     *            One of DeleteType.Soft or DeleteType.Hard
     * @throws IOException
     */
    public void deleteStudy(DicomUID studyInstanceUID, DeleteType dType) throws IOException {
        StudyEntry entry = directory.lookup(studyInstanceUID, true);
        if (entry == null) {
            throw new VersionDoesNotExistException();
        } else {
            if (!entry.deleteType.equals(DeleteType.Live)
                    && !(entry.deleteType.equals(DeleteType.Soft) && dType.equals(DeleteType.Hard))) {
                throw new VersionPreviouslyDeletedException();
            }
            entry.deleteStudy(dType);
        }
    }

    /**
     * Get version input stream
     * 
     * @param uuid
     * @param studyInstanceUID
     * @return
     * @throws IOException
     */
    public abstract InputStream getVersionInputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException;

    /**
     * Get bulk data input stream for reading a bulk data item out of the store.
     * 
     * @param uuid
     * @param studyInstanceUID
     * @return OutputStream
     * @throws IOException
     */
    public abstract InputStream getBulkDataInputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException;

    /**
     * Get version output stream
     * 
     * @param uuid
     * @param studyInstanceUID
     * @return OutputStream
     * @throws IOException
     */
    public abstract OutputStream getVersionOutputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException;

    /**
     * Get bulk data output stream for writing a bulk data item
     * 
     * @param uuid
     * @param studyInstanceUID
     * @return OutputStream
     * @throws IOException
     */
    public abstract OutputStream getBulkDataOutputStream(UUID uuid, DicomUID studyInstanceUID)
            throws IOException;

    /**
     * Get the size of a version file in the store
     * 
     * @param uuid
     * @param studyInstanceUID
     * @return file size in bytes
     * @throws IOException
     */
    public abstract long getVersionFileSize(UUID uuid, DicomUID studyInstanceUID) throws IOException;

    /**
     * Get the size of a bulk data file in the store
     * 
     * @param uuid
     * @param studyInstanceUID
     * @return file size in bytes
     * @throws IOException
     */
    public abstract long getBulkDataFileSize(UUID uuid, DicomUID studyInstanceUID)
            throws IOException;

    /**
     * Insert study entry into the store
     * 
     * @param studyEntry
     *            transportable study entry needed to insert a study entry into
     *            the store
     * @return boolean Will return false if there is an existing study entry
     *         which is not a subset of the one being inserted.(For example, the
     *         new study entry has less images than the previous one.)
     */
    public abstract boolean insertStudyEntry(TransportableStudyEntry studyEntry);

    /**
     * Get transportable study entry. Will return null if study is not in the
     * store.
     * 
     * @param studyInstanceUID
     *            study instance UID
     * @return transportable study entry for given study instance UID
     */
    public TransportableStudyEntry getTransportableStudyEntry(DicomUID studyInstanceUID) {
        // TODO:Should we include deleted entries in the lookup?
        StudyEntry entry = directory.lookup(studyInstanceUID, true);
        if (entry == null) {
            return null;
        }
        return entry.createTransportableStudyEntry();
    }

    /**
     * Creates a new bulk data item
     * 
     * @param studyInstanceUID
     *            of item to be created
     * @return UUID of created item
     */
    public abstract UUID createBulkDataItem(DicomUID studyInstanceUID);

    // TODO: Add a method to delete bulk data for a study, discuss if necessary
}
