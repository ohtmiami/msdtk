/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.io;

import java.io.File;
import java.io.FileFilter;

/**
 * A <code>FileFilter</code> that accepts only DICOM files.
 * 
 * @author Raphael Yu Ning
 */
public class DicomFileFilter implements FileFilter {

    public static final String DICOM_FILE_EXTENSION = ".dcm";

    @Override
    public boolean accept(File file) {

        if (!file.isFile()) {
            // ignore subdirectories
            return false;
        }

        // filename must be like "*.dcm"
        String fileName = file.getName();

        if (fileName.length() <= DICOM_FILE_EXTENSION.length()) {
            return false;
        }

        String extension = fileName.substring(fileName.length() - DICOM_FILE_EXTENSION.length());
        return extension.equalsIgnoreCase(DICOM_FILE_EXTENSION);
    }
}
