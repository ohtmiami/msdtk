/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.deidentification;

import org.oht.miami.msdtk.studymodel.Study;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SequenceDicomElement;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.dcm4che2.util.UIDUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Class provides utilities used for de-Identification.
 * 
 * @author maismail
 */
public class DeIdentifyUtil {

    // Source: Dicom standard, PS(3.16), Table Context ID 7050 De-identification
    // Method, Page 607
    public static final String BasicApplicationConfidentialityProfile = "{113100}";
    public static final String CleanGraphicsOption = "{113103}";
    public static final String CleanStructuredContentOption = "{113104}";
    public static final String CleanDescriptorsOption = "{113105}";
    public static final String RetainLongitudinalTemporalInformationWithFullDatesOption = "{113106}";
    public static final String RetainLongitudinalTemporalInformationWithModifiedDatesOption = "{113107}";
    public static final String RetainPatientCharacteristicsOption = "{113108}";
    public static final String RetainDeviceIdentityOption = "{113109}";
    public static final String RetainUIDsOption = "{113110}";
    public static final String RetainSafePrivateOption = "{113111}";

    /*
     * D - replace with nonzero length Z - replace with zero length or nonzero
     * length X - remove K - keep C - clean U - replace nonzero UID
     */
    public enum ActionCode {
        D, Z, X, K, C, U, ZD, XZ, XD, XZD, XZU, NONE;
    }

    public static final Map<Integer, AttributeProfileActions> map = new HashMap<Integer, AttributeProfileActions>(
            248);
    // Source: Dicom standard, PS(3.15), Table E.1.1, page65.
    static {
        map.put(Tag.AccessionNumber, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AcquisitionComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AcquisitionContextSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.C, ActionCode.NONE));
        map.put(Tag.AcquisitionDate, new AttributeProfileActions(ActionCode.XZ, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AcquisitionDateTime, new AttributeProfileActions(ActionCode.XD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AcquisitionDeviceProcessingDescription, new AttributeProfileActions(ActionCode.XD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AcquisitionProtocolDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AcquisitionTime, new AttributeProfileActions(ActionCode.XZ, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ActualHumanPerformersSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AdditionalPatientHistory, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AdmissionID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AdmittingDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K,
                ActionCode.C, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.AdmittingDiagnosesCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AdmittingDiagnosesDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AdmittingTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AffectedSOPInstanceUID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.Allergies, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE,
                ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.Arbitrary, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.AuthorObserverSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.BranchOfService, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CassetteID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CommentsOnThePerformedProcedureStep, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ConcatenationUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ConfidentialityConstraintOnPatientDataDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.ContentCreatorName, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ContentCreatorIdentificationCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ContentDate, new AttributeProfileActions(ActionCode.ZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ContentSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.C, ActionCode.NONE));
        map.put(Tag.ContentTime, new AttributeProfileActions(ActionCode.ZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ContextGroupExtensionCreatorUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ContrastBolusAgent, new AttributeProfileActions(ActionCode.ZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ContributionDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CountryOfResidence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CreatorVersionUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CurrentPatientLocation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CurveDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CurveTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.CurveData, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.C));

        map.put(Tag.CustodialOrganizationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DataSetTrailingPadding, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DerivationDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DetectorID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DeviceSerialNumber, new AttributeProfileActions(ActionCode.XZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
       
        //TODO replace DeviceUID's value with dummy value
        map.put(Tag.DeviceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DigitalSignatureUID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DigitalSignaturesSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DimensionOrganizationUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DischargeDiagnosisDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DistributionAddress, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.DistributionName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.DoseReferenceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.EthnicGroup, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.FailedSOPInstanceUIDList, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.FiducialUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.FillerOrderNumberImagingServiceRequest, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.FrameComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.FrameOfReferenceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.GantryID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.GeneratorID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.GraphicAnnotationSequence, new AttributeProfileActions(ActionCode.D, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.C));
        map.put(Tag.HumanPerformerName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.HumanPerformerOrganization, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.IconImageSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.IdentifyingComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ImageComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ImagePresentationComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ImagingServiceRequestComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.Impressions, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InstanceCreatorUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InstitutionAddress, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InstitutionCodeSequence, new AttributeProfileActions(ActionCode.XZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InstitutionName, new AttributeProfileActions(ActionCode.XZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InstitutionalDepartmentName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InsurancePlanIdentification, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.IntendedRecipientsOfResultsIdentificationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationApproverSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationAuthor, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationDiagnosisDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationIDIssuer, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationRecorder, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationText, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.InterpretationTranscriber, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.IrradiationEventUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.IssuerOfAdmissionID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.IssuerOfPatientID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.IssuerOfServiceEpisodeID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.LargePaletteColorLookupTableUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.LastMenstrualDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.MAC, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.MediaStorageSOPInstanceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.MedicalAlerts, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.MedicalRecordLocator, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.MilitaryRank, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ModifiedAttributesSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ModifiedImageDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ModifyingDeviceID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ModifyingDeviceManufacturer, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.NameOfPhysiciansReadingStudy, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.NamesOfIntendedRecipientsOfResults, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.Occupation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OperatorIdentificationSequence, new AttributeProfileActions(ActionCode.XD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OperatorsName, new AttributeProfileActions(ActionCode.XZD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OriginalAttributesSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OrderCallbackPhoneNumber, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OrderEnteredBy, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OrderEntererLocation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OtherPatientIDs, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OtherPatientIDsSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OtherPatientNames, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.OverlayDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.OverlayTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PaletteColorLookupTableUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ParticipantSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientAddress, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientID, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientSexNeutered, new AttributeProfileActions(ActionCode.XZ, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientState, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE,
                ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientTransportArrangements, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientAge, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientBirthDate, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientBirthName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientBirthTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.PatientInstitutionResidence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientInsurancePlanCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientMotherBirthName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientName, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientPrimaryLanguageCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientPrimaryLanguageModifierCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientReligiousPreference, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientSex, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientSize, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientTelephoneNumbers, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PatientWeight, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedLocation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.PerformedProcedureStepDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedProcedureStepID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedProcedureStepStartDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedProcedureStepStartTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedStationAETitle, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedStationGeographicLocationCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedStationName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformingPhysicianIdentificationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.PerformingPhysicianName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PersonAddress, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        
        //TODO make sure Tag.PersonIdentificationCodeSequence value is dummied
        map.put(Tag.PersonIdentificationCodeSequence, new AttributeProfileActions(ActionCode.D, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
       //TODO make sure Tag.PersonName value is dummied
        map.put(Tag.PersonName, new AttributeProfileActions(ActionCode.D, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PersonTelephoneNumbers, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PerformedStationNameCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedPatientAliasSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PhysicianApprovingInterpretation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PhysiciansReadingStudyIdentificationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PhysiciansOfRecord, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PhysiciansOfRecordIdentificationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PlacerOrderNumberImagingServiceRequest, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.PlateID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PreMedication, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.PregnancyStatus, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ProtocolName, new AttributeProfileActions(ActionCode.XD, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReasonForTheImagingServiceRequest, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReasonForStudy, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedDigitalSignatureSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedFrameOfReferenceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedGeneralPurposeScheduledProcedureStepTransactionUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedImageSequence, new AttributeProfileActions(ActionCode.XZU, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.ReferencedPatientSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.X, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedPerformedProcedureStepSequence, new AttributeProfileActions(ActionCode.XZD, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedSOPInstanceMACSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedSOPInstanceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedSOPInstanceUIDInFile, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferencedStudySequence, new AttributeProfileActions(ActionCode.XZ, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferringPhysicianAddress, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferringPhysicianIdentificationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.ReferringPhysicianName, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReferringPhysicianTelephoneNumbers, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RegionOfResidence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RelatedFrameOfReferenceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestAttributesSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestedContrastAgent, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestedProcedureComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestedProcedureDescription, new AttributeProfileActions(ActionCode.XZ, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestedProcedureID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestedProcedureLocation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.RequestedSOPInstanceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestingPhysician, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.RequestingService, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ResponsibleOrganization, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ResponsiblePerson, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ResultsComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ResultsDistributionListSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ResultsIDIssuer, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ReviewerName, new AttributeProfileActions(ActionCode.XZ, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledHumanPerformersSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledPatientInstitutionResidence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.ScheduledPerformingPhysicianIdentificationSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledPerformingPhysicianName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledProcedureStepEndDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledProcedureStepEndTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledProcedureStepDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledProcedureStepLocation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledProcedureStepStartDate, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledProcedureStepStartTime, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.ScheduledStationAETitle, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledStationGeographicLocationCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledStationName, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledStationNameCodeSequence, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledStudyLocation, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ScheduledStudyLocationAETitle, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SeriesDate, new AttributeProfileActions(ActionCode.XD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SeriesDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SeriesInstanceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SeriesTime, new AttributeProfileActions(ActionCode.XD, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ServiceEpisodeDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.ServiceEpisodeID, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SmokingStatus, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SOPInstanceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.SourceImageSequence, new AttributeProfileActions(ActionCode.XZU, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SpecialNeeds, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StationName, new AttributeProfileActions(ActionCode.XZU, ActionCode.NONE,
                ActionCode.NONE, ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StorageMediaFileSetUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyDate, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyDescription, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyID, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyIDIssuer, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyInstanceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.StudyTime, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.SynchronizationFrameOfReferenceUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TemplateExtensionCreatorUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TemplateExtensionOrganizationUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TextComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));

        map.put(Tag.TextString, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TimezoneOffsetFromUTC, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.K, ActionCode.C,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TopicAuthor, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TopicKeywords, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TopicSubject, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TopicTitle, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.TransactionUID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.K, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.UID, new AttributeProfileActions(ActionCode.U, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.VerifyingObserverIdentificationCodeSequence, new AttributeProfileActions(ActionCode.Z, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        
        //TODO make sure that VerifyingObserverName value is dummied
        map.put(Tag.VerifyingObserverName, new AttributeProfileActions(ActionCode.D, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        //TODO make sure that VerifyingObserverSequence value is dummied
        map.put(Tag.VerifyingObserverSequence, new AttributeProfileActions(ActionCode.D, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.VerifyingOrganization, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE));
        map.put(Tag.VisitComments, new AttributeProfileActions(ActionCode.X, ActionCode.NONE,
                ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                ActionCode.NONE, ActionCode.C, ActionCode.NONE, ActionCode.NONE));

    }
//TODO Fill in return tag
    /**
     * Gets the actions associated with certain tag as defined by the
     * de-identification standard.
     * 
     * @param tag
     *
     * @return
     *
     */
    public static AttributeProfileActions getAttributeActions(final int tag) {
        // If the tag belongs to the range of tags reserved for curve data or
        // overlay data
        if (isCurveOverlayData(tag)) {
            return new AttributeProfileActions(ActionCode.X, ActionCode.NONE, ActionCode.NONE,
                    ActionCode.NONE, ActionCode.NONE, ActionCode.NONE, ActionCode.NONE,
                    ActionCode.NONE, ActionCode.NONE, ActionCode.C);
        }
        return map.get(tag);
    }

    /**
     * Applies the K action (Keep) as defined by the Dicom de-identification
     * standard The action keeps the simple attributes without changes and
     * recursively applies the profile rules to each element in each Item of the
     * Sequence attributes.
     * 
     * @param dcmElem   the dicom element to be de-identified.
     */
    public static void apply_K_Action(final DicomElement dcmElem,
            final AttributeProfileActions actions, final StudyProfileOptions options,
            final DicomObject parentObject) {
        // The first call for this function should have an input dicomelement
        // with a VR value
        // equals SQ and parent dicom object = null
        // Other recursive calls, have to have a parent dicom object does not
        // equal to null.
        if (dcmElem.vr() == VR.SQ) {
            for (int i = 0; i < dcmElem.countItems(); i++) {
                DicomObject childObj = dcmElem.getDicomObject(i);
                /* Add Attributes */
                for (Iterator<DicomElement> iter = childObj.iterator(
                        Tag.FileMetaInformationGroupLength, 0xffffffff); iter
                        .hasNext();) {
                    DicomElement childElem = iter.next();
                    AttributeProfileActions newActions = DeIdentifyUtil
                            .getAttributeActions(childElem.tag());
                    if (newActions != null) {
                        apply_K_Action(childElem, newActions, options, childObj);
                    }
                }
                // may be the recursive calls delete the dicom elements within
                // the childobj
                if (childObj.isEmpty()) {
                    dcmElem.removeDicomObject(childObj);
                }
            }
        } else {
            ActionCode action = DeIdentifyUtil.GetAction(actions, options);
            switch (action) {
            case X:
            case XZ:
            case XD:
            case XZU:
            case XZD:
                // removes it from the list of children of the parent Dicom
                // element
                parentObject.remove(dcmElem.tag());
                break;

            case U:
                DicomElement updatedElem = DeIdentifyUtil.changeUID(dcmElem, UIDUtils.createUID());
                parentObject.remove(dcmElem.tag());
                parentObject.add(updatedElem);
                break;

            case Z:
            case ZD:
                DicomElement zeroLengthElement = apply_Z_Action(dcmElem);
                parentObject.remove(dcmElem.tag());
                parentObject.add(zeroLengthElement);
                break;

            case K:
                // do nothing
                break;

            case D:
            case C:
            default:
                break;
            }
        }
    }

    /**
     * Applies the Z action (replace with a zero length value) as defined by the
     * Dicom de-identification standard,
     * 
     * @param dcmElem
     *            , the dicom element to be de-identified.
     * @return the dicom element after replacing the contents with a zero length
     *         value
     */
    public static DicomElement apply_Z_Action(final DicomElement dcmElem) {
        // Replaces the DicomElement value with a zero value
        if (dcmElem.vr() != VR.SQ) {
            return new SimpleDicomElement(dcmElem.tag(), dcmElem.vr(), dcmElem.bigEndian(), null,
                    null);
        } else {
            DicomElement sDicomElement = new SequenceDicomElement(dcmElem.tag(), VR.SQ,
                    dcmElem.bigEndian(), new ArrayList<Object>(), null);
            for (int i = 0; i < dcmElem.countItems(); i++) {
                DicomObject childObj = dcmElem.getDicomObject(i);
                /* Add Attributes */
                for (Iterator<DicomElement> iter = childObj.iterator(Tag.FileMetaInformationGroupLength, 0xffffffff); iter
                        .hasNext();) {
                    childObj.add(apply_Z_Action(iter.next()));
                }
                sDicomElement.addDicomObject(childObj);
            }
            return sDicomElement;
        }
    }

    /**
     * Creates a new dicom element with the same tag and vr as the input dicom
     * element but with a value equals to newUID.
     * 
     * @param dcmElem
     *            , input dicom element to be copied.
     * @param newUID
     *            , the new dicom element value.
     * @return DicomElement.
     */
    public static DicomElement changeUID(final DicomElement dcmElem, String newUID) {
        // All tags of UID dicom elements have a VR equals to non SQ "sequence"
        // value
        return new SimpleDicomElement(dcmElem.tag(), dcmElem.vr(), dcmElem.bigEndian(),
                newUID.getBytes(), null);
    }

    /**
     * Determines which action should be applied given chosen profile options
     * 
     * @param profileActions
     *            , profile actions associated with a certain dicom element to
     *            be de-identified. The actions are defined by the DICOM
     *            de-identification standard.
     * @param profileOptions
     *            , profile options associated with the study
     * @return the appropiate action to be applied.
     */
    public static ActionCode GetAction(AttributeProfileActions profileActions,
            StudyProfileOptions profileOptions) {
        if (profileOptions.isRetainDeviceIdentOption
                && profileActions.retainDeviceIdentAction == ActionCode.K
                || profileOptions.isRetainLongFullDatesOption
                && profileActions.retainLongFullDatesAction == ActionCode.K
                || profileOptions.isRetainLongModifiedDatesOption
                && profileActions.retainLongModifiedDatesAction == ActionCode.K
                || profileOptions.isRetainPatientCharsOption
                && profileActions.retainPatientCharsAction == ActionCode.K
                || profileOptions.isRetainUIDsOption
                && profileActions.retainUIDsAction == ActionCode.K
                || profileOptions.isRetainSafePrivateOption
                && profileActions.retainSafePrivateAction == ActionCode.K) {
            return ActionCode.K;
        } else {
            return profileActions.basicAction;
        }
    }

    /**
     * Determines weather a tag belongs to the range of tags associated with
     * curve or overlay data or not.
     * 
     * @param tag
     *            , input tag to be checked
     * @return boolean, result of the check
     */
    private static boolean isCurveOverlayData(final int tag) {
        /* Convert to Hexadecimal */
        String hexTag = Integer.toHexString(tag);
        /* Adds zero paddings such that the number is expressed using 8 digits */
        String zeros = "";
        for (int numberZeroPaddings = 8 - hexTag.length(); numberZeroPaddings > 0; numberZeroPaddings--) {
            zeros += '0';
        }
        hexTag = zeros + hexTag;
        int groupNumberStr = Integer.valueOf(hexTag.substring(0, 3), 16);
        int elementNumber = Integer.valueOf(hexTag.substring(4, 7), 16);
        // Check for curve data range of values
        if (groupNumberStr >= 0x5000 && groupNumberStr < 0x6000) {
            return true;
        }
        // Check for overlay range of values
        if (groupNumberStr >= 0x6000 && groupNumberStr < 0x7000) {
            if (elementNumber == 0x4000 || elementNumber == 0x3000) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds the necessary attributes to label a study as a de-identified a
     * study.
     * 
     * @param study
     *            , input study to be labeled as a de-identified study
     * @param pOptions
     *            , the options used for de-identfying the study.
     */
    public static void addDeIdentifyInfo(final Study study, final StudyProfileOptions pOptions) {
        study.removeAttribute(Tag.PatientIdentityRemoved);
        study.putAttribute(new SimpleDicomElement(Tag.PatientIdentityRemoved, VR.CS, false, "YES"
                .getBytes(), null));
    }

}