/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.deidentification;

/**
 * Class ProfileOptions. It holds the profile options for a certain study.
 * Initially all are false.
 * 
 * @author mmahmou4
 */
public class StudyProfileOptions {

    /*
     * Basic profile options determines the identification options set for the
     * study
     */
    public boolean isBasicProfile = true;
    public boolean isRetainSafePrivateOption = false;
    public boolean isRetainUIDsOption = false;

    public boolean isRetainDeviceIdentOption = false;
    public boolean isRetainPatientCharsOption = false;
    public boolean isRetainLongFullDatesOption = false;

    public boolean isRetainLongModifiedDatesOption = false;
    public boolean isCleanDescOption = false;
    public boolean isCleanStructContOption = false;
    public boolean isCleanGraphOption = false;

    /**
     * Constructor to initialize the attribute profile options
     */
    public StudyProfileOptions(boolean basic, boolean rSafePrivate, boolean rUIDOption,
            boolean rDeviceDeidentification, boolean rPatientCharsOptions, boolean rLongFullDates,
            boolean rLongModifiedDate, boolean cDescription, boolean cStructure, boolean cGraphics) {
        isBasicProfile = basic;
        isCleanDescOption = cDescription;
        isCleanGraphOption = cGraphics;
        isCleanStructContOption = cStructure;
        isRetainDeviceIdentOption = rDeviceDeidentification;
        isRetainLongFullDatesOption = rLongFullDates;
        isRetainLongModifiedDatesOption = rLongModifiedDate;
        isRetainPatientCharsOption = rPatientCharsOptions;
        isRetainSafePrivateOption = rSafePrivate;
        isRetainUIDsOption = rUIDOption;
    }

    /* default constructor */
    public StudyProfileOptions() {
    }

}
