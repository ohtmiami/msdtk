/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */
package org.oht.miami.msdtk.store;

/**
 * The Abstract StudyEntry class.  It implements the part of a StudyEntry that is not system dependent.
 * 
 * @author James F Philbin (james.philbin@jhmi.edu)
 * 
 * @copyright
 * 
 */

import org.oht.miami.msdtk.conversion.dicom.Study2Dicom;
import org.oht.miami.msdtk.crypto.StudyCipher;
import org.oht.miami.msdtk.crypto.StudyCryptoException;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomObjectUtil;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.util.CloseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

abstract class StudyEntry {

    // Constants

    private static final Logger LOG = LoggerFactory.getLogger(StudyEntry.class);

    /**
     * The DICOM Study Instance UID for the study. It must be supplied when the
     * Entry is created.
     */
    protected final DicomUID studyInstanceUID;

    // Fields

    /**
     * Version history
     */
    VersionHistory versionHistory;

    /**
     * A StudyInfo object containing a synopsis of study
     */
    StudyInfo studyInfo;

    /**
     * A flag specifying whether a study has been deleted or not. If this flag
     * is set the study will not be visible except to privileged users.
     */
    DeleteType deleteType = DeleteType.Live;

    /**
     * The date&time of the last access to this study. It can be used for
     * managing the cache in a caching implementation.
     */
    Date lastAccessDate;

    /**
     * The type of the most recent access
     */
    LastAccessType lastAccessType = null;

    /**
     * Bulk data set history
     */
    // TODO: determine whether or not this should be a list of BulkDataSet or
    // BulkDataInfo, and also whether it should be called history
    List<BulkDataSet> bulkDataSetHistory;

    protected StudyCipher cipher = StudyCipher.getInstance();

    // Constructors

    /**
     * Creates and initializes a new study entry object. StudyEntries are not
     * accessible outside this package.
     * 
     * @param version
     *            A stored version of a MSD Study
     * @see "http://ohtmiami.atlassian.net/MSD-store.html"
     */
    StudyEntry(Study version) {
        studyInstanceUID = version.getStudyInstanceUID();
        studyInfo = new StudyInfo(version);
        versionHistory = new VersionHistory();
        bulkDataSetHistory = new ArrayList<BulkDataSet>();
    }

    // Methods

    /**
     * Creates and initializes a new study entry object. StudyEntries are not
     * accessible outside this package.
     * 
     * @param transportableStudyEntry
     *            transportable study entry A stored version of a MSD Study
     * @see "http://ohtmiami.atlassian.net/MSD-store.html"
     */
    public StudyEntry(TransportableStudyEntry transportableStudyEntry) {
        studyInstanceUID = transportableStudyEntry.getStudyInstanceUID();
        studyInfo = transportableStudyEntry.getStudyInfo();
        versionHistory = transportableStudyEntry.getVersionHistory();
        bulkDataSetHistory = transportableStudyEntry.getBulkDataSetHistory();
    }

    /**
     * This method reads the most recent version of the Study with the matching
     * studyInstanceUID. It returns a new Study with the attributes from that
     * version. If no version of the Study exists it throws a
     * VersionDoesNotExistExceptoin. This method will also validate the contents
     * of the stored object using the value of the IntegritySeal in the
     * VersionInfo. It will also decrypte and decompress the stored data.
     * 
     * @return A new Study containing the attributes of the stored version
     * @throws IOException
     */
    abstract Study readVersion() throws IOException;

    /**
     * This method reads the stored version of the Study, with the version
     * number that equals to versionNumber, of the Study, with studyInstanceUID.
     * It returns a new Study containing the attributes from that version. If no
     * version exists with versionNumber then this method throws an
     * InvalidVersionNumberException.
     * 
     * @param vNumber
     * @return The new Study containing the attributes of the stored versoin
     * @throws IOException
     */
    // TODO: make readVersion use the Store's getVersionInputStream to make
    // implementation be done entirely in StudyEntry
    abstract Study readVersion(int vNumber) throws IOException;

    /**
     * writeVersion is the procedure used to write a version of the study. It is
     * a synchronized method. There are three version types that control the
     * behavior of this method:
     * <dl>
     * <dt>New:</dt>
     * <dd>A new version is being created and there should be no existing
     * versions of this study. If a version does exist then this method should
     * throw the VersionAlreadyExistsException.</dd>
     * <dt>Append:</dt>
     * <dd>A new version of an existing study is being created and this version
     * contains only additional data elements for this study.</dd>
     * <dt>Modify:</dt>
     * <dd>A new version of an existing study is being create that contained
     * data elements that have been deleted or whose values have been modified.
     * It might also contain new data elements.</dd>
     * </dl>
     * 
     * @param version
     *            The Study from which the version is to be written
     * @return The UUID of the written version
     * @throws IOException
     */
    synchronized UUID writeVersion(Study version) throws IOException {
        version.getBulkDataSet().close();
        UUID versionUUID = UUID.randomUUID();
        ByteBuf encrypted = encryptVersion(version, versionUUID);
        writeToStore(encrypted, versionUUID);
        updateLastAccess(LastAccessType.Write);
        // Create the VersionHistory after the Version is written
        versionHistory.addVersion(version, versionUUID);
        bulkDataSetHistory.add(version.getBulkDataSet());
        return versionUUID;
    }

    protected ByteBuf encryptVersion(Study version, UUID versionUUID) throws IOException {
        long conversionStart = System.nanoTime();
        DicomObject versionDicomObject = Study2Dicom
                .study2MultiSeriesDicom(version, versionHistory);
        long encodingStart = System.nanoTime();
        // 132B = 128B (Preamble) + 4B ('D', 'I', 'C', 'M')
        int estimatedUncompressedSize = 132 + DicomObjectUtil.estimateSize(versionDicomObject);
        ByteBuf uncompressed = cipher.allocateBufferForUnencryptedData(estimatedUncompressedSize,
                true);
        DicomOutputStream dicomOut = new DicomOutputStream((OutputStream) new ByteBufOutputStream(
                uncompressed));
        try {
            dicomOut.writeDicomFile(versionDicomObject);
        } finally {
            CloseUtils.safeClose(dicomOut);
        }
        long encodingEnd = System.nanoTime();
        int uncompressedSize = uncompressed.readableBytes() - StudyCipher.ZLIB_HEADER_LEN;
        LOG.trace("{}\t{}\t{}\t", new Object[] { encodingStart - conversionStart,
                        encodingEnd - encodingStart, uncompressedSize });
        try {
            cipher.initEncrypt(version.getStudyInstanceUID(), true, versionUUID, uncompressedSize);
            return cipher.doFinal(uncompressed);
        } catch (StudyCryptoException e) {
            throw new IOException(e);
        }
    }

    /**
     * Updates the entry access fields
     */
    // TODO remove
    protected void updateLastAccess(LastAccessType type) {
        lastAccessDate = new Date();
        lastAccessType = type;
    }

    /**
     * Write a version buffer to the underlying storage implementation.
     * 
     * @param versionBuffer
     *            Version buffer to be written out.
     * @param versionUUID
     *            UUID of the version file to be created.
     * @throws IOException
     */
    protected abstract void writeToStore(ByteBuf versionBuffer, UUID versionUUID)
            throws IOException;

    /**
     * deleteStudy is used to handle two types of Study deletion: Soft: If the
     * DeleteType is Soft then the StudyEntry associated with studyInstanceUID
     * is marked as deleted, but the StudyEntry and all the versions and
     * BulkData associated with the study are left in place. Hard: If the
     * DeleteType is Hard then the StudyEntry with studyInstanceUID is marked as
     * deleted and all of the versions and BulkData associated with the study
     * are deleted from the Store. When a study has been deleted, either Soft or
     * Hard, no versions, whether create, append or modify,can be added to the
     * study.
     * 
     * @param type
     *            The DeletionType to be done
     * @throws IOException
     */
    abstract void deleteStudy(DeleteType type) throws IOException;

    @Override
    public String toString() {
        return "[StudyEntry" + studyInstanceUID + "]";
    }

    /**
     * Get most recent version uuid from history
     * 
     * @return uuid of most recent version
     */
    protected UUID getMostRecentVersionUUID() {
        return versionHistory.getMostRecentVersionInfo().getUUID();
    }

    /**
     * Get study instance uuid
     * 
     * @return study instance uid
     */
    protected DicomUID getStudyInstanceUID() {
        return studyInstanceUID;
    }

    /**
     * Get most recent version number from history
     * 
     * @return most recent version number
     */
    protected int getMostRecentVersionNumber() {
        return versionHistory.getMostRecentVersionNumber();
    }

    /**
     * Create a transportable study entry from study entry fields
     * 
     * @return transportable study entry
     */
    public TransportableStudyEntry createTransportableStudyEntry() {
        return new TransportableStudyEntry(studyInstanceUID, versionHistory, studyInfo, deleteType,
                bulkDataSetHistory);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((bulkDataSetHistory == null) ? 0 : bulkDataSetHistory.hashCode());
        result = prime * result + ((deleteType == null) ? 0 : deleteType.hashCode());
        result = prime * result + ((studyInfo == null) ? 0 : studyInfo.hashCode());
        result = prime * result + ((studyInstanceUID == null) ? 0 : studyInstanceUID.hashCode());
        result = prime * result + ((versionHistory == null) ? 0 : versionHistory.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        StudyEntry other = (StudyEntry) obj;
        if (bulkDataSetHistory == null) {
            if (other.bulkDataSetHistory != null) {
                return false;
            }
        } else if (!bulkDataSetHistory.equals(other.bulkDataSetHistory)) {
            return false;
        }
        if (deleteType != other.deleteType) {
            return false;
        }
        if (studyInfo == null) {
            if (other.studyInfo != null) {
                return false;
            }
        } else if (!studyInfo.equals(other.studyInfo)) {
            return false;
        }
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null) {
                return false;
            }
        } else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
            return false;
        }
        if (versionHistory == null) {
            if (other.versionHistory != null) {
                return false;
            }
        } else if (!versionHistory.equals(other.versionHistory)) {
            return false;
        }
        return true;
    }

    public boolean contains(StudyEntry other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        if (bulkDataSetHistory == null) {
            if (other.bulkDataSetHistory != null) {
                return false;
            }
        } else if (!bulkDataSetHistory.containsAll(other.bulkDataSetHistory)) {
            return false;
        }
        if (deleteType != other.deleteType) {
            return false;
        }
        if (studyInfo == null) {
            if (other.studyInfo != null) {
                return false;
            }
        } else if (!studyInfo.contains(other.studyInfo)) {
            return false;
        }
        if (studyInstanceUID == null) {
            if (other.studyInstanceUID != null) {
                return false;
            }
        } else if (!studyInstanceUID.equals(other.studyInstanceUID)) {
            return false;
        }
        if (versionHistory == null) {
            if (other.versionHistory != null) {
                return false;
            }
        } else if (!versionHistory.contains(other.versionHistory)) {
            return false;
        }
        return true;
    }

}
