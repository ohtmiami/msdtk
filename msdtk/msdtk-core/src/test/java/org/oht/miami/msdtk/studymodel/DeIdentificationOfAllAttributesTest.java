/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.*;

import org.oht.miami.msdtk.deidentification.StudyProfileOptions;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.Test;

/**
 * Writes all the tags to a study to make sure they are de-identified correctly.
 * 
 * @author Katie Calabro kcalabr1@jhu.edu
 */
public class DeIdentificationOfAllAttributesTest {
    /**
     * Creates a Dicom Element
     * 
     * @param testTag
     *            The int value of the tag
     * @param id
     *            The VR id
     * @param testValue
     *            The value that will be in the dicom element
     * @return The Dicom element created
     */
    public DicomElement createDicomElement(int testTag, VR id, String testValue) {
        DicomElement testElement = new SimpleDicomElement(testTag, id, true, testValue.getBytes(),
                null);
        return testElement;
    }

    @Test
    public void testDeIdentificationOfAllAttributes() {
        DicomElement attribute1 = createDicomElement(Tag.VerifyingObserverName, VR.PN, "testString");
        DicomElement attribute2 = createDicomElement(Tag.ContentCreatorName, VR.PN, "testString");
        DicomElement attribute3 = createDicomElement(Tag.FillerOrderNumberImagingServiceRequest,
                VR.LO, "testString");
        DicomElement attribute4 = createDicomElement(Tag.PatientBirthDate, VR.DA, "testString");
        DicomElement attribute5 = createDicomElement(Tag.PatientID, VR.LO, "testString");
        DicomElement attribute6 = createDicomElement(Tag.PatientName, VR.PN, "testString");
        DicomElement attribute7 = createDicomElement(Tag.PlacerOrderNumberImagingServiceRequest,
                VR.SH, "testString");
        DicomElement attribute8 = createDicomElement(Tag.ReferringPhysicianName, VR.PN,
                "testString");
        DicomElement attribute9 = createDicomElement(Tag.StudyID, VR.SH, "testString");
        DicomElement attribute10 = createDicomElement(
                Tag.VerifyingObserverIdentificationCodeSequence, VR.SQ, "testString");
        DicomElement attribute11 = createDicomElement(Tag.ActualHumanPerformersSequence, VR.SQ,
                "testString");
        DicomElement attribute13 = createDicomElement(Tag.AdditionalPatientHistory, VR.LT,
                "testString");
        DicomElement attribute14 = createDicomElement(Tag.AdmissionID, VR.LO, "testString");
        DicomElement attribute15 = createDicomElement(Tag.AdmittingDate, VR.DA, "testString");
        DicomElement attribute16 = createDicomElement(Tag.AdmittingDiagnosesCodeSequence, VR.SQ,
                "testString");
        DicomElement attribute17 = createDicomElement(Tag.AdmittingDiagnosesDescription, VR.LO,
                "testString");
        DicomElement attribute18 = createDicomElement(Tag.AdmittingTime, VR.TM, "testString");
        DicomElement attribute19 = createDicomElement(Tag.Allergies, VR.LO, "testString");
        DicomElement attribute20 = createDicomElement(Tag.Arbitrary, VR.LT, "testString");
        DicomElement attribute21 = createDicomElement(Tag.AuthorObserverSequence, VR.SQ,
                "testString");
        DicomElement attribute22 = createDicomElement(Tag.BranchOfService, VR.LO, "testString");
        DicomElement attribute23 = createDicomElement(Tag.CassetteID, VR.LO, "testString");
        DicomElement attribute24 = createDicomElement(
                Tag.ConfidentialityConstraintOnPatientDataDescription, VR.LO, "testString");
        DicomElement attribute25 = createDicomElement(Tag.ContentCreatorIdentificationCodeSequence,
                VR.SQ, "testString");
        DicomElement attribute26 = createDicomElement(Tag.ContributionDescription, VR.ST,
                "testString");
        DicomElement attribute27 = createDicomElement(Tag.CurrentPatientLocation, VR.LO,
                "testString");
        DicomElement attribute28 = createDicomElement(Tag.CustodialOrganizationSequence, VR.SQ,
                "testString");
        DicomElement attribute29 = createDicomElement(Tag.DataSetTrailingPadding, VR.OB,
                "testString");
        DicomElement attribute30 = createDicomElement(Tag.DerivationDescription, VR.ST,
                "testString");
        DicomElement attribute31 = createDicomElement(Tag.DetectorID, VR.SH, "testString");
        DicomElement attribute32 = createDicomElement(Tag.DeviceSerialNumber, VR.LO, "testString");
        DicomElement attribute33 = createDicomElement(Tag.DeviceUID, VR.UI, "testString");
        DicomElement attribute34 = createDicomElement(Tag.DistributionAddress, VR.LO, "testString");
        DicomElement attribute35 = createDicomElement(Tag.DistributionName, VR.PN, "testString");
        DicomElement attribute36 = createDicomElement(Tag.GantryID, VR.LO, "testString");
        DicomElement attribute37 = createDicomElement(Tag.GeneratorID, VR.LO, "testString");
        DicomElement attribute38 = createDicomElement(Tag.HumanPerformerName, VR.PN, "testString");
        DicomElement attribute39 = createDicomElement(Tag.HumanPerformerOrganization, VR.LO,
                "testString");
        DicomElement attribute40 = createDicomElement(Tag.IconImageSequence, VR.SQ, "testString");
        DicomElement attribute41 = createDicomElement(Tag.IdentifyingComments, VR.LT, "testString");
        DicomElement attribute42 = createDicomElement(Tag.InstitutionAddress, VR.ST, "testString");
        DicomElement attribute43 = createDicomElement(Tag.InstitutionalDepartmentName, VR.LO,
                "testString");
        DicomElement attribute44 = createDicomElement(Tag.InstitutionName, VR.LO, "testString");
        DicomElement attribute45 = createDicomElement(Tag.InsurancePlanIdentification, VR.LO,
                "testString");
        DicomElement attribute46 = createDicomElement(
                Tag.IntendedRecipientsOfResultsIdentificationSequence, VR.SQ, "testString");
        DicomElement attribute47 = createDicomElement(Tag.InterpretationApproverSequence, VR.SQ,
                "testString");
        DicomElement attribute48 = createDicomElement(Tag.InterpretationAuthor, VR.PN, "testString");
        DicomElement attribute49 = createDicomElement(Tag.InterpretationIDIssuer, VR.LO,
                "testString");
        DicomElement attribute50 = createDicomElement(Tag.InterpretationRecorder, VR.PN,
                "testString");
        DicomElement attribute51 = createDicomElement(Tag.InterpretationTranscriber, VR.PN,
                "testString");
        DicomElement attribute53 = createDicomElement(Tag.IssuerOfAdmissionID, VR.LO, "testString");
        DicomElement attribute55 = createDicomElement(Tag.IssuerOfPatientID, VR.LO, "testString");
        DicomElement attribute57 = createDicomElement(Tag.IssuerOfServiceEpisodeID, VR.LO,
                "testString");
        DicomElement attribute59 = createDicomElement(Tag.LastMenstrualDate, VR.DA, "testString");
        DicomElement attribute60 = createDicomElement(Tag.MedicalAlerts, VR.LO, "testString");
        DicomElement attribute61 = createDicomElement(Tag.MedicalRecordLocator, VR.LO, "testString");
        DicomElement attribute62 = createDicomElement(Tag.MilitaryRank, VR.LO, "testString");
        DicomElement attribute63 = createDicomElement(Tag.ModifyingDeviceID, VR.CS, "testString");
        DicomElement attribute64 = createDicomElement(Tag.ModifyingDeviceManufacturer, VR.LO,
                "testString");
        DicomElement attribute65 = createDicomElement(Tag.NamesOfIntendedRecipientsOfResults,
                VR.PN, "testString");
        DicomElement attribute66 = createDicomElement(Tag.ContributionDescription, VR.ST,
                "testString");
        DicomElement attribute67 = createDicomElement(Tag.Occupation, VR.SH, "testString");
        DicomElement attribute68 = createDicomElement(Tag.OperatorIdentificationSequence, VR.SQ,
                "testString");
        DicomElement attribute69 = createDicomElement(Tag.OperatorsName, VR.PN, "testString");
        DicomElement attribute70 = createDicomElement(Tag.OrderCallbackPhoneNumber, VR.SH,
                "testString");
        DicomElement attribute71 = createDicomElement(Tag.OrderEnteredBy, VR.PN, "testString");
        DicomElement attribute72 = createDicomElement(Tag.OrderEntererLocation, VR.SH, "testString");
        DicomElement attribute73 = createDicomElement(Tag.OtherPatientIDs, VR.LO, "testString");
        DicomElement attribute74 = createDicomElement(Tag.OtherPatientIDsSequence, VR.SQ,
                "testString");
        DicomElement attribute75 = createDicomElement(Tag.ParticipantSequence, VR.SQ, "testString");
        DicomElement attribute76 = createDicomElement(Tag.PatientAddress, VR.LO, "testString");
        DicomElement attribute77 = createDicomElement(Tag.PatientBirthName, VR.PN, "testString");
        DicomElement attribute78 = createDicomElement(Tag.PatientBirthTime, VR.TM, "testString");
        DicomElement attribute79 = createDicomElement(Tag.PatientComments, VR.LT, "testString");
        DicomElement attribute80 = createDicomElement(Tag.PatientInsurancePlanCodeSequence, VR.SQ,
                "testString");
        DicomElement attribute81 = createDicomElement(Tag.PatientMotherBirthName, VR.PN,
                "testString");
        DicomElement attribute82 = createDicomElement(Tag.PatientPrimaryLanguageCodeSequence,
                VR.SQ, "testString");
        DicomElement attribute83 = createDicomElement(Tag.PatientReligiousPreference, VR.LO,
                "testString");
        DicomElement attribute84 = createDicomElement(Tag.PatientState, VR.LO, "testString");
        DicomElement attribute85 = createDicomElement(Tag.PatientTelephoneNumbers, VR.SH,
                "testString");
        DicomElement attribute86 = createDicomElement(Tag.PerformedLocation, VR.SH, "testString");
        DicomElement attribute87 = createDicomElement(Tag.PerformedProcedureStepID, VR.SH,
                "testString");
        DicomElement attribute88 = createDicomElement(Tag.PerformedStationAETitle, VR.AE,
                "testString");
        DicomElement attribute89 = createDicomElement(
                Tag.PerformedStationGeographicLocationCodeSequence, VR.SQ, "testString");
        DicomElement attribute90 = createDicomElement(Tag.PerformedStationName, VR.SH, "testString");
        DicomElement attribute91 = createDicomElement(Tag.PerformedStationNameCodeSequence, VR.SQ,
                "testString");
        DicomElement attribute92 = createDicomElement(
                Tag.PerformingPhysicianIdentificationSequence, VR.SQ, "testString");
        DicomElement attribute93 = createDicomElement(Tag.PerformingPhysicianName, VR.PN,
                "testString");
        DicomElement attribute94 = createDicomElement(Tag.PersonAddress, VR.ST, "testString");
        DicomElement attribute95 = createDicomElement(Tag.PersonIdentificationCodeSequence, VR.SQ,
                "testString");
        DicomElement attribute96 = createDicomElement(Tag.PersonName, VR.PN, "testString");
        DicomElement attribute97 = createDicomElement(Tag.PersonTelephoneNumbers, VR.LO,
                "testString");
        DicomElement attribute98 = createDicomElement(Tag.PhysicianApprovingInterpretation, VR.PN,
                "testString");
        DicomElement attribute99 = createDicomElement(Tag.PhysiciansOfRecord, VR.PN, "testString");
        DicomElement attribute100 = createDicomElement(
                Tag.PhysiciansOfRecordIdentificationSequence, VR.SQ, "testString");
        DicomElement attribute101 = createDicomElement(
                Tag.PhysiciansReadingStudyIdentificationSequence, VR.SQ, "testString");
        DicomElement attribute102 = createDicomElement(Tag.PlateID, VR.LO, "testString");
        DicomElement attribute103 = createDicomElement(Tag.PreMedication, VR.LO, "testString");
        DicomElement attribute104 = createDicomElement(Tag.ProtocolName, VR.LO, "testString");
        DicomElement attribute105 = createDicomElement(Tag.ReferencedPatientAliasSequence, VR.SQ,
                "testString");
        DicomElement attribute106 = createDicomElement(Tag.ReferencedPatientSequence, VR.SQ,
                "testString");
        DicomElement attribute107 = createDicomElement(Tag.ReferencedStudySequence, VR.SQ,
                "testString");
        DicomElement attribute108 = createDicomElement(Tag.ReferringPhysicianAddress, VR.ST,
                "testString");
        DicomElement attribute109 = createDicomElement(
                Tag.ReferringPhysicianIdentificationSequence, VR.SQ, "testString");
        DicomElement attribute110 = createDicomElement(Tag.ReferringPhysicianTelephoneNumbers,
                VR.SH, "testString");
        DicomElement attribute111 = createDicomElement(Tag.RegionOfResidence, VR.LO, "testString");
        DicomElement attribute112 = createDicomElement(Tag.RequestAttributesSequence, VR.SQ,
                "testString");
        DicomElement attribute113 = createDicomElement(Tag.RequestedProcedureID, VR.SH,
                "testString");
        DicomElement attribute114 = createDicomElement(Tag.RequestedProcedureLocation, VR.LO,
                "testString");
        DicomElement attribute115 = createDicomElement(Tag.RequestingPhysician, VR.PN, "testString");
        DicomElement attribute116 = createDicomElement(Tag.RequestingService, VR.LO, "testString");
        DicomElement attribute117 = createDicomElement(Tag.ResponsibleOrganization, VR.LO,
                "testString");
        DicomElement attribute118 = createDicomElement(Tag.ResponsiblePerson, VR.PN, "testString");
        DicomElement attribute119 = createDicomElement(Tag.ResultsDistributionListSequence, VR.SQ,
                "testString");
        DicomElement attribute120 = createDicomElement(Tag.ResultsIDIssuer, VR.LO, "testString");
        DicomElement attribute121 = createDicomElement(Tag.ScheduledHumanPerformersSequence, VR.SQ,
                "testString");
        DicomElement attribute122 = createDicomElement(Tag.ScheduledPatientInstitutionResidence,
                VR.LO, "testString");
        DicomElement attribute123 = createDicomElement(
                Tag.ScheduledPerformingPhysicianIdentificationSequence, VR.SQ, "testString");
        DicomElement attribute124 = createDicomElement(Tag.ScheduledPerformingPhysicianName, VR.PN,
                "testString");
        DicomElement attribute125 = createDicomElement(Tag.ScheduledProcedureStepLocation, VR.SH,
                "testString");
        DicomElement attribute126 = createDicomElement(Tag.ScheduledStationAETitle, VR.AE,
                "testString");
        DicomElement attribute127 = createDicomElement(
                Tag.ScheduledStationGeographicLocationCodeSequence, VR.SQ, "testString");
        DicomElement attribute128 = createDicomElement(Tag.ScheduledStationName, VR.SH,
                "testString");
        DicomElement attribute129 = createDicomElement(Tag.ScheduledStationNameCodeSequence, VR.SQ,
                "testString");
        DicomElement attribute130 = createDicomElement(Tag.ScheduledStudyLocation, VR.LO,
                "testString");
        DicomElement attribute131 = createDicomElement(Tag.ScheduledStudyLocationAETitle, VR.AE,
                "testString");
        DicomElement attribute132 = createDicomElement(Tag.ServiceEpisodeID, VR.LO, "testString");
        DicomElement attribute133 = createDicomElement(Tag.SpecialNeeds, VR.LO, "testString");
        DicomElement attribute134 = createDicomElement(Tag.StationName, VR.SH, "testString");
        DicomElement attribute135 = createDicomElement(Tag.StudyIDIssuer, VR.LO, "testString");
        DicomElement attribute138 = createDicomElement(Tag.TextComments, VR.LT, "testString");
        DicomElement attribute139 = createDicomElement(Tag.TextString, VR.LO, "testString");
        DicomElement attribute140 = createDicomElement(Tag.TopicAuthor, VR.LO, "testString");
        DicomElement attribute141 = createDicomElement(Tag.TopicKeywords, VR.LO, "testString");
        DicomElement attribute142 = createDicomElement(Tag.TopicSubject, VR.ST, "testString");
        DicomElement attribute143 = createDicomElement(Tag.TopicTitle, VR.LO, "testString");
        DicomElement attribute144 = createDicomElement(Tag.VerifyingOrganization, VR.LO,
                "testString");
        DicomElement attribute145 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute146 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "1");
        DicomElement attribute147 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "1");
        DicomElement attribute148 = createDicomElement(Tag.AccessionNumber, VR.SH, "testString");
        DicomElement attribute149 = createDicomElement(Tag.AcquisitionComments, VR.LT, "testString");
        DicomElement attribute150 = createDicomElement(Tag.AcquisitionContextSequence, VR.SQ,
                "testString");
        DicomElement attribute151 = createDicomElement(Tag.AcquisitionDate, VR.DA, "testString");
        DicomElement attribute152 = createDicomElement(Tag.AcquisitionDateTime, VR.DT, "testString");
        DicomElement attribute153 = createDicomElement(Tag.AcquisitionDeviceProcessingDescription,
                VR.LO, "testString");
        DicomElement attribute154 = createDicomElement(Tag.AcquisitionProtocolDescription, VR.LT,
                "testString");
        DicomElement attribute155 = createDicomElement(Tag.AcquisitionTime, VR.DA, "testString");
        DicomElement attribute156 = createDicomElement(Tag.AffectedSOPInstanceUID, VR.UI,
                "testString");
        DicomElement attribute157 = createDicomElement(Tag.CommentsOnThePerformedProcedureStep,
                VR.ST, "testString");
        DicomElement attribute158 = createDicomElement(Tag.ContrastBolusAgent, VR.LO, "testString");
        DicomElement attribute159 = createDicomElement(Tag.ContentDate, VR.DA, "testString");
        DicomElement attribute160 = createDicomElement(Tag.ContentSequence, VR.SQ, "testString");
        DicomElement attribute161 = createDicomElement(Tag.ContentTime, VR.TM, "testString");
        DicomElement attribute162 = createDicomElement(Tag.CountryOfResidence, VR.LO, "testString");
        DicomElement attribute163 = createDicomElement(Tag.CurveData, VR.OW, "testString");
        DicomElement attribute164 = createDicomElement(Tag.CurveDate, VR.DA, "testString");
        DicomElement attribute165 = createDicomElement(Tag.CurveTime, VR.TM, "testString");
        DicomElement attribute166 = createDicomElement(Tag.DigitalSignaturesSequence, VR.SQ,
                "testString");
        DicomElement attribute167 = createDicomElement(Tag.DischargeDiagnosisDescription, VR.LO,
                "testString");
        DicomElement attribute168 = createDicomElement(Tag.EthnicGroup, VR.SH, "testString");
        DicomElement attribute169 = createDicomElement(Tag.FrameComments, VR.LT, "testString");
        DicomElement attribute170 = createDicomElement(Tag.SeriesDate, VR.DA, "testString");
        DicomElement attribute171 = createDicomElement(Tag.SeriesDescription, VR.LO, "testString");
        DicomElement attribute172 = createDicomElement(Tag.SeriesTime, VR.TM, "testString");
        DicomElement attribute173 = createDicomElement(Tag.ServiceEpisodeDescription, VR.LO,
                "testString");
        DicomElement attribute174 = createDicomElement(Tag.SmokingStatus, VR.CS, "testString");
        DicomElement attribute175 = createDicomElement(Tag.SourceImageSequence, VR.SQ, "testString");
        DicomElement attribute176 = createDicomElement(Tag.StudyComments, VR.LT, "testString");
        DicomElement attribute177 = createDicomElement(Tag.StudyDate, VR.DA, "testString");
        DicomElement attribute178 = createDicomElement(Tag.StudyDescription, VR.LO, "testString");
        DicomElement attribute179 = createDicomElement(Tag.StudyTime, VR.TM, "testString");
        DicomElement attribute180 = createDicomElement(Tag.TimezoneOffsetFromUTC, VR.SH,
                "testString");
        DicomElement attribute181 = createDicomElement(Tag.VisitComments, VR.LT, "testString");
        DicomElement attribute182 = createDicomElement(Tag.ConcatenationUID, VR.UI, "testString");
        DicomElement attribute183 = createDicomElement(Tag.ContextGroupExtensionCreatorUID, VR.UI, "testString");
        DicomElement attribute184 = createDicomElement(Tag.CreatorVersionUID, VR.UI, "testString");
        DicomElement attribute185 = createDicomElement(Tag.DimensionOrganizationUID, VR.UI, "testString");
        DicomElement attribute186 = createDicomElement(Tag.DoseReferenceUID, VR.UI, "testString");
        DicomElement attribute187 = createDicomElement(Tag.FailedSOPInstanceUIDList, VR.UI, "testString");
        DicomElement attribute188 = createDicomElement(Tag.FiducialUID, VR.UI, "testString");
        DicomElement attribute189 = createDicomElement(Tag.FrameOfReferenceUID, VR.UI, "testString");
        DicomElement attribute190 = createDicomElement(Tag.InstanceCreatorUID, VR.UI, "testString");
        DicomElement attribute191 = createDicomElement(Tag.IrradiationEventUID, VR.UI, "testString");
        DicomElement attribute192 = createDicomElement(Tag.LargePaletteColorLookupTableUID, VR.UI, "testString");
        DicomElement attribute193 = createDicomElement(Tag.MediaStorageSOPInstanceUID, VR.UI, "testString");
        DicomElement attribute194 = createDicomElement(Tag.PaletteColorLookupTableUID, VR.UI, "testString");
        DicomElement attribute195 = createDicomElement(Tag.ReferencedFrameOfReferenceUID, VR.UI, "testString");
        DicomElement attribute196 = createDicomElement(Tag.ReferencedGeneralPurposeScheduledProcedureStepTransactionUID, VR.UI, "testString");
        DicomElement attribute197 = createDicomElement(Tag.ReferencedSOPInstanceUID, VR.UI, "testString");
        DicomElement attribute198 = createDicomElement(Tag.ReferencedSOPInstanceUIDInFile, VR.UI, "testString");
        DicomElement attribute199 = createDicomElement(Tag.RelatedFrameOfReferenceUID, VR.UI, "testString");
        DicomElement attribute200 = createDicomElement(Tag.RequestedSOPInstanceUID, VR.UI, "testString");
        DicomElement attribute201 = createDicomElement(Tag.StorageMediaFileSetUID, VR.UI, "testString");
        DicomElement attribute203 = createDicomElement(Tag.SynchronizationFrameOfReferenceUID, VR.UI, "testString");
        DicomElement attribute204 = createDicomElement(Tag.TemplateExtensionCreatorUID, VR.UI, "testString");
        DicomElement attribute205 = createDicomElement(Tag.TemplateExtensionOrganizationUID, VR.UI, "testString");
        DicomElement attribute206 = createDicomElement(Tag.TransactionUID, VR.UI, "testString");
        DicomElement attribute207 = createDicomElement(Tag.UID, VR.UI, "testString");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);
        studyTestDcmObj.add(attribute6);
        studyTestDcmObj.add(attribute7);
        studyTestDcmObj.add(attribute8);
        studyTestDcmObj.add(attribute9);
        studyTestDcmObj.add(attribute10);
        studyTestDcmObj.add(attribute11);
        studyTestDcmObj.add(attribute13);
        studyTestDcmObj.add(attribute14);
        studyTestDcmObj.add(attribute15);
        studyTestDcmObj.add(attribute16);
        studyTestDcmObj.add(attribute17);
        studyTestDcmObj.add(attribute18);
        studyTestDcmObj.add(attribute19);
        studyTestDcmObj.add(attribute20);
        studyTestDcmObj.add(attribute21);
        studyTestDcmObj.add(attribute22);
        studyTestDcmObj.add(attribute23);
        studyTestDcmObj.add(attribute24);
        studyTestDcmObj.add(attribute25);
        studyTestDcmObj.add(attribute26);
        studyTestDcmObj.add(attribute27);
        studyTestDcmObj.add(attribute28);
        studyTestDcmObj.add(attribute29);
        studyTestDcmObj.add(attribute30);
        studyTestDcmObj.add(attribute31);
        studyTestDcmObj.add(attribute32);
        studyTestDcmObj.add(attribute33);
        studyTestDcmObj.add(attribute34);
        studyTestDcmObj.add(attribute35);
        studyTestDcmObj.add(attribute36);
        studyTestDcmObj.add(attribute37);
        studyTestDcmObj.add(attribute38);
        studyTestDcmObj.add(attribute39);
        studyTestDcmObj.add(attribute40);
        studyTestDcmObj.add(attribute41);
        studyTestDcmObj.add(attribute42);
        studyTestDcmObj.add(attribute43);
        studyTestDcmObj.add(attribute44);
        studyTestDcmObj.add(attribute45);
        studyTestDcmObj.add(attribute46);
        studyTestDcmObj.add(attribute47);
        studyTestDcmObj.add(attribute48);
        studyTestDcmObj.add(attribute49);
        studyTestDcmObj.add(attribute50);
        studyTestDcmObj.add(attribute51);
        studyTestDcmObj.add(attribute53);
        studyTestDcmObj.add(attribute55);
        studyTestDcmObj.add(attribute57);
        studyTestDcmObj.add(attribute59);
        studyTestDcmObj.add(attribute60);
        studyTestDcmObj.add(attribute61);
        studyTestDcmObj.add(attribute62);
        studyTestDcmObj.add(attribute63);
        studyTestDcmObj.add(attribute64);
        studyTestDcmObj.add(attribute65);
        studyTestDcmObj.add(attribute66);
        studyTestDcmObj.add(attribute67);
        studyTestDcmObj.add(attribute68);
        studyTestDcmObj.add(attribute69);
        studyTestDcmObj.add(attribute70);
        studyTestDcmObj.add(attribute71);
        studyTestDcmObj.add(attribute72);
        studyTestDcmObj.add(attribute73);
        studyTestDcmObj.add(attribute74);
        studyTestDcmObj.add(attribute75);
        studyTestDcmObj.add(attribute76);
        studyTestDcmObj.add(attribute77);
        studyTestDcmObj.add(attribute78);
        studyTestDcmObj.add(attribute79);
        studyTestDcmObj.add(attribute80);
        studyTestDcmObj.add(attribute81);
        studyTestDcmObj.add(attribute82);
        studyTestDcmObj.add(attribute83);
        studyTestDcmObj.add(attribute84);
        studyTestDcmObj.add(attribute85);
        studyTestDcmObj.add(attribute86);
        studyTestDcmObj.add(attribute87);
        studyTestDcmObj.add(attribute88);
        studyTestDcmObj.add(attribute89);
        studyTestDcmObj.add(attribute90);
        studyTestDcmObj.add(attribute91);
        studyTestDcmObj.add(attribute92);
        studyTestDcmObj.add(attribute93);
        studyTestDcmObj.add(attribute94);
        studyTestDcmObj.add(attribute95);
        studyTestDcmObj.add(attribute96);
        studyTestDcmObj.add(attribute97);
        studyTestDcmObj.add(attribute98);
        studyTestDcmObj.add(attribute99);
        studyTestDcmObj.add(attribute100);
        studyTestDcmObj.add(attribute101);
        studyTestDcmObj.add(attribute102);
        studyTestDcmObj.add(attribute103);
        studyTestDcmObj.add(attribute104);
        studyTestDcmObj.add(attribute105);
        studyTestDcmObj.add(attribute106);
        studyTestDcmObj.add(attribute107);
        studyTestDcmObj.add(attribute108);
        studyTestDcmObj.add(attribute109);
        studyTestDcmObj.add(attribute110);
        studyTestDcmObj.add(attribute111);
        studyTestDcmObj.add(attribute112);
        studyTestDcmObj.add(attribute113);
        studyTestDcmObj.add(attribute114);
        studyTestDcmObj.add(attribute115);
        studyTestDcmObj.add(attribute116);
        studyTestDcmObj.add(attribute117);
        studyTestDcmObj.add(attribute118);
        studyTestDcmObj.add(attribute119);
        studyTestDcmObj.add(attribute120);
        studyTestDcmObj.add(attribute121);
        studyTestDcmObj.add(attribute122);
        studyTestDcmObj.add(attribute123);
        studyTestDcmObj.add(attribute124);
        studyTestDcmObj.add(attribute125);
        studyTestDcmObj.add(attribute126);
        studyTestDcmObj.add(attribute127);
        studyTestDcmObj.add(attribute128);
        studyTestDcmObj.add(attribute129);
        studyTestDcmObj.add(attribute130);
        studyTestDcmObj.add(attribute131);
        studyTestDcmObj.add(attribute132);
        studyTestDcmObj.add(attribute133);
        studyTestDcmObj.add(attribute134);
        studyTestDcmObj.add(attribute135);
        studyTestDcmObj.add(attribute138);
        studyTestDcmObj.add(attribute139);
        studyTestDcmObj.add(attribute140);
        studyTestDcmObj.add(attribute141);
        studyTestDcmObj.add(attribute142);
        studyTestDcmObj.add(attribute143);
        studyTestDcmObj.add(attribute144);
        studyTestDcmObj.add(attribute145);
        studyTestDcmObj.add(attribute146);
        studyTestDcmObj.add(attribute147);
        studyTestDcmObj.add(attribute148);
        studyTestDcmObj.add(attribute149);
        studyTestDcmObj.add(attribute150);
        studyTestDcmObj.add(attribute151);
        studyTestDcmObj.add(attribute152);
        studyTestDcmObj.add(attribute153);
        studyTestDcmObj.add(attribute154);
        studyTestDcmObj.add(attribute155);
        studyTestDcmObj.add(attribute156);
        studyTestDcmObj.add(attribute157);
        studyTestDcmObj.add(attribute158);
        studyTestDcmObj.add(attribute159);
        studyTestDcmObj.add(attribute160);
        studyTestDcmObj.add(attribute161);
        studyTestDcmObj.add(attribute162);
        studyTestDcmObj.add(attribute163);
        studyTestDcmObj.add(attribute164);
        studyTestDcmObj.add(attribute165);
        studyTestDcmObj.add(attribute166);
        studyTestDcmObj.add(attribute167);
        studyTestDcmObj.add(attribute168);
        studyTestDcmObj.add(attribute169);
        studyTestDcmObj.add(attribute170);
        studyTestDcmObj.add(attribute171);
        studyTestDcmObj.add(attribute172);
        studyTestDcmObj.add(attribute173);
        studyTestDcmObj.add(attribute174);
        studyTestDcmObj.add(attribute175);
        studyTestDcmObj.add(attribute176);
        studyTestDcmObj.add(attribute177);
        studyTestDcmObj.add(attribute178);
        studyTestDcmObj.add(attribute179);
        studyTestDcmObj.add(attribute180);
        studyTestDcmObj.add(attribute181);
        studyTestDcmObj.add(attribute182);
        studyTestDcmObj.add(attribute183);
        studyTestDcmObj.add(attribute184);
        studyTestDcmObj.add(attribute185);
        studyTestDcmObj.add(attribute186);
        studyTestDcmObj.add(attribute187);
        studyTestDcmObj.add(attribute188);
        studyTestDcmObj.add(attribute189);
        studyTestDcmObj.add(attribute190);
        studyTestDcmObj.add(attribute191);
        studyTestDcmObj.add(attribute192);
        studyTestDcmObj.add(attribute193);
        studyTestDcmObj.add(attribute194);
        studyTestDcmObj.add(attribute195);
        studyTestDcmObj.add(attribute196);
        studyTestDcmObj.add(attribute197);
        studyTestDcmObj.add(attribute198);
        studyTestDcmObj.add(attribute199);
        studyTestDcmObj.add(attribute200);
        studyTestDcmObj.add(attribute201);
        studyTestDcmObj.add(attribute203);
        studyTestDcmObj.add(attribute204);
        studyTestDcmObj.add(attribute205);
        studyTestDcmObj.add(attribute206);
        studyTestDcmObj.add(attribute207);

        Store store = StoreFactory.createStore("testStoreFS.properties");
        Study testStudy = new Study(store, new DicomUID("1"), true, studyTestDcmObj);// (null, new DicomUID("1"));
        StudyProfileOptions pOptions = new StudyProfileOptions();
        testStudy.deIdentifyStudy(pOptions);

        // dummied ~ D
        // TODO - test for all dummy tags when they are de-identified correctly

        /*
         * replace with a non-zero length UID that is internally consistent
         * within a set of Instances ~ U
         */
       
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.ConcatenationUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.ContextGroupExtensionCreatorUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.CreatorVersionUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.DeviceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.DimensionOrganizationUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.DoseReferenceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.FailedSOPInstanceUIDList).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.FiducialUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.FrameOfReferenceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.InstanceCreatorUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.IrradiationEventUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.LargePaletteColorLookupTableUID).equals("testString"));
       // assertTrue(!testStudy.getValueForAttributeAsString(Tag.MediaStorageSOPInstanceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.PaletteColorLookupTableUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.ReferencedFrameOfReferenceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.ReferencedGeneralPurposeScheduledProcedureStepTransactionUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.ReferencedSOPInstanceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.TransactionUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.ReferencedSOPInstanceUIDInFile).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.RelatedFrameOfReferenceUID).equals("testString"));
        /*These three tags below are set to null for some reason*/
        //assertTrue(!testStudy.getValueForAttributeAsString(Tag.RequestedSOPInstanceUID).equals("testString"));
        //assertTrue(!testStudy.getValueForAttributeAsString(Tag.SeriesInstanceUID).equals("1"));
        //assertTrue(!testStudy.getValueForAttributeAsString(Tag.SOPInstanceUID).equals("1"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.StorageMediaFileSetUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.SynchronizationFrameOfReferenceUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.TemplateExtensionCreatorUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.TemplateExtensionOrganizationUID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.UID).equals("testString"));
        assertTrue(!testStudy.getValueForAttributeAsString(Tag.StudyInstanceUID).equals("1"));

        // zeroed ~ Z
        assertNull(testStudy.getValueForAttributeAsString(Tag.AccessionNumber));
        assertNotNull(testStudy.getAttribute(Tag.AccessionNumber));
        assertNull(testStudy.getValueForAttributeAsString(Tag.ContentCreatorName));
        assertNotNull(testStudy.getAttribute(Tag.ContentCreatorName));
        assertNull(testStudy.getValueForAttributeAsString(Tag.ContentDate));
        assertNotNull(testStudy.getAttribute(Tag.ContentDate));
        assertNull(testStudy.getValueForAttributeAsString(Tag.ContentTime));
        assertNotNull(testStudy.getAttribute(Tag.ContentTime));
        assertNull(testStudy.getValueForAttributeAsString(Tag.ContrastBolusAgent));
        assertNotNull(testStudy.getAttribute(Tag.ContrastBolusAgent));
        assertNull(testStudy
                .getValueForAttributeAsString(Tag.FillerOrderNumberImagingServiceRequest));
        assertNotNull(testStudy.getAttribute(Tag.FillerOrderNumberImagingServiceRequest));
        assertNull(testStudy.getValueForAttributeAsString(Tag.PatientBirthDate));
        assertNotNull(testStudy.getAttribute(Tag.PatientBirthDate));
        assertNull(testStudy.getValueForAttributeAsString(Tag.PatientID));
        assertNotNull(testStudy.getAttribute(Tag.PatientID));
        assertNull(testStudy.getValueForAttributeAsString(Tag.PatientName));
        assertNotNull(testStudy.getAttribute(Tag.PatientName));
        assertNull(testStudy
                .getValueForAttributeAsString(Tag.PlacerOrderNumberImagingServiceRequest));
        assertNotNull(testStudy.getAttribute(Tag.PlacerOrderNumberImagingServiceRequest));
        assertNull(testStudy.getValueForAttributeAsString(Tag.ReferringPhysicianName));
        assertNotNull(testStudy.getAttribute(Tag.PlacerOrderNumberImagingServiceRequest));
        assertNull(testStudy.getValueForAttributeAsString(Tag.StudyID));
        assertNotNull(testStudy.getAttribute(Tag.StudyID));
        assertNull(testStudy.getValueForAttributeAsString(Tag.StudyDate));
        assertNotNull(testStudy.getAttribute(Tag.StudyDate));
        assertNull(testStudy.getValueForAttributeAsString(Tag.StudyTime));
        assertNotNull(testStudy.getAttribute(Tag.StudyTime));
        assertNotNull(testStudy.getAttribute(Tag.VerifyingObserverIdentificationCodeSequence));

        // removed ~ X
        assertNull(testStudy.getAttribute(Tag.AcquisitionComments));
        assertNull(testStudy.getAttribute(Tag.AcquisitionContextSequence));
        assertNull(testStudy.getAttribute(Tag.AcquisitionDate));
        assertNull(testStudy.getAttribute(Tag.AcquisitionDateTime));
        assertNull(testStudy.getAttribute(Tag.AcquisitionDeviceProcessingDescription));
        assertNull(testStudy.getAttribute(Tag.AcquisitionProtocolDescription));
        assertNull(testStudy.getAttribute(Tag.AcquisitionTime));
        assertNull(testStudy.getAttribute(Tag.AdditionalPatientHistory));
        assertNull(testStudy.getAttribute(Tag.AdmittingDiagnosesCodeSequence));
        assertNull(testStudy.getAttribute(Tag.AdmissionID));
        assertNull(testStudy.getAttribute(Tag.AdmittingDate));
        assertNull(testStudy.getAttribute(Tag.AdmittingDiagnosesDescription));
        assertNull(testStudy.getAttribute(Tag.AdmittingTime));
        assertNull(testStudy.getAttribute(Tag.AffectedSOPInstanceUID));
        assertNull(testStudy.getAttribute(Tag.Allergies));
        assertNull(testStudy.getAttribute(Tag.Arbitrary));
        assertNull(testStudy.getAttribute(Tag.AuthorObserverSequence));
        assertNull(testStudy.getAttribute(Tag.BranchOfService));
        assertNull(testStudy.getAttribute(Tag.CassetteID));
        assertNull(testStudy.getAttribute(Tag.ConfidentialityConstraintOnPatientDataDescription));
        assertNull(testStudy.getAttribute(Tag.ContributionDescription));
        assertNull(testStudy.getAttribute(Tag.ContentSequence));
        assertNull(testStudy.getAttribute(Tag.CommentsOnThePerformedProcedureStep));
        assertNull(testStudy.getAttribute(Tag.CountryOfResidence));
        assertNull(testStudy.getAttribute(Tag.CurrentPatientLocation));
        assertNull(testStudy.getAttribute(Tag.CurveData));
        assertNull(testStudy.getAttribute(Tag.CurveDate));
        assertNull(testStudy.getAttribute(Tag.CurveTime));
        assertNull(testStudy.getAttribute(Tag.DataSetTrailingPadding));
        assertNull(testStudy.getAttribute(Tag.DerivationDescription));
        assertNull(testStudy.getAttribute(Tag.DetectorID));
        assertNull(testStudy.getAttribute(Tag.DeviceSerialNumber));
        assertNull(testStudy.getAttribute(Tag.DistributionAddress));
        assertNull(testStudy.getAttribute(Tag.DistributionName));
        assertNull(testStudy.getAttribute(Tag.DischargeDiagnosisDescription));
        assertNull(testStudy.getAttribute(Tag.DigitalSignaturesSequence));
        assertNull(testStudy.getAttribute(Tag.EthnicGroup));
        assertNull(testStudy.getAttribute(Tag.FrameComments));
        assertNull(testStudy.getAttribute(Tag.GantryID));
        assertNull(testStudy.getAttribute(Tag.GeneratorID));
        assertNull(testStudy.getAttribute(Tag.HumanPerformerName));
        assertNull(testStudy.getAttribute(Tag.HumanPerformerOrganization));
        assertNull(testStudy.getAttribute(Tag.IdentifyingComments));
        assertNull(testStudy.getAttribute(Tag.InstitutionAddress));
        assertNull(testStudy.getAttribute(Tag.InstitutionalDepartmentName));
        assertNull(testStudy.getAttribute(Tag.InstitutionName));
        assertNull(testStudy.getAttribute(Tag.InsurancePlanIdentification));
        assertNull(testStudy.getAttribute(Tag.InterpretationAuthor));
        assertNull(testStudy.getAttribute(Tag.InterpretationIDIssuer));
        assertNull(testStudy.getAttribute(Tag.InterpretationRecorder));
        assertNull(testStudy.getAttribute(Tag.InterpretationTranscriber));
        assertNull(testStudy.getAttribute(Tag.IssuerOfAdmissionID));
        assertNull(testStudy.getAttribute(Tag.IssuerOfPatientID));
        assertNull(testStudy.getAttribute(Tag.IssuerOfServiceEpisodeID));
        assertNull(testStudy.getAttribute(Tag.LastMenstrualDate));
        assertNull(testStudy.getAttribute(Tag.MedicalAlerts));
        assertNull(testStudy.getAttribute(Tag.MedicalRecordLocator));
        assertNull(testStudy.getAttribute(Tag.MilitaryRank));
        assertNull(testStudy.getAttribute(Tag.ModifyingDeviceID));
        assertNull(testStudy.getAttribute(Tag.ModifyingDeviceManufacturer));
        assertNull(testStudy.getAttribute(Tag.NameOfPhysiciansReadingStudy));
        assertNull(testStudy.getAttribute(Tag.NamesOfIntendedRecipientsOfResults));
        assertNull(testStudy.getAttribute(Tag.Occupation));
        assertNull(testStudy.getAttribute(Tag.OperatorsName));
        assertNull(testStudy.getAttribute(Tag.OrderCallbackPhoneNumber));
        assertNull(testStudy.getAttribute(Tag.OrderEnteredBy));
        assertNull(testStudy.getAttribute(Tag.OrderEntererLocation));
        assertNull(testStudy.getAttribute(Tag.OtherPatientIDs));
        assertNull(testStudy.getAttribute(Tag.OtherPatientNames));
        assertNull(testStudy.getAttribute(Tag.PatientAddress));
        assertNull(testStudy.getAttribute(Tag.PatientBirthName));
        assertNull(testStudy.getAttribute(Tag.PatientBirthTime));
        assertNull(testStudy.getAttribute(Tag.PatientComments));
        assertNull(testStudy.getAttribute(Tag.PatientMotherBirthName));
        assertNull(testStudy.getAttribute(Tag.PatientReligiousPreference));
        assertNull(testStudy.getAttribute(Tag.PatientState));
        assertNull(testStudy.getAttribute(Tag.PatientTelephoneNumbers));
        assertNull(testStudy.getAttribute(Tag.PerformedLocation));
        assertNull(testStudy.getAttribute(Tag.PerformedProcedureStepID));
        assertNull(testStudy.getAttribute(Tag.PerformedStationAETitle));
        assertNull(testStudy.getAttribute(Tag.PerformedStationName));
        assertNull(testStudy.getAttribute(Tag.PerformedStationNameCodeSequence));
        assertNull(testStudy.getAttribute(Tag.PerformedStationGeographicLocationCodeSequence));
        assertNull(testStudy.getAttribute(Tag.PerformingPhysicianName));
        assertNull(testStudy.getAttribute(Tag.PersonAddress));
        assertNull(testStudy.getAttribute(Tag.PersonTelephoneNumbers));
        assertNull(testStudy.getAttribute(Tag.PhysicianApprovingInterpretation));
        assertNull(testStudy.getAttribute(Tag.PhysiciansOfRecord));
        assertNull(testStudy.getAttribute(Tag.PlateID));
        assertNull(testStudy.getAttribute(Tag.PreMedication));
        assertNull(testStudy.getAttribute(Tag.ProtocolName));
        assertNull(testStudy.getAttribute(Tag.ReferencedPatientAliasSequence));
        assertNull(testStudy.getAttribute(Tag.ReferringPhysicianAddress));
        assertNull(testStudy.getAttribute(Tag.ReferringPhysicianTelephoneNumbers));
        assertNull(testStudy.getAttribute(Tag.RegionOfResidence));
        assertNull(testStudy.getAttribute(Tag.RequestedProcedureID));
        assertNull(testStudy.getAttribute(Tag.RequestedProcedureLocation));
        assertNull(testStudy.getAttribute(Tag.RequestingPhysician));
        assertNull(testStudy.getAttribute(Tag.RequestingService));
        assertNull(testStudy.getAttribute(Tag.ResponsibleOrganization));
        assertNull(testStudy.getAttribute(Tag.ResponsiblePerson));
        assertNull(testStudy.getAttribute(Tag.ResultsIDIssuer));
        assertNull(testStudy.getAttribute(Tag.ScheduledPatientInstitutionResidence));
        assertNull(testStudy.getAttribute(Tag.ScheduledPerformingPhysicianName));
        assertNull(testStudy.getAttribute(Tag.ScheduledProcedureStepLocation));
        assertNull(testStudy.getAttribute(Tag.ScheduledStationGeographicLocationCodeSequence));
        assertNull(testStudy.getAttribute(Tag.ScheduledStationAETitle));
        assertNull(testStudy.getAttribute(Tag.ScheduledStationName));
        assertNull(testStudy.getAttribute(Tag.ScheduledStationNameCodeSequence));
        assertNull(testStudy.getAttribute(Tag.ScheduledStudyLocation));
        assertNull(testStudy.getAttribute(Tag.ScheduledStudyLocationAETitle));
        assertNull(testStudy.getAttribute(Tag.ServiceEpisodeID));
        assertNull(testStudy.getAttribute(Tag.ServiceEpisodeDescription));
        assertNull(testStudy.getAttribute(Tag.SeriesDate));
        assertNull(testStudy.getAttribute(Tag.SeriesDescription));
        assertNull(testStudy.getAttribute(Tag.SeriesTime));
        assertNull(testStudy.getAttribute(Tag.SmokingStatus));
        assertNull(testStudy.getAttribute(Tag.SourceImageSequence));
        assertNull(testStudy.getAttribute(Tag.SpecialNeeds));
        assertNull(testStudy.getAttribute(Tag.StationName));
        assertNull(testStudy.getAttribute(Tag.StudyDescription));
        assertNull(testStudy.getAttribute(Tag.StudyComments));
        assertNull(testStudy.getAttribute(Tag.StudyIDIssuer));
        assertNull(testStudy.getAttribute(Tag.TextComments));
        assertNull(testStudy.getAttribute(Tag.TextString));
        assertNull(testStudy.getAttribute(Tag.TimezoneOffsetFromUTC));
        assertNull(testStudy.getAttribute(Tag.TopicAuthor));
        assertNull(testStudy.getAttribute(Tag.TopicKeywords));
        assertNull(testStudy.getAttribute(Tag.TopicSubject));
        assertNull(testStudy.getAttribute(Tag.TopicTitle));
        assertNull(testStudy.getAttribute(Tag.VerifyingOrganization));
        assertNull(testStudy.getAttribute(Tag.VisitComments));

    }
}