/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.io;

import static org.junit.Assert.assertTrue;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Tests the conversion from the MS-DICOM Study data model to single-frame DICOM
 * files for accuracy.
 * 
 * @author Raphael Yu Ning
 */
public class SingleFrameDicomWriterTest {

    private final String OUTPUT_DIR = "target/test-out/SingleFrameDicomWriterTest";

    private Study study;

    private Study singleFrameStudy;

    private Store store;

    @Before
    public void setUp() {
        store = StoreFactory.createStore("testStoreFS.properties");
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    private void doTestWriteSingleFrame(String studyName) throws IOException {
        File outputDir = new File(OUTPUT_DIR);
        outputDir.mkdirs();
        study = TestUtils.readStudyFromResource(store, studyName);
        File singleFrameOutputDir = TestUtils.writeSingleFrame(study, outputDir, studyName, false);
        singleFrameStudy = TestUtils.readStudyFromFile(store, singleFrameOutputDir);
        assertTrue(study.equals(singleFrameStudy));
    }

    @Test
    public void testReadSingleFrameWriteSingleFrame() throws IOException {
        doTestWriteSingleFrame("20phase");
    }

    @Test
    public void testReadMultiFrameWriteSingleFrame() throws IOException {
        doTestWriteSingleFrame("nemamfct");
    }

}
