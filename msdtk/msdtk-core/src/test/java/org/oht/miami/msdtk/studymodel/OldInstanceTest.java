/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Tests the MS-DICOM Instance data model functionality and accuracy.
 * 
 * @author Sam Hu
 * @author Raphael Yu Ning
 */
public class OldInstanceTest {

    private Study study = null;
    private Instance instance = null;
    private Store store;

    // TODO: use 20phase instead of MINT10
    private static final String TEST_STUDY_NAME = "MINT10";
    private static final String TEST_SOP_IUID = "2.16.840.1.114255.393386351.1568457295.17895.5";

    @Before
    public void setUp() throws IOException {

        store = StoreFactory.createStore("testStoreFS.properties");
        study = TestUtils.readStudyFromResource(store, TEST_STUDY_NAME);
        instance = study.getInstance(TEST_SOP_IUID);
    }

    @After
    public void tearDown() throws IOException {

        TestUtils.cleanStore(store);
    }

    /**
     * Compares the expected SOP IUID with what was loaded into the data model.
     */
    @Test
    public void testGetSopInstanceUid() {

        testGetSopInstanceUid(instance);
    }

    private void testGetSopInstanceUid(Instance instance) {

        assertEquals("2.16.840.1.114255.393386351.1568457295.17895.5", instance.getSOPInstanceUID());
    }

    /**
     * Tests getting the transfer syntax UID for the instance.
     */
    @Test
    public void testGetTransferSyntaxUid() {

        testGetTransferSyntaxUid(instance);
    }

    private static void testGetTransferSyntaxUid(Instance instance) {

        assertEquals("1.2.840.10008.1.2.1", instance.getTransferSyntaxUID());
    }

    /**
     * Tests putting attributes into an instance. <br/>
     * Should throw an exception if an attribute with the same tag already
     * exists in the instance.
     */
    @Test
    public void testPutAttribute() {

        testPutAttribute(instance);
    }

    private static void testPutAttribute(Instance instance) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";
        final String TEST_NEW_VALUE = "another test attribute";

        instance.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        DicomElement attr = instance.getAttribute(TEST_TAG);
        assertEquals(TEST_VALUE, new String(attr.getBytes()));

        try {
            instance.putAttribute(new SimpleDicomElement(TEST_TAG, VR.AE, false, TEST_NEW_VALUE
                    .getBytes(), null));
            fail("Missing expected exception");
        } catch (IllegalArgumentException e) {
            DicomElement newAttr = instance.getAttribute(TEST_TAG);
            assertEquals(TEST_VALUE, new String(newAttr.getBytes()));
        }
    }

    /**
     * Tests getting the value of an attribute in the instance.
     */
    @Test
    public void testGetValue() {

        testGetValue(instance);
    }

    private void testGetValue(Instance instance) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";

        assertNull(instance.getValueForAttributeAsString(TEST_TAG));
        assertNull(instance.getValueForAttribute(TEST_TAG));

        instance.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        assertEquals(TEST_VALUE, instance.getValueForAttributeAsString(TEST_TAG));
        assertArrayEquals(TEST_VALUE.getBytes(), instance.getValueForAttribute(TEST_TAG));
    }

    /**
     * Tests removing an attribute from the instance.
     */
    @Test
    public void testRemoveAttribute() {

        testRemoveAttribute(instance);
    }

    private void testRemoveAttribute(Instance instance) {

        final int TEST_TAG = 0xffffffff;
        final String TEST_VALUE = "test attribute";

        instance.putAttribute(new SimpleDicomElement(TEST_TAG, VR.UN, true, TEST_VALUE.getBytes(),
                null));
        assertNotNull(instance.getAttribute(TEST_TAG));

        instance.removeAttribute(TEST_TAG);
        assertNull(instance.getAttribute(TEST_TAG));
    }

    /**
     * Compares expected elements in the DICOM file with those loaded into the
     * Instance model.
     */
    @Test
    public void testInstanceAttributes() {

        testInstanceAttributes(instance);
    }

    private void testInstanceAttributes(Instance instance) {

        boolean isNormalized = instance.getSeriesParent().getStudyParent().isNormalized();
        DicomElement attr = null;

        attr = instance.getAttribute(Tag.FileMetaInformationVersion);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("OB", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.MediaStorageSOPClassUID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            // FIXME: SpecificCharacterSet should not always be null
            assertEquals("1.2.840.10008.5.1.4.1.1.1", attr.getValueAsString(null, 0));
            assertEquals("UI", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.MediaStorageSOPInstanceUID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("2.16.840.1.114255.393386351.1568457295.17895.5",
                    attr.getValueAsString(null, 0));
            assertEquals("UI", attr.vr().toString());
        }

        attr = instance.findAttribute(Tag.TransferSyntaxUID);
        // this attribute is not subject to normalization
        assertEquals("1.2.840.10008.1.2.1", attr.getValueAsString(null, 0));
        assertEquals("UI", attr.vr().toString());

        attr = instance.getAttribute(Tag.ImplementationClassUID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1.2.250.1.59.3.0.3.5.3", attr.getValueAsString(null, 0));
            assertEquals("UI", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ImplementationVersionName);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("ETIAM_DCMTK_353", attr.getValueAsString(null, 0));
            assertEquals("SH", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SourceApplicationEntityTitle);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("IMG_ARCH_UV", attr.getValueAsString(null, 0));
            assertEquals("AE", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SpecificCharacterSet);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("ISO_IR 100", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ImageType);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("ORIGINAL\\PRIMARY", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SOPClassUID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1.2.840.10008.5.1.4.1.1.1", attr.getValueAsString(null, 0));
            assertEquals("UI", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SOPInstanceUID);
        // this attribute is not subject to normalization
        assertEquals("2.16.840.1.114255.393386351.1568457295.17895.5",
                attr.getValueAsString(null, 0));
        assertEquals("UI", attr.vr().toString());

        attr = instance.getAttribute(Tag.AcquisitionDate);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20100408", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ContentDate);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20100408", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.AcquisitionTime);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("073007.000000", attr.getValueAsString(null, 0));
            assertEquals("TM", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ContentTime);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("073007.000000", attr.getValueAsString(null, 0));
            assertEquals("TM", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RetrieveAETitle);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("AE_TITLE", attr.getValueAsString(null, 0));
            assertEquals("AE", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Manufacturer);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("SIEMENS", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.InstitutionName);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("Johns Hopkins Hospital", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.InstitutionAddress);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("ST", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StationName);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("JHEB ED1 CMSCB", attr.getValueAsString(null, 0));
            assertEquals("SH", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.InstitutionalDepartmentName);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ManufacturerModelName);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("SIEMENS FD-X", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DerivationDescription);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("ST", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00090010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("EMAGEON STUDY HOME", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00091000);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("JHH001002", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00091001);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("emageonjhu", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.IssuerOfPatientID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("JHH", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PatientAddress);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.MedicalAlerts);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("ROSUVASTATIN", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Allergies);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("DA^No Known Allergies", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.CountryOfResidence);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("U", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PatientTelephoneNumbers);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("4412368532", attr.getValueAsString(null, 0));
            assertEquals("SH", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SmokingStatus);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UNKNOWN", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PregnancyStatus);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("4", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.LastMenstrualDate);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20040529", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SliceThickness);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.KVP);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("117", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.EchoTime);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SpacingBetweenSlices);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DeviceSerialNumber);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1606", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PlateID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("SN060345       |DC0604         |SW61538691A    |PN00100204HD",
                    attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SoftwareVersions);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("VB20E", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DistanceSourceToDetector);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1800", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ExposureTime);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("8", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.XRayTubeCurrent);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("537", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Exposure);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("4", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ExposureInuAs);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("4350", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ImageAndFluoroscopyAreaDoseProduct);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.FilterType);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("NONE", attr.getValueAsString(null, 0));
            assertEquals("SH", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ImagerPixelSpacing);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0.143\\0.143", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Grid);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("FOCUSED", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.CollimatorGridName);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("180cm", attr.getValueAsString(null, 0));
            assertEquals("SH", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DateOfLastCalibration);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20100204", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.AcquisitionDeviceProcessingCode);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RelativeXRayExposure);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("361", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ViewPosition);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertNull(attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Sensitivity);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("400", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DetectorConditionsNominalFlag);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("YES", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DetectorTemperature);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("36", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.DateOfLastDetectorCalibration);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20100204", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.TimeOfLastDetectorCalibration);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("183455", attr.getValueAsString(null, 0));
            assertEquals("TM", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.GridFocalDistance);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1800", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00190010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("Siemens: Thorax/Multix FD Lab Settings", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00191002);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00191005);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00191006);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00191007);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00191008);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.AcquisitionNumber);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("2", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.InstanceNumber);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PatientOrientation);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ImagesInAcquisition);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SliceLocation);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ImageComments);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("LT", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00210010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("Siemens: Thorax/Multix FD Post Processing",
                    attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211000);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211001);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211002);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211003);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211004);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211005);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211006);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211007);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211008);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211009);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0021100a);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0021100b);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0021100c);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0021100d);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0021100e);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0021100f);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211011);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211012);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211013);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211014);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211015);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00211016);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00250010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("Siemens: Thorax/Multix FD Raw Image Settings",
                    attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251000);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251001);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251002);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251003);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251004);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251005);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251006);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251007);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251008);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251009);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0025100a);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0025100b);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0025100c);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0025100d);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0025100e);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x0025100f);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251011);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251012);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251013);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251014);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251015);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251016);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251017);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251018);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00251019);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SamplesPerPixel);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PhotometricInterpretation);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("MONOCHROME2", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PlanarConfiguration);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Rows);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("2991", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.Columns);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("2442", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PixelAspectRatio);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0\\0", attr.getValueAsString(null, 0));
            assertEquals("IS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.BitsAllocated);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("16", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.BitsStored);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("12", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.HighBit);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("11", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PixelRepresentation);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.SmallestImagePixelValue);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.LargestImagePixelValue);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("0", attr.getValueAsString(null, 0));
            assertEquals("US", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.BurnedInAnnotation);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("NO", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.WindowCenter);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("1829.0", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.WindowWidth);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("3264.0", attr.getValueAsString(null, 0));
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RescaleIntercept);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("DS", attr.vr().toString());
        }

        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            attr = instance.getAttribute(Tag.RescaleSlope);
            assertEquals("DS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RescaleType);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.LossyImageCompression);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("00", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00290010);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("SIEMENS MEDCOM HEADER", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00291031);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00291032);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00291033);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(0x00291034);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("UN", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StudyPriorityID);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("LOW", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.ReasonForStudy);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RequestingPhysician);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("TANG^NELSON^^^", attr.getValueAsString(null, 0));
            assertEquals("PN", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RequestingService);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("101", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StudyArrivalDate);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20100408", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StudyArrivalTime);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("085927.000", attr.getValueAsString(null, 0));
            assertEquals("TM", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StudyCompletionDate);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("20100625", attr.getValueAsString(null, 0));
            assertEquals("DA", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StudyCompletionTime);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("113048.000", attr.getValueAsString(null, 0));
            assertEquals("TM", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RequestedProcedureDescription);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("CHEST PA AND LATERAL", attr.getValueAsString(null, 0));
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.RequestedProcedureCodeSequence);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("SQ", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.StudyComments);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("LT", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.CurrentPatientLocation);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("LO", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PresentationLUTShape);
        if (isNormalized) {
            // this attribute is moved to study/series level during
            // normalization
            assertNull(attr);
        } else {
            assertEquals("IDENTITY", attr.getValueAsString(null, 0));
            assertEquals("CS", attr.vr().toString());
        }

        attr = instance.getAttribute(Tag.PixelData);
        // this attribute is included in the data model
        assertNotNull(attr);
    }
}
