/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test the behavior of the Study object when facing modification
 * 
 * @author Jiefeng Zhai
 */
public class StudyTest {

    private Study studyNormalized = null;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    @Before
    public void setUp() throws IOException {
        final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14401";
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);
        final String STUDY_DATE_VALUE = "20120531";
        DicomElement studyDate = new SimpleDicomElement(Tag.StudyDate, VR.DA, true,
                STUDY_DATE_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);
        obj.add(studyDate);

        studyNormalized = new Study(store, new DicomUID(STUDY_UID_VALUE), true, obj);
    }

    @After
    public void tearDown() {
        studyNormalized = null;
    }

    /**
     * Tests putting attributes into a study. Should throw an exception if an
     * attribute with the same tag already exists in the study.
     */
    @Test
    public void testAddDuplicateAttributes() {
        final int TEST_TAG = Tag.StudyDate;
        final String TEST_VALUE = "20120531";
        final String TEST_NEW_VALUE = "20061219";

        DicomElement duplicateAttribute = new SimpleDicomElement(TEST_TAG, VR.DA, true,
                TEST_NEW_VALUE.getBytes(), null);

        try {
            studyNormalized.putAttribute(duplicateAttribute);
            fail("Missing expected exception");
        } catch (IllegalArgumentException e) {
            DicomElement newAttr = studyNormalized.getAttribute(TEST_TAG);
            assertEquals(TEST_VALUE, new String(newAttr.getBytes()));
        }
    }

    /**
     * Add some new attributes and the number of attributes should remain
     * consistent
     */
    @Test
    public void testAddAttributes() {
        final String VALUE01 = "St. Nowhere Hospital";
        DicomElement attr01 = new SimpleDicomElement(Tag.InstitutionName, VR.LO, true,
                VALUE01.getBytes(), null);
        final String VALUE02 = "Thomas^Albert";
        DicomElement attr02 = new SimpleDicomElement(Tag.ReferringPhysicianName, VR.PN, true,
                VALUE02.getBytes(), null);

        assertEquals(studyNormalized.elementCount(), 4);

        studyNormalized.putAttribute(attr01);
        studyNormalized.putAttribute(attr02);

        assertEquals(studyNormalized.elementCount(), 6);
    }

    /**
     * Delete some attributes and the number of attributes should remain
     * consistent
     */
    @Test
    public void testDeleteAttributes() {
        assertEquals(studyNormalized.elementCount(), 4);

        studyNormalized.removeAttribute(Tag.StudyDate);

        assertEquals(studyNormalized.elementCount(), 3);
    }
}
