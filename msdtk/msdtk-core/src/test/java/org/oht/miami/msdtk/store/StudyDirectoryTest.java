/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.junit.Before;
import org.junit.Test;

/**
 * Tests for the StudyDirectory object.
 * 
 * @author Josh Stephens (joshua.stephens@harris.com)
 */
public class StudyDirectoryTest {

    private final String validUID = "1.2.250.1.59.453.859.781157385.1640.1290623935.3.3.816";
    private StoreFS store;

    @Before
    public void setUp() {
        store = (StoreFS) StoreFactory.createStore("testStoreFS.properties");
    }

    /**
     * Tests that lookup() behaves correctly when looking up a study which
     * exists in the study directory.
     */
    @Test
    public void testLookupStudyExists() {

        /* Create the study entry and study directory objects */
        DicomUID duid = new DicomUID(validUID);
        Study sv = new Study(store, duid, true);
        // StudyInfo si = null;
        StudyEntryFS se = new StudyEntryFS(store, sv);
        StudyDirectory sd = new StudyDirectory();
        sd.insert(se);

        /* Retrieve the study entry from the study directory */
        StudyEntry retrievedStudy = sd.lookup(duid);

        assertEquals(se, retrievedStudy);
    }

    /**
     * Tests that lookup() behaves correctly when looking up a study which does
     * not exist in the study directory.
     */
    @Test
    public void testLookupStudyDne() {

        /* Creates an empty study directory */
        DicomUID duid = new DicomUID(validUID);
        StudyDirectory sd = new StudyDirectory();
        StudyEntry retrievedStudy = sd.lookup(duid);
        assertNull(retrievedStudy);
    }

    /**
     * Tests that remove() behaves correctly when looking up a study which
     * exists in the study directory.
     */
    @Test
    public void testRemoveStudyExists() {
        /* create the study entry and study directory */
        DicomUID duid = new DicomUID(validUID);
        Study sv = new Study(store, duid, true);
        // StudyInfo si = null;
        StudyEntryFS se = new StudyEntryFS(store, sv);
        StudyDirectory sd = new StudyDirectory();

        /* Insert the study entry */
        sd.insert(se);

        /* delete the study entry */
        sd.delete(se);

        /* Attempt to retrieve the deleted study */
        StudyEntry retrievedStudy = sd.lookup(duid);

        assertNull(retrievedStudy);
    }

    /**
     * Tests that lookup() behaves correctly when looking up a study which does
     * not exist in the study directory.
     */
    @Test
    public void testRemoveStudyDne() {
        /* create the study entry and study directory */
        DicomUID duid = new DicomUID(validUID);
        Study sv = new Study(store, duid, true);
        // StudyInfo si = null;
        StudyEntryFS se = new StudyEntryFS(store, sv);
        StudyDirectory sd = new StudyDirectory();

        /* delete the study entry */
        try {
            sd.delete(se);
            fail();
        } catch (IllegalStateException e) {
            assertEquals("The StudyEntry was not found in the Study Directory", e.getMessage());
        }

    }

    /**
     * Tests that remove() behaves correctly when looking up a study which
     * exists in the study directory.
     */
    @Test
    public void testRemoveStudyByUIDExists() {
        /* create the study entry and study directory */
        DicomUID duid = new DicomUID(validUID);
        Study sv = new Study(store, duid, true);
        // StudyInfo si = null;
        StudyEntryFS se = new StudyEntryFS(store, sv);
        StudyDirectory sd = new StudyDirectory();

        /* Insert the study entry */
        sd.insert(se);

        /* delete the study entry */
        sd.delete(se.getStudyInstanceUID());

        /* Attempt to retrieve the deleted study */
        StudyEntry retrievedStudy = sd.lookup(duid);

        assertNull(retrievedStudy);
    }

    /**
     * Tests that lookup() behaves correctly when looking up a study which does
     * not exist in the study directory.
     */
    @Test
    public void testRemoveStudyByUIDDne() {
        /* create the study entry and study directory */
        // DicomUID duid = new DicomUID(validUID);
        Study sv = new Study(store, new DicomUID(), true);
        // StudyInfo si = null;
        StudyEntryFS se = new StudyEntryFS(store, sv);
        StudyDirectory sd = new StudyDirectory();

        /* delete the study entry */
        try {
            sd.delete(se.getStudyInstanceUID());
            fail();
        } catch (IllegalStateException e) {
            assertEquals("The StudyEntry was not found in the Study Directory", e.getMessage());
        }

    }

}
