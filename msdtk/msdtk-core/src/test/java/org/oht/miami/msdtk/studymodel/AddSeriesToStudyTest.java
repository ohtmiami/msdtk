/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test the behavior of the Study object when interacting with series
 * 
 * @author Jiefeng Zhai
 */
public class AddSeriesToStudyTest {

    private Study studyNormalized = null;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    @Before
    public void setUp() throws IOException {
        final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14401";
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);

        studyNormalized = new Study(store, new DicomUID(STUDY_UID_VALUE), true, obj);
    }

    @After
    public void tearDown() {
        studyNormalized = null;
    }

    /**
     * Add another instance to the series Verify number of instances per series
     */
    @Test
    public void testAddInstance() {
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";
        Series srs = studyNormalized.getSeries(SERIES_UID_VALUE);
        Instance inst = new Instance(srs);

        assertEquals(srs.getInstanceCount(), 1);

        srs.addInstance(inst);

        assertEquals(srs.getInstanceCount(), 2);
    }

    /**
     * Delete series from a study Verify number of series
     */
    @Test
    public void testDeleteSeries() {
        final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";

        assertEquals(studyNormalized.seriesCount(), 1);

        studyNormalized.removeSeries(SERIES_UID_VALUE);

        assertEquals(studyNormalized.seriesCount(), 0);
    }
}
