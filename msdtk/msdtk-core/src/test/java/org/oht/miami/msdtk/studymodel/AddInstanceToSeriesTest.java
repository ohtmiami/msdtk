/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Test 1-pass normalizing functionalities when adding instance and series using
 * dicom object
 * 
 * @author Jiefeng Zhai
 */
public class AddInstanceToSeriesTest {

    private final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";

    private final String SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";

    private final String SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1166562673.14401";

    private final String NEW_STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.144444673.14401";

    private final String NEW_SERIES_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.14444473.14401";

    private final String NEW_SOP_UID_VALUE = "1.3.6.1.4.1.5962.1.1.10.2.1.1123223373.14401";

    private final String STUDY_DATE_VALUE = "20120531";

    private Study studyNormalized = null;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    @Before
    public void setUp() throws IOException {
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);
        DicomElement studyDate = new SimpleDicomElement(Tag.StudyDate, VR.DA, true,
                STUDY_DATE_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);
        obj.add(studyDate);

        studyNormalized = new Study(store, new DicomUID(STUDY_UID_VALUE), true, obj);
    }

    @After
    public void tearDown() {
        studyNormalized = null;
    }

    /**
     * Add instance from another study into current study An exception should be
     * thrown
     */
    @Test(expected = RuntimeException.class)
    public void testAddInvalidInstance() {
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                NEW_STUDY_UID_VALUE.getBytes(), null);
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                NEW_SERIES_UID_VALUE.getBytes(), null);
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                NEW_SOP_UID_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);

        studyNormalized.addDicomObject(obj);
    }

    /**
     * Add an instance belonging to a series in the study. And verify the number
     * of instances in the study
     */
    @Test
    public void testAddValidInstance() {
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                NEW_SOP_UID_VALUE.getBytes(), null);
        DicomElement studyDate = new SimpleDicomElement(Tag.StudyDate, VR.DA, true,
                STUDY_DATE_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);
        obj.add(studyDate);

        assertEquals(studyNormalized.getSeries(SERIES_UID_VALUE).getInstanceCount(), 1);

        try {
            studyNormalized.addDicomObject(obj);
        } catch (RuntimeException e) {
            fail("unexpected exception");
        }

        assertEquals(studyNormalized.getSeries(SERIES_UID_VALUE).getInstanceCount(), 2);
    }

    /**
     * Add an instance with an instance UID that exists in the study (should
     * throw exception)
     */
    @Test(expected = IllegalArgumentException.class)
    public void testAddDuplicateInstance() {
        DicomElement studyUID = new SimpleDicomElement(Tag.StudyInstanceUID, VR.UI, true,
                STUDY_UID_VALUE.getBytes(), null);
        DicomElement seriesUID = new SimpleDicomElement(Tag.SeriesInstanceUID, VR.UI, true,
                SERIES_UID_VALUE.getBytes(), null);
        DicomElement sopUID = new SimpleDicomElement(Tag.SOPInstanceUID, VR.UI, true,
                SOP_UID_VALUE.getBytes(), null);

        DicomObject obj = new BasicDicomObject();
        obj.add(sopUID);
        obj.add(studyUID);
        obj.add(seriesUID);

        studyNormalized.addDicomObject(obj);
    }

    /**
     * Delete instance and verify the number of Instances.
     */
    @Test
    public void testDeleteInstance() {
        assertEquals(studyNormalized.getSeries(SERIES_UID_VALUE).getInstanceCount(), 1);

        Series srs = studyNormalized.getSeries(SERIES_UID_VALUE);
        Instance inst = studyNormalized.getInstance(SOP_UID_VALUE);
        srs.removeInstance(inst);

        assertEquals(studyNormalized.getSeries(SERIES_UID_VALUE).getInstanceCount(), 0);
    }
}
