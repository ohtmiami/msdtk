/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.io;

import static org.junit.Assert.assertEquals;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Tests DicomStudyReader for reading various kinds of studies.
 * 
 * @author Raphael Yu Ning
 */
public class StudyReaderTest {

    private Store store;

    @Before
    public void setUp() {

        store = StoreFactory.createStore("testStoreFS.properties");
    }

    @After
    public void tearDown() throws IOException {

        TestUtils.cleanStore(store);
    }

    @Test
    public void testRead20phase() throws IOException {

        Study study = TestUtils.readStudyFromResource(store, "20phase");

        assertEquals("1.2.392.200036.9116.2.2.2.1762893313.1029997326.945873",
                study.getStudyInstanceUIDAsString());
        assertEquals("20 phase", study.getPatientName());
        assertEquals(2, study.seriesCount());
    }

    @Test
    public void testReadMINT10() throws IOException {

        Study study = TestUtils.readStudyFromResource(store, "MINT10");

        assertEquals("2.16.840.1.114255.393386351.1568457295.34445.4",
                study.getStudyInstanceUIDAsString());
        assertEquals("MINT10^MINT10", study.getPatientName());
        assertEquals(2, study.seriesCount());
    }

    @Test
    public void testReadMultipleStudies() throws IOException {

        List<Study> studies = TestUtils.readStudiesFromResource(store, "MultipleStudies");

        assertEquals(5, studies.size());
    }

    // TODO Add tests for reading MFD and MSD studies
}
