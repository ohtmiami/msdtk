/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.studymodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.util.DicomUID;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.SimpleDicomElement;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.VR;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;

/**
 * Tests the one pass algorithm
 * 
 * @author Katie Calabro kcalabr1@jhu.edu
 */
public class StudyNormalizationTest {

    private Study testStudy;

    private Store store = StoreFactory.createStore("testStoreFS.properties");

    /**
     * Sets up the new study
     */
    @Before
    public void setUp() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "3");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "20");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        testStudy = new Study(store, new DicomUID("1"), true, studyTestDcmObj);
    }

    @After
    public void tearDown() {
        testStudy = null;
    }

    /**
     * Creates a Dicom Element
     * 
     * @param testTag
     *            The int value of the tag
     * @param id
     *            The VR id
     * @param testValue
     *            The value that will be in the dicom element
     * @return The Dicom element created
     */
    public DicomElement createDicomElement(int testTag, VR id, String testValue) {
        DicomElement testElement = new SimpleDicomElement(testTag, id, true, testValue.getBytes(),
                null);
        return testElement;
    }

    /**
     * Iterated though the ArrayList of attributes and checks that the previous
     * tag is less than the current tag
     * 
     * @param iter
     *            The Array List of attributes
     */
    public boolean checkAttributeListOrder(Iterator<DicomElement> iter) {

        int perviousTag = iter.next().tag();
        int currentTag = 0;

        if (iter != null) {
            while (iter.hasNext()) {
                currentTag = iter.next().tag();
                if (perviousTag >= currentTag)
                    return false;
                perviousTag = currentTag;
            }
        }
        return true;
    }

    /**
     * This test creates a study and adds an instance to it. It verifies that
     * the attributes are added to the model correctly.
     */
    @Test
    public void testStudyLevelAttrInsertion() {
        assertEquals(testStudy.elementCount(), 4);
    }

    /**
     * Tests that the of dicom elements in the study changes from zero to number
     * of dicom elements in the first dicom object inserted.
     */
    @Test
    public void testRepeatedStudyLevelAttrInsertion() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "20");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        testStudy.addDicomObject(studyTestDcmObj);

        assertEquals(testStudy.elementCount(), 5);

    }

    /**
     * Creates a study and adds an instance to it.Then adds another instance in
     * the same series with an attribute that is stored in the study level
     * attribute but with a different value. It verifies that the attribute has
     * moved from the study level attributes down to the instance level.
     */
    @Test
    public void testRepeatedStudyLevelAttrInsertionWithDiffVal() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "21");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);

        testStudy.addDicomObject(studyTestDcmObj);

        /* Sets the two instances in the study */
        Instance testInstance = testStudy.getInstance("3");
        Instance testInstance2 = testStudy.getInstance("4");

        /*
         * Verifies the attributes is no longer in the study or series level
         * attributes.
         */
        assertEquals(testInstance.getValueForAttributeAsString(Tag.FileSetID), "20");
        assertEquals(testInstance2.getValueForAttributeAsString(Tag.FileSetID), "21");

    }

    /**
     * Compares number of data elements in the normalized study model versus
     * number of data elements in the study without normalization.
     */
    @Test
    public void testNumElementsInNormalizedModelVsNonNormalizedModel() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "3");
        DicomElement attribute4 = createDicomElement(Tag.CodingSchemeUID, VR.UI, "55");
        DicomElement attribute5 = createDicomElement(Tag.FileSetID, VR.LO, "20");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);

        Study nonNormalizedStudy = new Study(store, new DicomUID("1"), false, studyTestDcmObj);

        DicomElement attribute6 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");

        DicomObject studyTestDcmObj2 = new BasicDicomObject();
        studyTestDcmObj2.add(attribute1);
        studyTestDcmObj2.add(attribute2);
        studyTestDcmObj2.add(attribute6);
        studyTestDcmObj2.add(attribute4);
        studyTestDcmObj2.add(attribute5);

        nonNormalizedStudy.addDicomObject(studyTestDcmObj2);

        DicomElement attribute9 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "5");

        DicomObject studyTestDcmObj3 = new BasicDicomObject();
        studyTestDcmObj3.add(attribute1);
        studyTestDcmObj3.add(attribute2);
        studyTestDcmObj3.add(attribute9);
        studyTestDcmObj3.add(attribute4);
        studyTestDcmObj3.add(attribute5);

        nonNormalizedStudy.addDicomObject(studyTestDcmObj3);

        testStudy.addDicomObject(studyTestDcmObj2);
        testStudy.addDicomObject(studyTestDcmObj3);

        assertTrue(nonNormalizedStudy.elementCount() > testStudy.elementCount());

    }

    /**
     * Creates a study and add an instance to it. It then adds another series in
     * order to move an attribute from the study level down to the series level.
     * Then it adds another instance in the first series with an attribute that
     * exists in the previously added instance and is stored in the series level
     * attribute but has a different value. Then it verifies that the attribute
     * has moved from the series level attributes down to the instance level.
     */
    @Test
    public void testRepeatedSeriesLevelAttrInsertionWithDiffVal() {

        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "3");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "21");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);

        testStudy.addDicomObject(studyTestDcmObj);

        /* Add attribute at Series level */
        DicomElement attribute5 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute6 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute7 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "5");
        DicomElement attribute8 = createDicomElement(Tag.FileSetID, VR.LO, "22");

        DicomObject studyTestDcmObj2 = new BasicDicomObject();
        studyTestDcmObj2.add(attribute5);
        studyTestDcmObj2.add(attribute6);
        studyTestDcmObj2.add(attribute7);
        studyTestDcmObj2.add(attribute8);

        /* Add a new instance to the series */
        testStudy.addDicomObject(studyTestDcmObj2);

        Instance testInstance = testStudy.getInstance("3");
        Instance testInstance2 = testStudy.getInstance("5");

        Series testSeries = testStudy.getSeries("2");

        /*
         * Checks that the attribute has moved from the Series level to the
         * Instance level
         */
        assertEquals(testInstance.getValueForAttributeAsString(Tag.FileSetID), "20");
        assertEquals(testInstance2.getValueForAttributeAsString(Tag.FileSetID), "22");
        assertEquals(testSeries.getValueForAttributeAsString(Tag.PatientAge), null);
    }

    /**
     * Creates a study and adds an instance to it. It then adds another series
     * in order to move an attribute from the study level down to the series
     * level. Then it adds another instance in the first series with an
     * attribute that does exist in the previously added instance and is stored
     * in the series level attribute with the same value. Then it verifies that
     * no change has happened to the model because the attribute is already in
     * the series level attributes with the same value
     */
    @Test
    public void testRepeatedSeriesLevelAttrInsertionWithSameVal() {
        Series testSeries = testStudy.getSeries("2");

        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "3");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "21");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);

        testStudy.addDicomObject(studyTestDcmObj);

        /* Adds attribute at Series level */
        DicomElement attribute5 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute6 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute7 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "5");
        DicomElement attribute8 = createDicomElement(Tag.FileSetID, VR.LO, "20");

        DicomObject studyTestDcmObj2 = new BasicDicomObject();
        studyTestDcmObj2.add(attribute5);
        studyTestDcmObj2.add(attribute6);
        studyTestDcmObj2.add(attribute7);
        studyTestDcmObj2.add(attribute8);

        /* Adds a new instance to the series */
        testStudy.addDicomObject(studyTestDcmObj2);

        Instance testInstance = testStudy.getInstance("4");
        Instance testInstance2 = testStudy.getInstance("5");
        Series testSeries2 = testStudy.getSeries("3");

        /*
         * Checks that the attribute has stayed at the Series level and is not
         * at the Instance level
         */
        assertEquals(testSeries.getValueForAttributeAsString(Tag.FileSetID), "20");
        assertEquals(testSeries2.getValueForAttributeAsString(Tag.FileSetID), "21");
        assertEquals(testInstance.getValueForAttributeAsString(Tag.FileSetID), null);
        assertEquals(testInstance2.getValueForAttributeAsString(Tag.FileSetID), null);
    }

    /**
     * Creates a study and adds an instance to it. It then adds another series
     * in order to move an attribute from the study level down to the series
     * level. Then adds another instance in a new series with an attribute that
     * does exist in the previously added instance and is stored in the study
     * level attribute but with a different value. Then it verifies that the
     * attribute has moved from the study level attribute down to the series
     * level attribute.
     */
    @Test
    public void testMoveStudyLvlAttrToSeriesLvlAttr() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "4");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "5");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "21");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);

        /* Verify that the patient age attribute is at the study level */
        assertEquals(testStudy.getValueForAttributeAsString(Tag.FileSetID), "20");

        /* Adds a new instance to a new series */
        testStudy.addDicomObject(studyTestDcmObj);

        Series testSeries1 = testStudy.getSeries("2");
        Series testSeries2 = testStudy.getSeries("4");

        /*
         * Tests that the attribute has moved down to the series levels and that
         * it is not in the study level attributes or the instance level as well
         */
        assertEquals(testSeries1.getValueForAttributeAsString(Tag.FileSetID), "20");
        assertEquals(testSeries2.getValueForAttributeAsString(Tag.FileSetID), "21");
        assertEquals(testStudy.getValueForAttributeAsString(Tag.FileSetID), null);

    }

    /**
     * Creates a study and adds an instance to it. Then it adds another instance
     * in the same series with an attribute that does not exist in the
     * previously added instance. Then it verifies the attribute is added to the
     * instance level attributes of the newly added instance.
     */
    @Test
    public void testAddNewAttrToInstanceListOfAttr() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "20");
        DicomElement attribute5 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);

        testStudy.addDicomObject(studyTestDcmObj);

        Instance testInstance = testStudy.getInstance("4");

        assertEquals(testInstance.getValueForAttributeAsString(Tag.Location), "Johns Hopkins");
    }

    /**
     * Creates a study and adds an instance to it. Then it adds another instance
     * in a new series with an attribute that does not exist in the previously
     * added instance. Then it verifies the attribute is added to the series
     * level attributes of the newly added instance.
     */
    @Test
    public void testAddNewAttrToSeriesListOAttr() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "3");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "4");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "20");
        DicomElement attribute5 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);

        testStudy.addDicomObject(studyTestDcmObj);

        Series testSeries = testStudy.getSeries("3");
        assertEquals(testSeries.getValueForAttributeAsString(Tag.Location), "Johns Hopkins");
    }

    /**
     * Creates a series and add a study level attribute to it. This should throw
     * exception.
     */
    @Test
    public void testAddStudyLvlAttrToSeries() {
        Series testSeries = testStudy.getSeries("2");

        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "11");

        /* Checks to make sure that the tag is a correct Series tag */
        try {
            testSeries.putAttribute(attribute1);
            fail("The tag is a Series Tag");
        } catch (IllegalArgumentException e) {
            assertEquals(testSeries.getAttribute(Tag.StudyInstanceUID), null);
        }
    }

    /**
     * Creates a instance and add a study level attribute to it. This should
     * throw exception.
     */
    @Test
    public void testAddStudyLvlAttrToInstance() {
        Instance testInstance = testStudy.getInstance("3");

        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "11");

        /* Checks to make sure that the tag is a correct Instance tag */
        try {
            testInstance.putAttribute(attribute1);
            fail("The tag is an Instance Tag");
        } catch (IllegalArgumentException e) {
            assertEquals(testInstance.getAttribute(Tag.StudyInstanceUID), null);
        }
    }

    /**
     * Verifies input Dicom object has sorted DICOM attributes. It creates a
     * DICOM object and adds Data elements to it in different tag order. It
     * Verifies that the Data elements within the dcm4che DICOM object are in
     * order.
     */
    @Test
    public void testDataElementsAreInOrderWRTTags() {
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "3");
        DicomElement attribute4 = createDicomElement(Tag.FileSetID, VR.LO, "20");
        DicomElement attribute5 = createDicomElement(Tag.CurveTime, VR.LO, "8");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);

        int hexValue1 = 0;
        int currentTag = 0;

        Iterator<DicomElement> iter = studyTestDcmObj.iterator();
        hexValue1 = iter.next().tag();

        /*
         * The while loop will iterate through the array list and compare each
         * element to the next element in the array list
         */
        while (iter.hasNext()) {
            currentTag = iter.next().tag();
            assertTrue(hexValue1 < currentTag);
            hexValue1 = currentTag;
        }

    }

    /**
     * Verifies the attributes are in order within the list of study, series and
     * instance level attributes. It creates a study. Then it creates DICOM
     * objects and adds Data elements to it in different tag order. Then it adds
     * the DICOM objects to the study. It verifies data elements within study,
     * series, and instances list of attributes are in order.
     */
    @Test
    public void testAttributesAreInOrderInStudySeriesAndInstanceLevel() {
        /* Study UID=1; Series UID=1; Instance UID=1; */
        DicomElement attribute1 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute2 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "1");
        DicomElement attribute3 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "1");
        DicomElement attribute4 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute5 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");
        DicomElement attribute6 = createDicomElement(Tag.PatientSex, VR.UI, "Male");
        DicomElement attribute7 = createDicomElement(Tag.PatientBirthDate, VR.UI, "1/22/1964");
        DicomElement attribute8 = createDicomElement(Tag.SeriesTime, VR.UI, "12:00");
        DicomElement attribute9 = createDicomElement(Tag.SeriesDate, VR.UI, "1/1/2000");
        DicomElement attribute10 = createDicomElement(Tag.OverlayTime, VR.LO, "3");
        DicomElement attribute11 = createDicomElement(Tag.InstanceCreationDate, VR.LO, "1/2/2001");
        DicomElement attribute12 = createDicomElement(Tag.InstanceCreationTime, VR.UI, "2:00");
        DicomElement attribute13 = createDicomElement(Tag.Manufacturer, VR.UI, "Company1");
        DicomElement attribute14 = createDicomElement(Tag.StationName, VR.UI, "Name1");
        DicomElement attribute15 = createDicomElement(Tag.TimeRange, VR.LO, "1001");

        DicomObject studyTestDcmObj = new BasicDicomObject();
        studyTestDcmObj.add(attribute1);
        studyTestDcmObj.add(attribute2);
        studyTestDcmObj.add(attribute3);
        studyTestDcmObj.add(attribute4);
        studyTestDcmObj.add(attribute5);
        studyTestDcmObj.add(attribute6);
        studyTestDcmObj.add(attribute7);
        studyTestDcmObj.add(attribute8);
        studyTestDcmObj.add(attribute9);
        studyTestDcmObj.add(attribute10);
        studyTestDcmObj.add(attribute11);
        studyTestDcmObj.add(attribute12);
        studyTestDcmObj.add(attribute13);
        studyTestDcmObj.add(attribute14);
        studyTestDcmObj.add(attribute15);

        Study testStudy2 = new Study(store, new DicomUID("1"), true, studyTestDcmObj);

        /* Study UID=1; Series UID=1; Instance UID=2; */
        DicomElement attribute16 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute17 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "1");
        DicomElement attribute18 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "2");
        DicomElement attribute19 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute20 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");
        DicomElement attribute21 = createDicomElement(Tag.PatientSex, VR.UI, "Male");
        DicomElement attribute22 = createDicomElement(Tag.PatientBirthDate, VR.UI, "1/22/1964");
        DicomElement attribute23 = createDicomElement(Tag.SeriesTime, VR.UI, "12:00");
        DicomElement attribute24 = createDicomElement(Tag.SeriesDate, VR.UI, "1/1/2000");
        DicomElement attribute25 = createDicomElement(Tag.OverlayTime, VR.LO, "3");
        DicomElement attribute26 = createDicomElement(Tag.InstanceCreationDate, VR.LO, "5/19/2001");
        DicomElement attribute27 = createDicomElement(Tag.InstanceCreationTime, VR.UI, "10:00");
        DicomElement attribute28 = createDicomElement(Tag.Manufacturer, VR.UI, "Company2");
        DicomElement attribute29 = createDicomElement(Tag.StationName, VR.UI, "Name2");
        DicomElement attribute30 = createDicomElement(Tag.TimeRange, VR.LO, "1002");

        DicomObject studyTestDcmObj2 = new BasicDicomObject();
        studyTestDcmObj2.add(attribute16);
        studyTestDcmObj2.add(attribute17);
        studyTestDcmObj2.add(attribute18);
        studyTestDcmObj2.add(attribute19);
        studyTestDcmObj2.add(attribute20);
        studyTestDcmObj2.add(attribute21);
        studyTestDcmObj2.add(attribute22);
        studyTestDcmObj2.add(attribute23);
        studyTestDcmObj2.add(attribute24);
        studyTestDcmObj2.add(attribute25);
        studyTestDcmObj2.add(attribute26);
        studyTestDcmObj2.add(attribute27);
        studyTestDcmObj2.add(attribute28);
        studyTestDcmObj2.add(attribute29);
        studyTestDcmObj2.add(attribute30);

        testStudy2.addDicomObject(studyTestDcmObj2);

        /* Study UID=1; Series UID=2; Instance UID=1; */
        DicomElement attribute31 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute32 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute33 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "1");
        DicomElement attribute34 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute35 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");
        DicomElement attribute36 = createDicomElement(Tag.PatientSex, VR.UI, "Male");
        DicomElement attribute37 = createDicomElement(Tag.PatientBirthDate, VR.UI, "1/22/1964");
        DicomElement attribute38 = createDicomElement(Tag.SeriesTime, VR.UI, "5:00");
        DicomElement attribute39 = createDicomElement(Tag.SeriesDate, VR.UI, "5/8/2001");
        DicomElement attribute40 = createDicomElement(Tag.OverlayTime, VR.LO, "10");
        DicomElement attribute41 = createDicomElement(Tag.InstanceCreationDate, VR.LO, "5/21/2001");
        DicomElement attribute42 = createDicomElement(Tag.InstanceCreationTime, VR.UI, "11:00");
        DicomElement attribute43 = createDicomElement(Tag.Manufacturer, VR.UI, "Company3");
        DicomElement attribute44 = createDicomElement(Tag.StationName, VR.UI, "Name3");
        DicomElement attribute45 = createDicomElement(Tag.TimeRange, VR.LO, "1003");

        DicomObject studyTestDcmObj3 = new BasicDicomObject();
        studyTestDcmObj3.add(attribute31);
        studyTestDcmObj3.add(attribute32);
        studyTestDcmObj3.add(attribute33);
        studyTestDcmObj3.add(attribute34);
        studyTestDcmObj3.add(attribute35);
        studyTestDcmObj3.add(attribute36);
        studyTestDcmObj3.add(attribute37);
        studyTestDcmObj3.add(attribute38);
        studyTestDcmObj3.add(attribute39);
        studyTestDcmObj3.add(attribute40);
        studyTestDcmObj3.add(attribute41);
        studyTestDcmObj3.add(attribute42);
        studyTestDcmObj3.add(attribute43);
        studyTestDcmObj3.add(attribute44);
        studyTestDcmObj3.add(attribute45);

        testStudy2.addDicomObject(studyTestDcmObj3);

        /* Study UID=1; Series UID=2; Instance UID=2; */
        DicomElement attribute46 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute47 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "2");
        DicomElement attribute48 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "2");
        DicomElement attribute49 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute50 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");
        DicomElement attribute51 = createDicomElement(Tag.PatientSex, VR.UI, "Male");
        DicomElement attribute52 = createDicomElement(Tag.PatientBirthDate, VR.UI, "1/22/1964");
        DicomElement attribute53 = createDicomElement(Tag.SeriesTime, VR.UI, "5:00");
        DicomElement attribute54 = createDicomElement(Tag.SeriesDate, VR.UI, "5/8/2001");
        DicomElement attribute55 = createDicomElement(Tag.OverlayTime, VR.LO, "10");
        DicomElement attribute56 = createDicomElement(Tag.InstanceCreationDate, VR.LO, "5/23/2001");
        DicomElement attribute57 = createDicomElement(Tag.InstanceCreationTime, VR.UI, "4:00");
        DicomElement attribute58 = createDicomElement(Tag.Manufacturer, VR.UI, "Company4");
        DicomElement attribute59 = createDicomElement(Tag.StationName, VR.UI, "Name4");
        DicomElement attribute60 = createDicomElement(Tag.TimeRange, VR.LO, "1004");

        DicomObject studyTestDcmObj4 = new BasicDicomObject();
        studyTestDcmObj4.add(attribute46);
        studyTestDcmObj4.add(attribute47);
        studyTestDcmObj4.add(attribute48);
        studyTestDcmObj4.add(attribute49);
        studyTestDcmObj4.add(attribute50);
        studyTestDcmObj4.add(attribute51);
        studyTestDcmObj4.add(attribute52);
        studyTestDcmObj4.add(attribute53);
        studyTestDcmObj4.add(attribute54);
        studyTestDcmObj4.add(attribute55);
        studyTestDcmObj4.add(attribute56);
        studyTestDcmObj4.add(attribute57);
        studyTestDcmObj4.add(attribute58);
        studyTestDcmObj4.add(attribute59);
        studyTestDcmObj4.add(attribute60);

        testStudy2.addDicomObject(studyTestDcmObj4);

        /* Study UID=1; Series UID=3; Instance UID=1; */
        DicomElement attribute61 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute62 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "3");
        DicomElement attribute63 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "1");
        DicomElement attribute64 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute65 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");
        DicomElement attribute66 = createDicomElement(Tag.PatientSex, VR.UI, "Male");
        DicomElement attribute67 = createDicomElement(Tag.PatientBirthDate, VR.UI, "1/22/1964");
        DicomElement attribute68 = createDicomElement(Tag.SeriesTime, VR.UI, "7:00");
        DicomElement attribute69 = createDicomElement(Tag.SeriesDate, VR.UI, "11/9/2001");
        DicomElement attribute70 = createDicomElement(Tag.OverlayTime, VR.LO, "8");
        DicomElement attribute71 = createDicomElement(Tag.InstanceCreationDate, VR.LO, "11/15/2001");
        DicomElement attribute72 = createDicomElement(Tag.InstanceCreationTime, VR.UI, "8:00");
        DicomElement attribute73 = createDicomElement(Tag.Manufacturer, VR.UI, "Company5");
        DicomElement attribute74 = createDicomElement(Tag.StationName, VR.UI, "Name5");
        DicomElement attribute75 = createDicomElement(Tag.TimeRange, VR.LO, "1005");

        DicomObject studyTestDcmObj5 = new BasicDicomObject();
        studyTestDcmObj5.add(attribute61);
        studyTestDcmObj5.add(attribute62);
        studyTestDcmObj5.add(attribute63);
        studyTestDcmObj5.add(attribute64);
        studyTestDcmObj5.add(attribute65);
        studyTestDcmObj5.add(attribute66);
        studyTestDcmObj5.add(attribute67);
        studyTestDcmObj5.add(attribute68);
        studyTestDcmObj5.add(attribute69);
        studyTestDcmObj5.add(attribute70);
        studyTestDcmObj5.add(attribute71);
        studyTestDcmObj5.add(attribute72);
        studyTestDcmObj5.add(attribute73);
        studyTestDcmObj5.add(attribute74);
        studyTestDcmObj5.add(attribute75);

        testStudy2.addDicomObject(studyTestDcmObj5);

        /* Study UID=1; Series UID=3; Instance UID=2; */
        DicomElement attribute76 = createDicomElement(Tag.StudyInstanceUID, VR.UI, "1");
        DicomElement attribute77 = createDicomElement(Tag.SeriesInstanceUID, VR.UI, "3");
        DicomElement attribute78 = createDicomElement(Tag.SOPInstanceUID, VR.UI, "2");
        DicomElement attribute79 = createDicomElement(Tag.PatientName, VR.LO, "Joe Smith");
        DicomElement attribute80 = createDicomElement(Tag.Location, VR.LO, "Johns Hopkins");
        DicomElement attribute81 = createDicomElement(Tag.PatientSex, VR.UI, "Male");
        DicomElement attribute82 = createDicomElement(Tag.PatientBirthDate, VR.UI, "1/22/1964");
        DicomElement attribute83 = createDicomElement(Tag.SeriesTime, VR.UI, "7:00");
        DicomElement attribute84 = createDicomElement(Tag.SeriesDate, VR.UI, "11/9/2001");
        DicomElement attribute85 = createDicomElement(Tag.OverlayTime, VR.LO, "8");
        DicomElement attribute86 = createDicomElement(Tag.InstanceCreationDate, VR.LO, "11/20/2001");
        DicomElement attribute87 = createDicomElement(Tag.InstanceCreationTime, VR.UI, "9:00");
        DicomElement attribute88 = createDicomElement(Tag.Manufacturer, VR.UI, "Company6");
        DicomElement attribute89 = createDicomElement(Tag.StationName, VR.UI, "Name6");
        DicomElement attribute90 = createDicomElement(Tag.TimeRange, VR.LO, "1006");

        DicomObject studyTestDcmObj6 = new BasicDicomObject();
        studyTestDcmObj6.add(attribute76);
        studyTestDcmObj6.add(attribute77);
        studyTestDcmObj6.add(attribute78);
        studyTestDcmObj6.add(attribute79);
        studyTestDcmObj6.add(attribute80);
        studyTestDcmObj6.add(attribute81);
        studyTestDcmObj6.add(attribute82);
        studyTestDcmObj6.add(attribute83);
        studyTestDcmObj6.add(attribute84);
        studyTestDcmObj6.add(attribute85);
        studyTestDcmObj6.add(attribute86);
        studyTestDcmObj6.add(attribute87);
        studyTestDcmObj6.add(attribute88);
        studyTestDcmObj6.add(attribute89);
        studyTestDcmObj6.add(attribute90);

        testStudy2.addDicomObject(studyTestDcmObj6);

        Iterator<DicomElement> iter = testStudy2.studyDicomElements.iterator();
        assertTrue(checkAttributeListOrder(iter));

        /* iterates through all the levels of the study */
        for (Iterator<Series> it = testStudy2.seriesIterator(); it.hasNext();) {
            Series tempSeries = it.next();
            for (Iterator<Instance> it2 = tempSeries.instanceIterator(); it2.hasNext();) {
                Instance tempInstance = it2.next();
                iter = tempInstance.attributeIterator();
                assertTrue(checkAttributeListOrder(iter));
            }
            iter = tempSeries.attributeIterator();
            assertTrue(checkAttributeListOrder(iter));
        }

    }
}
