/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import static org.junit.Assert.*;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class StudyEntryFSTest {

    private StoreFS store;

    private Study mintStudy;

    private StudyEntryFS studyEntryMint;

    private File mintDirectory;

    @Before
    public void setUp() throws IOException {
        store = (StoreFS) StoreFactory.createStore("testStoreFS.properties");
        mintStudy = TestUtils.readStudyFromResource(store, "MINT10");
        studyEntryMint = new StudyEntryFS(store, mintStudy);
        mintDirectory = store.getStudyFolderPath(mintStudy.getStudyInstanceUID());
    }

    @After
    public void tearDownAfterEachTest() throws IOException {
        TestUtils.cleanStore(store);
    }

    @Test
    public void testReadWriteVersionEquality() throws IOException {
        // TODO: modify test to add new versions to the study and test read
        // functionality after version history is moved to studyentry
        studyEntryMint.writeVersion(mintStudy);
        Study readStudy = studyEntryMint.readVersion();
        assertEquals(mintStudy, readStudy);
    }

    @Test
    public void testSoftDeleteFilesExist() throws IOException {
        studyEntryMint.writeVersion(mintStudy);
        File versionFile = store.getVersionFilePath(mintStudy.getStudyInstanceUID(),
                studyEntryMint.getMostRecentVersionUUID());
        studyEntryMint.deleteStudy(DeleteType.Soft);
        assertTrue(mintDirectory.exists());
        assertTrue(versionFile.exists());
    }

    @Test
    public void testHardDeleteFilesDoNotExist() throws IOException {
        studyEntryMint.writeVersion(mintStudy);
        studyEntryMint.deleteStudy(DeleteType.Hard);
        assertFalse(mintDirectory.exists());
    }

}
