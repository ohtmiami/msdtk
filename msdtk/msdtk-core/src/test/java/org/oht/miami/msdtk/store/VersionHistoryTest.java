/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.store;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.data.BasicDicomObject;
import org.dcm4che2.data.DicomElement;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.UUID;

/**
 * Tests the VersionHistory class.
 * 
 * @author Ryan Stonebraker (rstonebr@harris.com)
 */
public class VersionHistoryTest {

    private Store store;

    private Study study;

    @Before
    public void setUp() {
        store = StoreFactory.createStore("testStoreFS.properties");
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    /**
     * Tests that an empty version history's expected values are correct, and
     * that the version numbers and version info object are updated correctly
     * upon adding a version
     * 
     * @throws IOException
     */
    @Test
    public void testAddVersion() throws IOException {
        UUID uuid = UUID.randomUUID();
        study = TestUtils.readStudyFromResource(store, "MINT10");
        VersionHistory history = new VersionHistory();
        assertEquals(-1, history.getMostRecentVersionNumber());
        assertNull(history.getMostRecentVersionInfo());
        history.addVersion(study, uuid);
        assertEquals(0, history.getMostRecentVersionNumber());
        VersionInfo info = history.getMostRecentVersionInfo();
        assertEquals(uuid, info.getUUID());
        assertEquals(VersionType.New, info.getType());
        assertEquals(0, info.getVersionNumber());
        study.setVersionType(VersionType.Append);
        uuid = UUID.randomUUID();
        history.addVersion(study, uuid);
        assertEquals(1, history.getMostRecentVersionNumber());
        info = history.getMostRecentVersionInfo();
        assertEquals(uuid, info.getUUID());
        assertEquals(VersionType.Append, info.getType());
        assertEquals(1, info.getVersionNumber());
    }

    @Test
    public void testEncodeVersionHistory() throws IOException {
        UUID uuid = UUID.randomUUID();
        study = TestUtils.readStudyFromResource(store, "MINT10");
        VersionHistory history = new VersionHistory();
        history.addVersion(study, uuid);
        UUID uuid2 = UUID.randomUUID();
        study.setVersionType(VersionType.Append);
        history.addVersion(study, uuid2);
        UUID uuid3 = UUID.randomUUID();
        history.addVersion(study, uuid3);
        DicomObject object = new BasicDicomObject();
        object.add(history.encodeVersionHistory());
        DicomElement previousVersionsSequence = object.get(Tag.PreviousStudyVersionsSequence);
        int numberOfPreviousVersions = previousVersionsSequence.countItems();
        assertEquals(3, numberOfPreviousVersions);
        for (int i = numberOfPreviousVersions - 1, j = 0; i >= 0; --i, j++) {
            DicomObject previousVersionItem = previousVersionsSequence.getDicomObject(i);
            assertEquals(history.getVersionInfo(j).getUUID().toString(),
                    previousVersionItem.getString(Tag.StudyVersionUUID));
            assertEquals(history.getVersionInfo(j).getVersionNumber() + "",
                    previousVersionItem.getString(Tag.StudyVersionNumber));
            assertEquals(history.getVersionInfo(j).getType().toString(),
                    previousVersionItem.getString(Tag.StudyVersionType));
        }
    }
}
