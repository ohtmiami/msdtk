package org.oht.miami.msdtk.store;

import static org.junit.Assert.*;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;
import org.oht.miami.msdtk.util.TestUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.UUID;

public class StoreFSTest {

    private StoreFS store;

    private Study studyMINT10;

    private static final String ROOT_DIRECTORY = "target" + File.separator + "test-out"
            + File.separator + "StoreFSTest";

    private static final String OUTPUT_FILE_NAME = ROOT_DIRECTORY + File.separator + "output.SEM";

    private static final String VERSION_FILE_EXTENSION = ".dcm";

    private static final String BULK_DATA_FILE_EXTENSION = ".msdbd";

    private static final String STUDY_UID_VALUE = "1.3.6.1.4.1.5962.1.3.10.2.1166562673.14401";

    private static final UUID uuid = UUID.randomUUID();

    private static final String STORED_VERSION_NAME_PATH = ROOT_DIRECTORY + File.separator
            + STUDY_UID_VALUE + File.separator + uuid + VERSION_FILE_EXTENSION;

    private static final String STUDY_PATH = ROOT_DIRECTORY + File.separator + STUDY_UID_VALUE;

    private static final String STORED_BULK_DATA_NAME = ROOT_DIRECTORY + File.separator
            + STUDY_UID_VALUE + File.separator + uuid + BULK_DATA_FILE_EXTENSION;

    @Before
    public void setUp() throws IOException {
        store = new StoreFS(ROOT_DIRECTORY);
        studyMINT10 = TestUtils.readStudyFromResource(store, "MINT10");
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    @Test
    public void testGetStoredVersionName() {
        DicomUID dicomUID = new DicomUID(STUDY_UID_VALUE);
        File studyPath = store.getVersionFilePath(dicomUID, uuid);
        assertEquals(STORED_VERSION_NAME_PATH, studyPath.getPath());
    }

    @Test
    public void testGetStudyPath() {
        DicomUID dicomUID = new DicomUID(STUDY_UID_VALUE);
        File path = store.getStudyFolderPath(dicomUID);
        assertEquals(STUDY_PATH, path.getPath());
    }

    @Test
    public void testGetStoredBulkDataName() {
        DicomUID dicomUID = new DicomUID(STUDY_UID_VALUE);
        File path = store.getBulkDataFilePath(dicomUID, uuid);
        assertEquals(STORED_BULK_DATA_NAME, path.getPath());
    }

    @Test
    public void testSaveMSDStudy() throws IOException {

        Study study20Phase = TestUtils.readStudyFromResource(store, "20phase");
        UUID versionUUID_20phase = store.writeVersion(study20Phase);
        UUID versionUUID_MINT10 = store.writeVersion(studyMINT10);

        assertEquals(study20Phase.getBulkDataSet().getNumberOfItems(), 1);
        assertTrue(store.getBulkDataFilePath(study20Phase.getStudyInstanceUID(),
                study20Phase.getBulkDataSet().getInfo(0).getUUID()).exists());
        assertTrue(store
                .getVersionFilePath(study20Phase.getStudyInstanceUID(), versionUUID_20phase)
                .exists());

        assertEquals(studyMINT10.getBulkDataSet().getNumberOfItems(), 1);
        assertTrue(store.getBulkDataFilePath(studyMINT10.getStudyInstanceUID(),
                studyMINT10.getBulkDataSet().getInfo(0).getUUID()).exists());
        assertTrue(store.getVersionFilePath(studyMINT10.getStudyInstanceUID(), versionUUID_MINT10)
                .exists());

        assertNotNull(store.readVersion(study20Phase.getStudyInstanceUID()));
        assertNotNull(store.readVersion(studyMINT10.getStudyInstanceUID()));
    }

    /**
     * Test to check if you append to a hard deleted study if the correct
     * exception is thrown not allowing the action.
     * 
     * @throws IOException
     */
    @Test
    public void testAddInstancesToStudyWithSoftDeletedVersion() throws IOException {
        Study studyTESTMR1 = TestUtils.readStudyFromResource(store, "TESTMR_1");
        store.writeVersion(studyTESTMR1);
        store.deleteStudy(studyTESTMR1.getStudyInstanceUID(), DeleteType.Soft);
        try {
            TestUtils.readStudyFromResource(store, "TESTMR_2");
            fail("Exception not thrown after trying to read version already deleted");
        } catch (VersionPreviouslyDeletedException ex) {

        }
    }

    /**
     * Test to check if you append to a soft deleted study if the correct
     * exception is thrown not allowing the action.
     * 
     * @throws IOException
     */
    @Test
    public void testAddInstancesToStudyWithHardDeletedVersion() throws IOException {
        Study studyTESTMR1 = TestUtils.readStudyFromResource(store, "TESTMR_1");
        store.writeVersion(studyTESTMR1);
        store.deleteStudy(studyTESTMR1.getStudyInstanceUID(), DeleteType.Hard);
        try {
            TestUtils.readStudyFromResource(store, "TESTMR_2");
            fail("Exception not thrown after trying to read version already deleted");
        } catch (VersionPreviouslyDeletedException ex) {

        }
    }

    /**
     * Test to check if you append to a hard deleted study if the correct
     * exception is thrown not allowing the action.
     * 
     * @throws IOException
     */
    @Test
    public void testCantWriteNewVersionToHardDeletedStudy() throws IOException {
        store.writeVersion(studyMINT10);
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Hard);
        try {
            store.writeVersion(studyMINT10);
            fail("The store should not be able to append to a study that has been hard deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    /**
     * Test to check if you append to a soft deleted study if the correct
     * exception is thrown not allowing the action.
     * 
     * @throws IOException
     */
    @Test
    public void testCantWriteNewVersionToSoftDeletedStudy() throws IOException {
        store.writeVersion(studyMINT10);
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Soft);
        try {
            store.writeVersion(studyMINT10);
            fail("The store should not be able to append to a study that has been soft deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    @Test
    public void testSoftDeletedStudyNotReturnedFromStore() throws IOException {
        store.writeVersion(studyMINT10);
        assertNotNull(store.readVersion(studyMINT10.getStudyInstanceUID()));
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Soft);
        try {
            store.readVersion(studyMINT10.getStudyInstanceUID());
            fail("Exception not thrown after trying to read version already deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    @Test
    public void testHardDeletedStudyNotReturnedFromStore() throws IOException {
        store.writeVersion(studyMINT10);
        assertNotNull(store.readVersion(studyMINT10.getStudyInstanceUID()));
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Hard);
        try {
            store.readVersion(studyMINT10.getStudyInstanceUID());
            fail("Exception not thrown after trying to read version already deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    @Test
    public void testCanHardDeletePreviouslySoftDeletedStudy() throws IOException {
        store.writeVersion(studyMINT10);
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Soft);
        try {
            store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Hard);
        } catch (VersionPreviouslyDeletedException e) {
            fail("Exception thrown after trying to delete version already deleted");
        }
    }

    @Test
    public void testCantHardDeletePreviouslyHardDeletedStudy() throws IOException {
        store.writeVersion(studyMINT10);
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Hard);
        try {
            store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Hard);
            fail("Exception not thrown after trying to delete version already deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    @Test
    public void testCantSoftDeletePreviouslySoftDeletedStudy() throws IOException {
        store.writeVersion(studyMINT10);
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Soft);
        try {
            store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Soft);
            fail("Exception not thrown after trying to delete version already deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    @Test
    public void testCantSoftDeletePreviouslyHardDeletedStudy() throws IOException {
        store.writeVersion(studyMINT10);
        store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Hard);
        try {
            store.deleteStudy(studyMINT10.getStudyInstanceUID(), DeleteType.Soft);
            fail("Exception not thrown after trying to delete version already deleted");
        } catch (VersionPreviouslyDeletedException e) {
        }
    }

    @Test
    public void testSaveMultipleVersions() throws IOException {

        Study studyTESTMR1 = TestUtils.readStudyFromResource(store, "TESTMR_1");
        store.writeVersion(studyTESTMR1);
        VersionHistory history = store.directory.lookup(studyTESTMR1.getStudyInstanceUID()).versionHistory;
        assertEquals(VersionType.New, history.getMostRecentVersionInfo().getType());
        assertEquals(0, store.getMostRecentVersionNumber(studyTESTMR1.getStudyInstanceUID()));

        Study studyTESTMR2 = TestUtils.readStudyFromResource(store, "TESTMR_2");
        store.writeVersion(studyTESTMR2);
        assertEquals(VersionType.New, history.getVersionInfo(0).getType());
        assertEquals(0, history.getVersionInfo(0).getVersionNumber());
        assertEquals(VersionType.Append, history.getMostRecentVersionInfo().getType());
        assertEquals(1, store.getMostRecentVersionNumber(studyTESTMR1.getStudyInstanceUID()));
        assertEquals(8, studyTESTMR2.frameCount());
        assertEquals(2, studyTESTMR2.getBulkDataSet().getNumberOfItems());
        Study dummyStudy = store.readVersion(studyTESTMR1.getStudyInstanceUID(), 0);
        assertNotNull(dummyStudy);
        assertEquals(VersionType.Unchanged, dummyStudy.getVersionType());
        dummyStudy = store.readVersion(studyTESTMR1.getStudyInstanceUID(), 1);
        assertNotNull(dummyStudy);
        assertEquals(VersionType.Unchanged, dummyStudy.getVersionType());
        try {
            assertNull(store.readVersion(studyTESTMR1.getStudyInstanceUID(), 2));
            fail();
        } catch (InvalidVersionNumberException ex) {
            // Do Nothing. Assert statement has to throw an exception.
        }
    }

    /**
     * Tests the insertStudyEntry method for accuracy by pushing a
     * TransportableStudyEntry into a store and pulling it and comparing the
     * original study entries for equality
     * 
     * @throws IOException
     */
    @Test
    public void testInsertStudyEntry() throws IOException {
        Study studyTESTMR1 = TestUtils.readStudyFromResource(store, "TESTMR_1");
        store.writeVersion(studyTESTMR1);
        StudyEntry originalEntry = store.directory.lookup(studyTESTMR1.getStudyInstanceUID());
        Store store2 = new StoreFS(ROOT_DIRECTORY);
        store2.insertStudyEntry(originalEntry.createTransportableStudyEntry());
        StudyEntry convertedEntry = store2.directory.lookup(studyTESTMR1.getStudyInstanceUID());
        assertEquals(originalEntry, convertedEntry);
    }

    /**
     * Tests that when reading a stored study and rewriting that the store
     * returns null as the study is unchanged according to its version type
     * 
     * @throws IOException
     */
    @Test
    public void testWriteUnchangedStudy() throws IOException {
        Study studyTESTMR1 = TestUtils.readStudyFromResource(store, "TESTMR_1");
        store.writeVersion(studyTESTMR1);
        Study readStudy = store.readVersion(studyTESTMR1.getStudyInstanceUID());
        assertNull(store.writeVersion(readStudy));
    }

    @Test
    public void testStudyEntryContains() throws IOException, ClassNotFoundException {
        Study studyTESTMR1 = TestUtils.readStudyFromResource(store, "TESTMR_1");
        StudyEntryFS entry = new StudyEntryFS(store, studyTESTMR1);
        entry.writeVersion(studyTESTMR1);
        TransportableStudyEntry entry1 = entry.createTransportableStudyEntry();
        // serialize/deserialize to create deep copy of object
        serialize(entry1);
        entry1 = deserialize();
        Study studyTESTMR2 = TestUtils.readStudyFromResource(store, "TESTMR_2");
        entry.writeVersion(studyTESTMR2);
        TransportableStudyEntry entry2 = entry.createTransportableStudyEntry();
        Store store2 = new StoreFS(ROOT_DIRECTORY);
        store2.insertStudyEntry(entry1);
        store2.insertStudyEntry(entry2);
        if (store2.insertStudyEntry(entry1)) {
            fail("Shouldn't be able to insert study entry that doesn't contain the old study entry");
        }
    }

    private void serialize(TransportableStudyEntry message) throws IOException {
        FileOutputStream fos = new FileOutputStream(OUTPUT_FILE_NAME);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        try {
            oos.writeObject(message);
            oos.flush();
        } finally {
            oos.close();
        }

    }

    private TransportableStudyEntry deserialize() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(OUTPUT_FILE_NAME);
        ObjectInputStream ois = new ObjectInputStream(fis);
        TransportableStudyEntry messageDeserialized;
        try {
            messageDeserialized = (TransportableStudyEntry) ois.readObject();
        } finally {
            ois.close();
        }
        return messageDeserialized;
    }
}
