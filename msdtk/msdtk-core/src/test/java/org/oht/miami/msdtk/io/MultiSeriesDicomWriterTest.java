/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.io;

import static org.junit.Assert.assertEquals;

import org.oht.miami.msdtk.conversion.dicom.Study2Dicom;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.TestUtils;

import org.dcm4che2.io.DicomOutputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Tests the conversion from the MS-DICOM Study data model to a MS-DICOM file
 * for accuracy.
 * 
 * @author Raphael Yu Ning
 */
public class MultiSeriesDicomWriterTest {

    private final String OUTPUT_DIR = "target/test-out/MultiSeriesDicomWriterTest";

    private Study study;

    private Study multiSeriesStudy;

    private Study singleFrameStudy;

    private Study multiFrameStudy;

    private Store store;

    @Before
    public void setUp() {
        store = StoreFactory.createStore("testStoreFS.properties");
    }

    @After
    public void tearDown() throws IOException {
        TestUtils.cleanStore(store);
    }

    @Test
    public void testReadTESTMRWriteUnifiedMultiSeries() throws IOException {
        doTestReadSingleFrameWriteUnifiedMultiSeries("TESTMR");
    }

    @Test
    public void testRead20phaseWriteUnifiedMultiSeries() throws IOException {
        doTestReadSingleFrameWriteUnifiedMultiSeries("20phase");
    }

    private void doTestReadSingleFrameWriteUnifiedMultiSeries(String studyName) throws IOException {
        File outputDir = new File(OUTPUT_DIR);
        outputDir.mkdirs();

        study = TestUtils.readStudyFromResource(store, studyName);
        File multiSeriesOutputDir = TestUtils.writeMultiSeries(study, outputDir, studyName);
        multiSeriesStudy = TestUtils.readStudyFromFile(store, multiSeriesOutputDir);
        File singleFrameOutputDir = TestUtils.writeSingleFrame(multiSeriesStudy, outputDir,
                studyName, true);
        singleFrameStudy = TestUtils.readStudyFromFile(store, singleFrameOutputDir);
        assertEquals(study, singleFrameStudy);
    }

    @Test
    public void testReadMultiFrameWriteUnifiedMultiSeries() throws IOException {
        final String STUDY_NAME = "nemamfct";

        File outputDir = new File(OUTPUT_DIR);
        outputDir.mkdirs();

        study = TestUtils.readStudyFromResource(store, STUDY_NAME);
        File multiSeriesOutputDir = TestUtils.writeMultiSeries(study, outputDir, STUDY_NAME);
        multiSeriesStudy = TestUtils.readStudyFromFile(store, multiSeriesOutputDir);
        File multiFrameOutputDir = TestUtils.writeMultiFrame(multiSeriesStudy, outputDir,
                STUDY_NAME);
        multiFrameStudy = TestUtils.readStudyFromFile(store, multiFrameOutputDir);
        assertEquals(study, multiFrameStudy);
    }

    private void doTestReadSingleFrameWriteMultiSeries(String studyName) throws IOException {
        File outputDir = new File(OUTPUT_DIR + "/" + studyName);
        outputDir.mkdirs();
        study = TestUtils.readStudyFromResource(store, studyName);
        File studyVersionFile = new File(outputDir, "version.dcm");
        DicomOutputStream dicomOut = new DicomOutputStream(studyVersionFile);
        try {
            dicomOut.writeDicomFile(Study2Dicom.study2MultiSeriesDicom(study));
        } finally {
            dicomOut.close();
        }

        // Declare a new Study so that tearDown() won't unnecessarily call
        // deleteStudyBulkData() on it
        Study multiSeriesStudy = TestUtils.readStudyFromFile(store, studyVersionFile);
        File singleFrameOutputDir = TestUtils.writeSingleFrame(multiSeriesStudy, outputDir,
                studyName, true);
        singleFrameStudy = TestUtils.readStudyFromFile(store, singleFrameOutputDir);
        assertEquals(study, singleFrameStudy);
    }

    private void doTestReadMultiFrameWriteMultiSeries(String studyName) throws IOException {
        File outputDir = new File(OUTPUT_DIR + "/" + studyName);
        outputDir.mkdirs();
        study = TestUtils.readStudyFromResource(store, studyName);
        File studyVersionFile = new File(outputDir, "version.dcm");
        DicomOutputStream dicomOut = new DicomOutputStream(studyVersionFile);
        try {
            dicomOut.writeDicomFile(Study2Dicom.study2MultiSeriesDicom(study));
        } finally {
            dicomOut.close();
        }
        Study multiSeriesStudy = TestUtils.readStudyFromFile(store, studyVersionFile);
        // Call writeSingleFrame instead of writeMultiFrame as writeMultiFrame
        // generates more files than necessary
        File multiFrameOutputDir = TestUtils.writeSingleFrame(multiSeriesStudy, outputDir,
                studyName, false);
        Study multiFrameStudy = TestUtils.readStudyFromFile(store, multiFrameOutputDir);
        assertEquals(study, multiFrameStudy);
    }

    @Test
    public void testReadBRTUMWriteMultiSeries() throws IOException {
        doTestReadMultiFrameWriteMultiSeries("BRTUM");
    }

    @Test
    public void testReadNemamfctWriteMultiSeries() throws IOException {
        doTestReadMultiFrameWriteMultiSeries("nemamfct");
    }

    @Test
    public void testRead20phaseWriteMultiSeries() throws IOException {
        doTestReadSingleFrameWriteMultiSeries("20phase");
    }

    @Test
    public void testReadMINT10WriteMultiSeries() throws IOException {
        doTestReadSingleFrameWriteMultiSeries("MINT10");
    }

    @Test
    public void testReadTESTCTWriteMultiSeries() throws IOException {
        doTestReadSingleFrameWriteMultiSeries("TESTCT");
    }

    @Test
    public void testReadTESTMRWriteMultiSeries() throws IOException {
        doTestReadSingleFrameWriteMultiSeries("TESTMR");
    }
}
