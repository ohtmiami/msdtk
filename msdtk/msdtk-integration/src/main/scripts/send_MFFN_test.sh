if [ $# -lt 3 ]
then
  echo "Usage: $0 <input_directory> <num_iterations> <ip>"
  exit 1
fi

IP=$3
TEMP=./temp

if [ ! -d $TEMP ]
then
  #create temporary directory for multiframe files
  mkdir $TEMP
fi 

echo Writing input formats...
for f in $1/*
do
  CURR_DIR=`basename $f`
  echo $CURR_DIR
  if [ ! -d "./out/$CURR_DIR/" ]
  then
    mkdir ./out/$CURR_DIR
  fi
  
  java -Xmx8g -jar mint-integration-2.0.jar $f $TEMP/D-MFFN write_study_multiframe normalize >> ./temp/write.log
done

echo Sending MFFN...
for (( i = 0; i < $2; i++))
do    
  for f in $1/*
  do
    CURR_STUDY=`basename $f`

    sh ./dcm4che-2.0.25/bin/dcmsnd DCMRCV@$IP:12345 $TEMP/D-MFFN/$CURR_STUDY/ | grep Sent | awk -F 'in ' '{print $2}' | awk -F 's' '{print $1}' >> ./out/$CURR_STUDY/send_MFFN.txt & 
  done
done
#rm -r ./temp/
exit $?
