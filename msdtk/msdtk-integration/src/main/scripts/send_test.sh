if [ $# -lt 3 ]
then
  echo "Usage: $0 <input_directory> <num_iterations> <ip>"
  exit 1
fi
IP=$3
TEMP=./temp
if [ ! -d $TEMP ]
then
  #create temporary directory for multiframe files
  mkdir $TEMP
fi 

for (( i = 0; i < $2; i++))
do    
  for f in $1/*
  do
    CURR_STUDY=`basename $f`
    echo $CURR_STUDY 

    if [ ! -d "./out/$CURR_STUDY/" ]
    then
	mkdir ./out/$CURR_STUDY
    fi  
    
    if [ "$i" -eq "0" ]
    then 
      echo "Writing Input Formats"
      java -Xmx8g -jar mint-integration-2.0.jar $f $TEMP/D-MFFN write_study_multiframe normalize >> ./temp/write.log
    fi

    #read D-SFC Multi-File
    echo Sending Composite Mult-File...
    sh ./dcm4che-2.0.25/bin/dcmsnd DCMRCV@$IP:12345 $f -L DCMSND:11113 >> ./out/$CURR_STUDY/send_comp.txt 
    echo Sending D-MFFN...   
    sh ./dcm4che-2.0.25/bin/dcmsnd DCMRCV@$IP:12345 $TEMP/D-MFFN/$CURR_STUDY/ -L DCMSND:11113 >> ./out/$CURR_STUDY/send_MFFN.txt
  done
done
#rm -r ./temp/
exit $?
