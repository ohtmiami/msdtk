if [ $# -lt 3 ]
then
  echo "Usage: $0 <input_directory> <num_iterations> <ip>"
  exit 1
fi

IP=$3

echo Sending Composite Mult-File...
for (( i = 0; i < $2; i++))
do    
  for f in $1/*
  do
    CURR_STUDY=`basename $f`

    if [ ! -d "./out/$CURR_STUDY/" ]
    then
      mkdir ./out/$CURR_STUDY
    fi

    sh ./dcm4che-2.0.25/bin/dcmsnd DCMRCV@$IP:12345 $f | grep Sent | awk -F 'in ' '{print $2}' | awk -F 's' '{print $1}' >> ./out/$CURR_STUDY/send_comp.txt & 
  done
done
#rm -r ./temp/
exit $?
