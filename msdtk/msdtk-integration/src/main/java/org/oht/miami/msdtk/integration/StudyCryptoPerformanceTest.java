/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.integration;

import org.oht.miami.msdtk.crypto.StudyCipher;
import org.oht.miami.msdtk.crypto.StudyCryptoException;
import org.oht.miami.msdtk.io.FileBasedStudyReader;
import org.oht.miami.msdtk.io.StudyReader;
import org.oht.miami.msdtk.store.BulkDataSet;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomUID;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.UUID;

public class StudyCryptoPerformanceTest {

    private static final Logger LOG = LoggerFactory.getLogger(StudyCryptoPerformanceTest.class);

    private static final String USAGE = "StudyCryptoPerformanceTest [OPTIONS]"
            + " <input file> <output dir>";

    private static final String DESCRIPTION = "\n<input file> must be a text file"
            + " in which each line contains the location and the name of a DICOM study,"
            + " e.g. '/path/to/TESTCT   TESTCT'" + "\nOPTIONS:";

    private static final String EXAMPLE = "\nStudyCryptoPerformanceTest"
            + " --dummy-study=/path/to/MINT10 --version-only"
            + " /path/to/studylist.txt /path/to/out";

    private static final String LONG_OPT_DUMMY_STUDY = "dummy-study";

    private static final String OPT_VERSION_ONLY = "V";

    private static final String LONG_OPT_VERSION_ONLY = "version-only";

    private static final String OPT_BULK_DATA_ONLY = "B";

    private static final String LONG_OPT_BULK_DATA_ONLY = "bulk-data-only";

    private static final String OPT_HELP = "h";

    private static final String LONG_OPT_HELP = "help";

    private static final Path STORE_PATH = Paths.get("store-root");

    // 5MB
    private static final int VERSION_BUF_SIZE = 5 << 20;

    private static final int BULK_DATA_BUF_SIZE = BulkDataSet.MAX_FILE_SIZE;

    private static StudyCipher cipher = StudyCipher.getInstance();

    private static ByteBuf buf = null;

    private static CommandLine parse(String[] args) {

        Options options = new Options();
        OptionBuilder.withLongOpt(LONG_OPT_DUMMY_STUDY);
        OptionBuilder.withDescription("path to a DICOM study to be tested first,"
                + " so as to counterbalance initialization effects");
        OptionBuilder.hasArg();
        OptionBuilder.withArgName("dir");
        options.addOption(OptionBuilder.create());
        options.addOption(OPT_VERSION_ONLY, LONG_OPT_VERSION_ONLY, false,
                "ignore Bulk Data; test encrypting Versions only");
        options.addOption(OPT_BULK_DATA_ONLY, LONG_OPT_BULK_DATA_ONLY, false,
                "ignore Versions; test encrypting Bulk Data only");
        options.addOption(OPT_HELP, LONG_OPT_HELP, false, "print this message");

        CommandLine commandLine = null;
        try {
            commandLine = new PosixParser().parse(options, args);
        } catch (ParseException e) {
            System.err.println("Invalid command-line arguments: " + e.getMessage());
            System.err.println("Try 'StudyCryptoPerformanceTest -h' for usage");
            System.exit(1);
        }
        if (commandLine.hasOption(OPT_HELP) || commandLine.getArgList().size() != 2) {
            new HelpFormatter().printHelp(USAGE, DESCRIPTION, options, EXAMPLE);
            System.exit(0);
        }
        return commandLine;
    }

    public static void main(String[] args) throws StudyCryptoException, IOException {

        CommandLine commandLine = parse(args);
        @SuppressWarnings("unchecked")
        List<String> argList = commandLine.getArgList();
        Path inputPath = Paths.get(argList.get(0));
        Path outputPath = Paths.get(argList.get(1));
        Files.createDirectories(outputPath);

        int bufSize = commandLine.hasOption(OPT_VERSION_ONLY) ? VERSION_BUF_SIZE
                : BULK_DATA_BUF_SIZE;
        buf = cipher.allocateBufferForEncryptedData(bufSize);

        if (commandLine.hasOption(LONG_OPT_DUMMY_STUDY)) {
            Path dummyStudyPath = Paths.get(commandLine.getOptionValue(LONG_OPT_DUMMY_STUDY));
            LOG.trace("DUMMY\t");
            Store store = StoreFactory.createStore();
            StudyReader dummyStudyReader = new FileBasedStudyReader(store, dummyStudyPath.toFile());
            Study dummyStudy = dummyStudyReader.readStudy();
            encryptDecryptVersion(dummyStudy);
            LOG.trace("\n");
        }

        Charset charset = Charset.forName("UTF-8");
        try (BufferedReader inputReader = Files.newBufferedReader(inputPath, charset)) {
            String line = null;
            while ((line = inputReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty()) {
                    continue;
                }
                int tabPos = line.indexOf('\t');
                Path studyPath = tabPos == -1 ? Paths.get(line) : Paths.get(line.substring(0,
                        tabPos));
                Store store = StoreFactory.createStore();
                StudyReader studyReader = new FileBasedStudyReader(store, studyPath.toFile());
                Study study = studyReader.readStudy();
                String studyName = tabPos == -1 ? study.getStudyInstanceUIDAsString() : line
                        .substring(tabPos + 1);
                Path studyOutputPath = outputPath.resolve(studyName);
                LOG.trace("{}\t", studyName);
                if (!commandLine.hasOption(OPT_BULK_DATA_ONLY)) {
                    encryptDecryptVersion(study);
                }
                if (!commandLine.hasOption(OPT_VERSION_ONLY)) {
                    if (!Files.isDirectory(studyOutputPath)) {
                        Files.createDirectory(studyOutputPath);
                    }
                    encryptDecryptBulkData(study, studyOutputPath);
                }
                LOG.trace("\n");
            }
        }
    }

    private static void encryptDecryptVersion(Study study) throws StudyCryptoException, IOException {

        Store store = StoreFactory.createStore();
        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        UUID versionUUID = store.writeVersion(study);

        Path studyStorePath = STORE_PATH.resolve(studyInstanceUID.toString());
        Path versionPath = studyStorePath.resolve(versionUUID.toString() + ".dcm");
        buf.clear();
        try (FileChannel versionInputChannel = FileChannel.open(versionPath,
                StandardOpenOption.READ)) {
            buf.writeBytes(versionInputChannel, buf.writableBytes());
        }
        cipher.initAuthenticate(studyInstanceUID);
        cipher.doFinal(buf);

        store.readVersion(studyInstanceUID);
    }

    private static void encryptDecryptBulkData(Study study, Path outputPath)
            throws StudyCryptoException, IOException {

        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        Path studyStorePath = STORE_PATH.resolve(studyInstanceUID.toString());
        try (DirectoryStream<Path> studyStoreDirStream = Files.newDirectoryStream(studyStorePath,
                "*.msdbd")) {
            for (Path bulkDataPath : studyStoreDirStream) {
                String bulkDataFileName = bulkDataPath.getFileName().toString();
                UUID bulkDataUUID = UUID.fromString(bulkDataFileName.substring(0,
                        bulkDataFileName.length() - ".msdbd".length()));
                int bulkDataSize = (int) Files.size(bulkDataPath);
                buf.clear();
                try (FileChannel bulkDataInputChannel = FileChannel.open(bulkDataPath,
                        StandardOpenOption.READ)) {
                    buf.writeBytes(bulkDataInputChannel, buf.writableBytes());
                }
                cipher.initEncrypt(studyInstanceUID, false, bulkDataUUID, bulkDataSize);
                ByteBuf encryptedBulkData = cipher.doFinal(buf);
                Path encryptedBulkDataPath = outputPath.resolve(bulkDataUUID + "_encrypted.msdbd");
                try (FileChannel bulkDataOutputChannel = FileChannel.open(encryptedBulkDataPath,
                        StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
                    encryptedBulkData.readBytes(bulkDataOutputChannel,
                            encryptedBulkData.readableBytes());
                }

                buf.clear();
                try (FileChannel bulkDataInputChannel = FileChannel.open(encryptedBulkDataPath,
                        StandardOpenOption.READ)) {
                    buf.writeBytes(bulkDataInputChannel, buf.writableBytes());
                }
                cipher.initAuthenticate(studyInstanceUID);
                ByteBuf authenticatedBulkData = cipher.doFinal(buf);

                cipher.initDecrypt(studyInstanceUID);
                ByteBuf decryptedBulkData = cipher.doFinal(authenticatedBulkData);
                Path decryptedBulkDataPath = outputPath.resolve(bulkDataUUID + "_decrypted.msdbd");
                try (FileChannel bulkDataOutputChannel = FileChannel.open(decryptedBulkDataPath,
                        StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
                    decryptedBulkData.readBytes(bulkDataOutputChannel,
                            decryptedBulkData.readableBytes());
                }
            }
        }
    }
}
