/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.msdtk.integration;

import org.oht.miami.msdtk.crypto.StudyCipher;
import org.oht.miami.msdtk.crypto.StudyCryptoException;
import org.oht.miami.msdtk.io.StudyReader;
import org.oht.miami.msdtk.io.FileBasedStudyReader;
import org.oht.miami.msdtk.store.Store;
import org.oht.miami.msdtk.store.StoreFactory;
import org.oht.miami.msdtk.studymodel.Instance;
import org.oht.miami.msdtk.studymodel.Series;
import org.oht.miami.msdtk.studymodel.Study;
import org.oht.miami.msdtk.util.DicomObjectUtil;
import org.oht.miami.msdtk.util.DicomUID;
import org.oht.miami.msdtk.util.Study2Dicom;

import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.io.DicomInputStream;
import org.dcm4che2.io.DicomOutputStream;
import org.dcm4che2.util.CloseUtils;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.UUID;
import java.util.zip.Deflater;

public class IntegrationMain {

    private static final Store store = StoreFactory.createStore();

    /*
     * args[0]: input directory args[1]: output directory args[2]: operation
     */
    public static void main(String[] args) throws StudyCryptoException, IOException,
            InterruptedException {

        if (args.length < 3) {
            System.err.println("Too few arguments");
            printUsage();
            return;
        }

        Study study = null;
        final String inputDir = args[0];
        final String outputDir = args[1];
        final String operation = args[2];
        final File in = new File(inputDir);
        final File out = new File(outputDir, in.getName());

        final long start;
        final long end;

        if (!out.exists()) {
            out.mkdirs();
        }

        /* Cleanup */
        study = null;
        File[] contents;
        contents = out.listFiles();
        for (File file : contents) {
            String fileName = file.getName();
            if (fileName.endsWith(".dcm")) {
                file.delete();
            }
        }

        if (operation.equalsIgnoreCase("read_dicom")) {
            start = System.currentTimeMillis();
            study = readStudy(in);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("read_gpb")) {
            throw new UnsupportedOperationException("read_gpb has been deprecated");
        } else if (operation.equalsIgnoreCase("read_dcmObj")) {
            start = System.currentTimeMillis();
            createDicomObjects(in);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_study_multiframe")) {
            study = readStudy(in);
            Thread.sleep(1000);
            start = System.currentTimeMillis();
            Study2Dicom.study2MultiSeriesDicom(study, out);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_series_multiframe")) {
            study = readStudy(in);
            Thread.sleep(1000);
            start = System.currentTimeMillis();
            Study2Dicom.study2MultiFrameDicom(study, out);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_composite")) {
            study = readStudy(in);
            Thread.sleep(1000);
            start = System.currentTimeMillis();
            Study2Dicom.study2SingleFrameDicom(study, out, true);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_gpb")) {
            throw new UnsupportedOperationException("write_gpb has been deprecated");
        } else if (operation.equalsIgnoreCase("write_msd")) {
            study = readStudy(in);
            Thread.sleep(1000);
            start = System.currentTimeMillis();
            writeStudy(study, out);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_sfd")) {
            start = System.currentTimeMillis();
            study = readStudy(in);
            Study2Dicom.study2SingleFrameDicom(study, out, false);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_msd_sfd")) {
            start = System.currentTimeMillis();
            study = readStudy(in);
            store.writeVersion(study);
            Study2Dicom.study2SingleFrameDicom(study, out, false);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("write_encrypted_msd")) {
            int compressionLevel = args.length < 4 ? Deflater.DEFAULT_COMPRESSION : Integer
                    .parseInt(args[3]);
            start = System.currentTimeMillis();
            study = readStudy(in);
            encryptStudy(study, out, compressionLevel);
            end = System.currentTimeMillis();
        } else if (operation.equalsIgnoreCase("study_report")) {
            start = System.currentTimeMillis();
            createStudyReport(in, out);
            end = System.currentTimeMillis();
        } else {
            System.err.println("Unrecognized operation \"" + operation + "\"");
            printUsage();
            return;
        }

        /* ignore initial run if using cached resources */
        final long time = end - start;
        System.out.printf("%d\n", time);
    }

    private static void createDicomObjects(File inputDir) throws IOException {
        if (inputDir.isDirectory()) {
            final File[] dicomFiles = inputDir.listFiles();
            for (File file : dicomFiles) {
                DicomInputStream inputStream = null;
                if (file.toString().endsWith(".dcm")) {
                    try {
                        inputStream = new DicomInputStream(file);
                        inputStream.readDicomObject();
                    } finally {
                        if (inputStream != null)
                            inputStream.close();
                    }
                }
            }
        } else {
            throw new IllegalArgumentException(inputDir + " is not a valid directory");
        }
    }

    private static Study readStudy(File inputDir) throws IOException {
        StudyReader reader = new FileBasedStudyReader(store, inputDir);
        return reader.readStudy();
    }

    private static void writeStudy(Study study, File outputDir) throws IOException {
        File studyVersionFile = new File(outputDir, study.getStudyInstanceUIDAsString() + ".dcm");
        DicomOutputStream dicomOut = new DicomOutputStream(studyVersionFile);
        try {
            dicomOut.writeDicomFile(org.oht.miami.msdtk.conversion.dicom.Study2Dicom
                    .study2MultiSeriesDicom(study));
        } finally {
            CloseUtils.safeClose(dicomOut);
        }
    }

    private static void encryptStudy(Study study, File outputDir, int compressionLevel)
            throws StudyCryptoException, IOException {
        DicomObject versionDicomObject = org.oht.miami.msdtk.conversion.dicom.Study2Dicom
                .study2MultiSeriesDicom(study);
        StudyCipher cipher = StudyCipher.getInstance();
        boolean compress = compressionLevel == Deflater.NO_COMPRESSION;
        // 132B = 128B (Preamble) + 4B ('D', 'I', 'C', 'M')
        int estimatedUnencryptedSize = 132 + DicomObjectUtil.estimateSize(versionDicomObject);
        ByteBuf unencrypted = cipher.allocateBufferForUnencryptedData(estimatedUnencryptedSize,
                compress);
        DicomOutputStream dicomOut = new DicomOutputStream((OutputStream) new ByteBufOutputStream(
                unencrypted));
        try {
            dicomOut.writeDicomFile(versionDicomObject);
        } finally {
            CloseUtils.safeClose(dicomOut);
        }
        int unencryptedSize = unencrypted.readableBytes();
        if (compress) {
            unencryptedSize -= StudyCipher.ZLIB_HEADER_LEN;
        }

        DicomUID studyInstanceUID = study.getStudyInstanceUID();
        UUID versionUUID = UUID.randomUUID();
        cipher.initEncrypt(studyInstanceUID, compress, versionUUID, unencryptedSize);
        ByteBuf encrypted = cipher.doFinal(unencrypted);

        ByteBuf key = cipher.getKey();
        for (int i = 0, n = key.readableBytes(); i < n; i++) {
            System.out.printf("0x%X ", key.getByte(i));
        }
        System.out.println();

        Path outputDirPath = outputDir.toPath();
        Path versionPath = outputDirPath.resolve(versionUUID.toString() + ".msdver");
        FileChannel versionChannel = FileChannel.open(versionPath, StandardOpenOption.WRITE,
                StandardOpenOption.CREATE_NEW);
        try {
            encrypted.readBytes(versionChannel, encrypted.readableBytes());
        } finally {
            CloseUtils.safeClose(versionChannel);
        }
    }

    private static void printUsage() {
        System.err.println("IntegrationMain <input dir> <output dir>"
                + " <operation> [<operation-specific argument(s)>]");
        System.err.println(" where <operation> must be one of the following:");
        System.err.println("  read_dicom");
        System.err.println("  write_study_multiframe");
        System.err.println("  write_series_multiframe");
        System.err.println("  write_composite");
        System.err.println("  write_msd");
        System.err.println("  write_sfd");
        System.err.println("  write_msd_sfd");
        System.err.println("  write_encrypted_msd" + " (optional argument: [<compression level>]");
        System.err.println("  study_report");
    }

    /**
     * [MSDTK-105] Creates a function that reports the series within a study. It
     * prints the seriesInstanceUID, that series Instance's and there UID's, and
     * the path to the file for the Instance. All of this information is written
     * to a .txt file
     * 
     * @param inputDir
     *            String path of the study
     * @param outputDir
     *            String path of where to write the .txt report
     * @throws IOException
     */

    public static void createStudyReport(File inputDir, File outputDir) throws IOException {
        /* reads the study */
        StudyReader reader = new FileBasedStudyReader(store, inputDir);
        Study readStudy = reader.readStudy();

        int numSeries = 1;
        int numInstance = 1;
        String fileName = "";

        /* makes sure the outputFilePath exists */

        File reportFile = new File(outputDir,
                readStudy.getValueForAttributeAsString(Tag.StudyInstanceUID) + ".txt");
        FileWriter fWriter = new FileWriter(reportFile);
        BufferedWriter writer = new BufferedWriter(fWriter);

        File[] files = inputDir.listFiles();

        writer.write("StudyInstanceUID:  "
                + readStudy.getValueForAttributeAsString(Tag.StudyInstanceUID) + "\n\n");

        for (Iterator<Series> iter = readStudy.seriesIterator(); iter.hasNext();) {
            Series testSeries = iter.next();
            writer.write("Series #" + numSeries + "\nUID:  " + testSeries.getSeriesInstanceUID()
                    + "\n");
            numSeries++;
            for (Iterator<Instance> instanceIter = testSeries.instanceIterator(); instanceIter
                    .hasNext();) {
                Instance testInstance = instanceIter.next();
                for (File file : files) {
                    DicomInputStream din = new DicomInputStream(new File("" + file));
                    DicomObject dcmObj = din.readDicomObject();
                    din.close();
                    if (testInstance.getSOPInstanceUID().equals(
                            dcmObj.getString(Tag.SOPInstanceUID))) {
                        fileName = file.getName().toString();
                        break;
                    }
                }

                writer.write("\nInstance #" + numInstance + "\nUID:  "
                        + testInstance.getSOPInstanceUID() + "\nFile Location:  " + fileName + "\n");
                numInstance++;
            }
            writer.write("---------------------------------\n");
        }
        writer.close();
    }
}
