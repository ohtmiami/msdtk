/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.dc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.nio.ByteBuffer;

public class IPPZlibTest {

    @Test
    public void testZlibVersion() {

        assertTrue(IPPZlib.zlibVersion().length() > 0);
    }

    @Test
    public void testZError() {

        String errorString = IPPZlib.zError(IPPZlibError.MEM_ERROR);
        assertEquals("insufficient memory", errorString);
    }

    @Test
    public void testCompressString() throws IPPZlibException {

        final String UNCOMPRESSED_TEXT = "blah blah blah blah";
        byte[] uncompressedBytes = UNCOMPRESSED_TEXT.getBytes();
        int uncompressedSize = uncompressedBytes.length;
        ByteBuffer uncompressed = ByteBuffer.allocateDirect(uncompressedSize + IPPZlib.HEADER_LEN);
        uncompressed.position(IPPZlib.HEADER_LEN);
        uncompressed.put(uncompressedBytes);
        uncompressed.flip();

        ByteBuffer compressed = IPPZlib.compress(uncompressed);
        int compressedSize = compressed.remaining();
        assertTrue(compressedSize < uncompressedSize);

        ByteBuffer decompressed = ByteBuffer.allocateDirect(uncompressedSize);
        IPPZlib.decompress(compressed, decompressed);
        int decompressedSize = decompressed.remaining();
        assertEquals(uncompressedSize, decompressedSize);

        byte[] decompressedBytes = new byte[decompressedSize];
        decompressed.get(decompressedBytes);
        String decompressedText = new String(decompressedBytes);
        assertEquals(UNCOMPRESSED_TEXT, decompressedText);
    }
}
