/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.cp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.oht.miami.ipp4j.core.IPPLibraryVersion;

import org.junit.Test;

public class IPPCryptoTest {

    @Test
    public void testIPPCPGetLibVersion() {

        IPPLibraryVersion libVersion = IPPCrypto.ippcpGetLibVersion();
        assertTrue(libVersion.name.length() > 0);
        assertTrue(libVersion.version.length() > 0);
        assertTrue(libVersion.buildDate.length() > 0);
        assertTrue(libVersion.getTargetCPU().length() > 0);
        assertEquals(7, libVersion.major);
    }
}
