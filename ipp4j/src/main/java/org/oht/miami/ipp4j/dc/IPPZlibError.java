/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.dc;

/**
 * Definitions of zlib error codes.
 * 
 * @author Raphael Yu Ning
 */
public class IPPZlibError {

    public static final int OK = 0;

    public static final int DATA_ERROR = -3;

    public static final int MEM_ERROR = -4;

    public static final int BUF_ERROR = -5;
}
