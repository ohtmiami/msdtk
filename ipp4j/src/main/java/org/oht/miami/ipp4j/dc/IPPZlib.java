/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.dc;

import java.nio.ByteBuffer;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.NativeLongByReference;

/**
 * JNA interface to the zlib library (implemented with IPP).
 * 
 * @author Raphael Yu Ning
 */
public class IPPZlib {

    static {

        Native.register("ippz");
    }

    /**
     * Length of the zlib header, including only the CMF and FLG fields, but not
     * DICTID (see RFC 1950).
     */
    public static final int HEADER_LEN = 2;

    public static native String zlibVersion();

    public static native String zError(int errorCode);

    public static native int compress(Pointer pDst, NativeLongByReference pDstLen, Pointer pSrc,
            NativeLong srcLen);

    public static native int compress2(Pointer pDst, NativeLongByReference pDstLen, Pointer pSrc,
            NativeLong srcLen, int level);

    public static void compress(ByteBuffer src, ByteBuffer dst, int level) throws IPPZlibException {

        ByteBuffer oldDst = dst;
        if (dst.position() > 0) {
            dst = dst.slice();
        }
        if (src.position() > 0) {
            src = src.slice();
        }
        NativeLong dstLen = new NativeLong(dst.remaining());
        NativeLong srcLen = new NativeLong(src.remaining());
        NativeLongByReference pDstLen = new NativeLongByReference(dstLen);
        Pointer pDst = Native.getDirectBufferPointer(dst);
        Pointer pSrc = Native.getDirectBufferPointer(src);
        int error = compress2(pDst, pDstLen, pSrc, srcLen, level);
        if (error != IPPZlibError.OK) {
            throw new IPPZlibException(error);
        }
        int compressedSize = pDstLen.getValue().intValue();
        oldDst.limit(oldDst.position() + compressedSize);
    }

    public static void compress(ByteBuffer src, ByteBuffer dst) throws IPPZlibException {

        compress(src, dst, IPPZlibLevel.DEFAULT_COMPRESSION);
    }

    /**
     * Compresses/deflates the input buffer in place, i.e. the output buffer is
     * the same as the input buffer. Since zlib always outputs the zlib header
     * before reading the input, the first {@code HEADER_LEN} bytes of input
     * data cannot be compressed in place. The corresponding region of the input
     * buffer should thus be filled with garbage data in advance.
     * 
     * @param src
     *            The input buffer. Input data is assumed to be found between
     *            {@code (src.position() + HEADER_LEN)} and
     *            {@code (src.limit() - 1)}.
     * @param level
     *            The zlib compression level. A valid value must be between
     *            {@code IPPZlibLevel.NO_COMPRESSION} and
     *            {@code IPPZlibLevel.BEST_COMPRESSION}, or be
     *            {@code IPPZlibLevel.DEFAULT_COMPRESSION}.
     * @return The output buffer, which overlaps {@code src} and contains the
     *         compressed data.
     * @throws IPPZlibException
     *             If zlib reports an error.
     */
    public static ByteBuffer compress(ByteBuffer src, int level) throws IPPZlibException {

        ByteBuffer dst = src.duplicate();
        dst.limit(dst.capacity());
        if (dst.position() > 0) {
            dst = dst.slice();
        }
        // Ignore the first HEADER_LEN bytes of input data
        ByteBuffer realSrc = src.duplicate();
        realSrc.position(realSrc.position() + HEADER_LEN);
        NativeLong dstLen = new NativeLong(dst.remaining());
        NativeLong srcLen = new NativeLong(realSrc.remaining());
        NativeLongByReference pDstLen = new NativeLongByReference(dstLen);
        Pointer pDst = Native.getDirectBufferPointer(dst);
        Pointer pSrc = pDst.share(HEADER_LEN);
        int error = compress2(pDst, pDstLen, pSrc, srcLen, level);
        if (error != IPPZlibError.OK) {
            throw new IPPZlibException(error);
        }
        int compressedSize = pDstLen.getValue().intValue();
        dst.limit(compressedSize);
        return dst;
    }

    public static ByteBuffer compress(ByteBuffer src) throws IPPZlibException {

        return compress(src, IPPZlibLevel.DEFAULT_COMPRESSION);
    }

    public static native NativeLong compressBound(NativeLong srcLen);

    public static native int uncompress(Pointer pDst, NativeLongByReference pDstLen, Pointer pSrc,
            NativeLong srcLen);

    public static void decompress(ByteBuffer src, ByteBuffer dst) throws IPPZlibException {

        ByteBuffer oldDst = dst;
        if (dst.position() > 0) {
            dst = dst.slice();
        }
        if (src.position() > 0) {
            src = src.slice();
        }
        NativeLong dstLen = new NativeLong(dst.remaining());
        NativeLong srcLen = new NativeLong(src.remaining());
        NativeLongByReference pDstLen = new NativeLongByReference(dstLen);
        Pointer pDst = Native.getDirectBufferPointer(dst);
        Pointer pSrc = Native.getDirectBufferPointer(src);
        int error = uncompress(pDst, pDstLen, pSrc, srcLen);
        if (error != IPPZlibError.OK) {
            throw new IPPZlibException(error);
        }
        int decompressedSize = pDstLen.getValue().intValue();
        oldDst.limit(oldDst.position() + decompressedSize);
    }
}
