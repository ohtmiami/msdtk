/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.core;

import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

/**
 * JNA interface to the IPP Core library (see ippcore.h).
 * 
 * @author Raphael Yu Ning
 */
public class IPPCore {

    static {

        Native.register(Platform.isWindows() ? "ippcore-7.1" : "ippcore");
    }

    public static native IPPLibraryVersion ippGetLibVersion();

    public static native String ippGetStatusString(int stsCode);

    public static native Pointer ippMalloc(int length);

    public static Pointer malloc(int length) throws IPPException {

        Pointer pointer = ippMalloc(length);
        if (pointer == null) {
            throw new IPPException(IPPStatus.NO_MEM_ERR);
        }
        return pointer;
    }

    public static native void ippFree(Pointer ptr);

    public static native int ippSetNumThreads(int numThr);

    public static void setNumberOfThreads(int numThreads) throws IPPException {

        int status = ippSetNumThreads(numThreads);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPException(status);
        }
    }

    public static native int ippGetNumThreads(IntByReference pNumThr);

    public static int getNumberOfThreads() throws IPPException {

        IntByReference pNumThreads = new IntByReference();
        int status = ippGetNumThreads(pNumThreads);
        if (status != IPPStatus.NO_ERR) {
            throw new IPPException(status);
        }
        return pNumThreads.getValue();
    }

    public static native int ippGetNumCoresOnDie();
}
