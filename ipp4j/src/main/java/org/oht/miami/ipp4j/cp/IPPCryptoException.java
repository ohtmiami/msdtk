/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.cp;

import org.oht.miami.ipp4j.core.IPPCore;
import org.oht.miami.ipp4j.core.IPPException;

/**
 * A class that represents exceptions in encryption/decryption using IPP.
 * 
 * @author Raphael Yu Ning
 */
public class IPPCryptoException extends IPPException {

    private static final long serialVersionUID = -4124672354823293058L;

    public IPPCryptoException(String msg) {

        super(msg);
    }

    public IPPCryptoException(int statusCode) {

        super(IPPCore.ippGetStatusString(statusCode));
    }
}
