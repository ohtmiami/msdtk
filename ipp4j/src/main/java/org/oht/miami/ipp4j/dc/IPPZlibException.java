/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.dc;

import org.oht.miami.ipp4j.core.IPPException;

/**
 * A class that represents exceptions in compression/decompression using zlib.
 * 
 * @author Raphael Yu Ning
 */
public class IPPZlibException extends IPPException {

    private static final long serialVersionUID = 5751132847550153196L;

    public IPPZlibException(String msg) {

        super(msg);
    }

    public IPPZlibException(int errorCode) {

        super(IPPZlib.zError(errorCode));
    }
}
