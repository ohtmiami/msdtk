/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.util;

import java.nio.ByteBuffer;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

/**
 * Utilities for direct buffers.
 * 
 * @author Raphael Yu Ning
 */
public class DirectBuffers {

    public enum Alignment {

        _4(4), _8(8), _16(16), _32(32), _64(64);

        private final int byteBoundary;

        private Alignment(int byteBoundary) {

            this.byteBoundary = byteBoundary;
        }

        public int getByteBoundary() {

            return byteBoundary;
        }
    }

    public static Memory allocateAligned(int size, Alignment alignment) {

        int byteBoundary = alignment.getByteBoundary();
        int enlargedSize = size + byteBoundary - 1;
        Memory memoryBlock = new Memory(enlargedSize);
        long address = Memory.nativeValue(memoryBlock);
        long mask = ~((long) byteBoundary - 1);
        if ((address & mask) == address) {
            return memoryBlock;
        }
        long newAddress = (address + byteBoundary) & mask;
        long offset = newAddress - address;
        return (Memory) memoryBlock.share(offset, enlargedSize - offset);
    }

    public static Memory allocateAligned32(int size) {

        return allocateAligned(size, Alignment._32);
    }

    public static Memory allocateAligned64(int size) {

        return allocateAligned(size, Alignment._64);
    }

    public static Pointer getPointer(ByteBuffer directBuffer) {

        Pointer pDirectBuffer = Native.getDirectBufferPointer(directBuffer);
        if (directBuffer.position() == 0) {
            return pDirectBuffer;
        } else {
            return pDirectBuffer.share(directBuffer.position());
        }
    }
}
