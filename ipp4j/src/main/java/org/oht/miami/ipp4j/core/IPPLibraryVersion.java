/*
 * Copyright (C) 2012, The MINT Consortium (See COPYRIGHTS file for a list of copyright holders).
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright holders, and is made
 * available under a license. If you do not know the terms of the license, please review it before
 * you read further.
 *
 * You can read LICENSES for detailed information about the license terms this source code file is
 * available under.
 *
 * Questions should be directed to legal@peakhealthcare.com
 *
 */

package org.oht.miami.ipp4j.core;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import com.sun.jna.Structure;

/**
 * JNA interface to the IppLibraryVersion struct (see ippdefs.h).
 * 
 * @author Raphael Yu Ning
 */
public class IPPLibraryVersion extends Structure {

    public int major;

    public int minor;

    public int majorBuild;

    public int build;

    public byte[] targetCPU = new byte[4];

    public String name;

    public String version;

    public String buildDate;

    public String getTargetCPU() {

        try {
            return new String(targetCPU, 0, targetCPU.length - 1, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("US-ASCII encoding is not supported", e);
        }
    }

    @Override
    protected List<String> getFieldOrder() {

        return Arrays.asList(new String[] { "major", "minor", "majorBuild", "build", "targetCPU",
                        "name", "version", "buildDate" });
    }
}
